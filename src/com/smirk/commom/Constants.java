package com.smirk.commom;

public class Constants {
	public static final String SIGNUP_URL = "http://23.253.166.61:8080/smirkservices/api/user/signup";
	public static final String LOGIN_URL = "http://23.253.166.61:8080/smirkservices/api/user/login";
	public static final String UPDATE_PROFILE_URL = "http://23.253.166.61:8080/smirkservices/api/user/profile/updates";
													
	public static final String SET_SECRET_ANSWERS = "http://23.253.166.61:8080/smirkservices/api/user/answers";
	public static final String GET_SECRET_ANSWERS = "http://23.253.166.61:8080/smirkservices/api/user/my_secret_qa";
	public static final String SET_NEW_PASSWORD = "http://23.253.166.61:8080/smirkservices/api/user/new_password";
	
	public static final String USERS_URL = "http://23.253.166.61:8080/smirkservices/api/friend/users/0/0";
	public static final String USERS_SEARCH_URL = "http://23.253.166.61:8080/smirkservices/api/friend/user/search/";
	
	public static final String INVITE_FRIEND_URL = "http://23.253.166.61:8080/smirkservices/api/friend/invitation/"; //{id}
	public static final String NOTIFICATIONS_URL = "http://23.253.166.61:8080/smirkservices/api/friend/notifications/0/0/";  //{page}/{size}
	public static final String INVITE_RESPONSE_URL = "http://23.253.166.61:8080/smirkservices/api/friend/invitation_response/";  //{notId}/{response}
	public static final String REMOVE_FRIEND_URL = "http://23.253.166.61:8080/smirkservices/api/friend/remove/";  //{id}
	
	public static final String FRIENDS_URL = "http://23.253.166.61:8080/smirkservices/api/friend/friends/0/0";  //{page}/{size}

	public static final String SMIRKED_PICS_URL = "http://23.253.166.61:8080/smirkservices/api/user/smirkedpics";
	public static final String GET_SMIRKED_PICS_URL = "http://23.253.166.61:8080/smirkservices/api/user/smirkedpics";
	public static final String SHARE_SMIRKS = "http://23.253.166.61:8080/smirkservices/api/user/share";
	public static final String DELETE_SMIRK = "http://23.253.166.61:8080/smirkservices/api/user/smirkedpics_delete";

	public static final String DOWNLOAD_RESOURCES = "http://23.253.166.61:8080/smirkservices/api/user/smirks/0/0";
}
