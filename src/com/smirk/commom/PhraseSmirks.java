package com.smirk.commom;

import java.util.List;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.content.res.Resources;
import android.util.Log;

import com.smirk.R;

public class PhraseSmirks{
	
	public static final String TAG = "PhraseSmirks";

	private int thumbnailBMP;
	private int smirkGIF;
	
	public static List<PhraseSmirks> initializePhraseSmirks(Context context){
		
		List<PhraseSmirks> list = new ArrayList<PhraseSmirks>();
		
		return list;
	}
	
	public PhraseSmirks(int thumbnailBMP, int smirkGIF){
		this.thumbnailBMP = thumbnailBMP;
		this.smirkGIF = smirkGIF;
	}
	
	public int getThumbId(){
		return this.thumbnailBMP;
	}
	
	public int getGifId(){
		return this.smirkGIF;
	}

	private static int getGifFromThumbId(int thumbId){
		return 0;
	}
	
}