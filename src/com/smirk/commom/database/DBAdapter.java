package com.smirk.commom.database;

import java.io.IOException;
import android.content.Context;
import android.database.Cursor;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteStatement;
import android.util.Log;

public class DBAdapter 
{
    protected static final String TAG = "DataAdapter";

    private final Context mContext;
    private SQLiteDatabase mDb;
    private DataBaseHelper mDbHelper;

    private static DBAdapter adapterInstance;

    public static synchronized DBAdapter getAdapter(Context context)
    {
        if (adapterInstance == null){
            adapterInstance = new DBAdapter(context);}

        return adapterInstance;
    }

    public DBAdapter(Context context) 
    {
		mContext = context;
        mDbHelper = new DataBaseHelper(mContext);

   }

    public void begin(){
		mDb.beginTransaction();
    }

    public void end(){
	mDb.endTransaction();
    }

    public DBAdapter createDatabase() throws SQLException 
    {
        try 
        {
            mDbHelper.createDataBase();
        } 
        catch (IOException mIOException) 
        {
            Log.e(TAG, mIOException.toString() + "  UnableToCreateDatabase");
            throw new Error("UnableToCreateDatabase");
        }
        return this;
    }

    public DBAdapter open() throws SQLException 
    {
        try 
        {
            mDbHelper.openDataBase();
            //mDbHelper.close();
            mDb = mDbHelper.getReadableDatabase();
        } 
        catch (SQLException mSQLException) 
        {
            Log.e(TAG, "open >>"+ mSQLException.toString());
            throw mSQLException;
        }
        return this;
    }

    public void close() 
    {
        mDbHelper.close();
    }

     public Cursor getData(String query)
     {
         try
         {

             Cursor mCur = mDb.rawQuery(query, null);
             if (mCur.getCount() > 0)
             	{
                	mCur.moveToFirst();
			return mCur;
             	}
             else{
			//SQLiteDatabase.rawQuery(query, null) won't return a real null; just a Cursor with nothing in it
			//Added this to be able to easiy test for null 
			return null;
		}
         }
         catch (SQLException mSQLException) 
         {
             Log.e(TAG, "getData >>"+ mSQLException.toString());
             throw mSQLException;
         }
	
     }

   public void setSuccessful(){
	mDb.setTransactionSuccessful();
    }

    public void execute(String command){
	try{
		Log.e(TAG, "Execute: "+command);
		mDb.execSQL(command);
	} catch (SQLException mSQLException){
		throw mSQLException;
	}
    }

    public int getTotalRows(String tableName)
    {
		String query = "SELECT COUNT (*) FROM "+tableName;
		SQLiteStatement statement = mDb.compileStatement(query);
		int count = (int)statement.simpleQueryForLong();
		return count;
    }
}
