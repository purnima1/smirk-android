package com.smirk.commom.database;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;

import android.content.Context;
import android.database.SQLException;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class DataBaseHelper extends SQLiteOpenHelper{

	private static String TAG = "DataBaseHelper"; //tag for logcat
	private static String DB_PATH = "/data/data/com.smirk/databases/";
	private static String DB_NAME ="smirk_DB2.db";// Database name
	private SQLiteDatabase mDataBase; 
	private final Context mContext;

	public DataBaseHelper(Context context){
	    super(context, DB_NAME, null, 1);
	    this.mContext = context;
	}   

	public void onCreate(SQLiteDatabase db) {
      
	}

	public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion){
		//db.execSQL("DROP TABLE IF EXISTS" + DATABASE_TABLE);
		onCreate(db);
	}

	public void createDataBase() throws IOException
	{	
    	//If database does not exist, copy from assets/

    	boolean mDataBaseExist = checkDataBase();
  	  if(!mDataBaseExist)
 	   {
        	this.getReadableDatabase();
        	//this.close();
        	try {
            	//Copy the database from assests
            	copyDataBase();
            	Log.e(TAG, "createDatabase database created");
        } 
        catch (IOException mIOException) 
        {
            throw new Error("ErrorCopyingDataBase");
        }
		}
	}

    private boolean checkDataBase()
    {
        File dbFile = new File(DB_PATH + DB_NAME);
        //Log.v("dbFile", dbFile + "   "+ dbFile.exists());
        return dbFile.exists();
    }

    //Copy the database from assets
    private void copyDataBase() throws IOException
    {
        InputStream mInput = mContext.getAssets().open(DB_NAME);
        String outFileName = DB_PATH + DB_NAME;
        OutputStream mOutput = new FileOutputStream(outFileName);
        byte[] mBuffer = new byte[1024];
        int mLength;
        while ((mLength = mInput.read(mBuffer))>0)
        {
            mOutput.write(mBuffer, 0, mLength);
        }
        mOutput.flush();
    }

  
    public boolean openDataBase() throws SQLException
    {
        String mPath = DB_PATH + DB_NAME;
        //Log.v("mPath", mPath);
        //mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.CREATE_IF_NECESSARY);
        mDataBase = SQLiteDatabase.openDatabase(mPath, null, SQLiteDatabase.NO_LOCALIZED_COLLATORS);
        return mDataBase != null;
    }

    @Override
    public void close() 
    {
        if(mDataBase != null)
            mDataBase.close();
        super.close();
    }

}
