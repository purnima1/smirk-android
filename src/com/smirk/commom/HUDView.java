package com.smirk.commom;

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.PixelFormat;
import android.util.Log;
import android.view.Gravity;
import android.view.MotionEvent;
import android.view.ViewGroup;
import android.view.WindowManager;

/**
 * View to display above all other views
 * http://stackoverflow.com/questions/4481226/creating-a-system-overlay-window-always-on-top
 * @author Mark Stanford
 *
 */
public class HUDView extends ViewGroup {
	
	private final String TAG = "HUDView";
	
    private Paint mLoadPaint;
    private String message = "";
    WindowManager wm;

    public HUDView(Context context,String msg) {
        super(context);
        
        message = msg;
        mLoadPaint = new Paint();
        mLoadPaint.setTextSize(50);
        mLoadPaint.setColor(Color.BLACK);
        
        WindowManager.LayoutParams params = new WindowManager.LayoutParams(WindowManager.LayoutParams.WRAP_CONTENT, WindowManager.LayoutParams.WRAP_CONTENT,
				WindowManager.LayoutParams.TYPE_PHONE, WindowManager.LayoutParams.FLAG_NOT_TOUCH_MODAL
				| WindowManager.LayoutParams.FLAG_WATCH_OUTSIDE_TOUCH | WindowManager.LayoutParams.FLAG_NOT_FOCUSABLE, PixelFormat.TRANSLUCENT);
        params.gravity = Gravity.LEFT | Gravity.BOTTOM;
        wm = (WindowManager) context.getSystemService(context.WINDOW_SERVICE);
        Log.d(TAG,"Creating and adding HUDView");
        
        wm.addView(this, params);
    }

    @Override
    protected void onDraw(Canvas canvas) {
        //canvas.drawText(message, 0, 0, mLoadPaint);
        canvas.drawText("Processing images please wait...", 0, 0, mLoadPaint);
    }

    @Override
    protected void onLayout(boolean arg0, int arg1, int arg2, int arg3, int arg4) {
    }

    @Override
    public boolean onTouchEvent(MotionEvent event) {
    	wm.removeView(this);
    	Log.d(TAG,"Removing HUDView");
        return true;
    }
    
    public void kill(){
    	Log.d(TAG,"Removing HUDView");
    	this.removeAllViews();
    	wm.removeView(this);
    }
}
