package com.smirk.commom;

import java.util.ArrayList;
import java.util.List;

import android.net.Uri;
import android.util.Log;
import android.graphics.Bitmap;
import android.graphics.drawable.Drawable;
import android.widget.ImageView;
import android.os.Parcelable;
import android.os.Parcel;

public class Friend{
	
	private static final String TAG = "Friend: ";
	
	public static int countHolder;
	public static boolean hasNew;
	
	private String userName, email, realName, screenName;
	private int profilePictureId;
	private String profilePictureUrl;
	private int id;
	private Drawable profilePictureBMP;
	
	private int notificationId;
	//private Uri profilePicUri;
	
	public static List<Friend> existingFriends = new ArrayList<Friend>();
	
	public Friend(String screenName, String profilePictureUrl, int id, Drawable profilePictureBMP){
		this.screenName = screenName;
		this.profilePictureUrl = profilePictureUrl;
		this.id = id;
		this.profilePictureBMP = profilePictureBMP;
	}
	
	public static List<Friend> getList(){
		return existingFriends;
	}
	
	public static void setList(List<Friend> backendExistingFriends){
		existingFriends = backendExistingFriends;
		Log.d(TAG, "List of existing friend set!");
	}
	
	public static void removeFriend(int removeId){
		for (int i=0; i<existingFriends.size(); i++){
			if (existingFriends.get(i).id == removeId){
				existingFriends.remove(i);
			}
		}
	}
	
	public String getProfilePicURL(){
		return this.profilePictureUrl;
	}
	
	public Drawable getUserBitmap(){
		return this.profilePictureBMP;
	}
	
	public String getUserName(){
		return this.userName;}
		
	public String getEmail(){
		return this.email;}
		
	public String getRealName(){
		return this.realName;}
		
	public int getPictureId(){
		return this.profilePictureId;}
		
	public void setProfilePicture(Drawable pic){
		this.profilePictureBMP = pic;
	}
		
	public void setUserName(String userName){
		this.userName = userName;}
		
	public void setPictureId(int profilePictureId){
		this.profilePictureId = profilePictureId;}
		
	public void setEmail(String email){
		this.email = email;}
	
	public void setRealName(String realName){
		this.realName = realName;}
	
	public List<Friend> addExistingFriend(List<Friend> existingFriends, Friend existingFriend){
		existingFriends.add(existingFriend);
		return existingFriends;}

	public String getScreenName() {
		return this.screenName;
	}

	public void setScreenName(String screenName) {
		this.screenName = screenName;
	}

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public int getNotificationId() {
		return notificationId;
	}

	public void setNotificationId(int notificationId) {
		this.notificationId = notificationId;
	}
		

		
}