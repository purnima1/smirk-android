package com.smirk.commom;

import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.ByteArrayInputStream;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Movie;
import android.widget.ImageView;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.res.Resources;
import android.util.Log;
import android.os.Bundle;
import android.widget.Toast;

import com.smirk.R;
import com.smirk.network.SmirkApiCalls;
import com.smirk.screens.PreviewSmirkActivity;

public class SmirkResource{
	
	public static final String TAG = "SmirkResource: ";
	
	public static List<SmirkResource> smirkResources = new ArrayList<SmirkResource>();
	public static List<SmirkResource> eyeResources = new ArrayList<SmirkResource>();
	public static List<SmirkResource> mouthResources = new ArrayList<SmirkResource>();
	public static List<SmirkResource> wordResources = new ArrayList<SmirkResource>();
	
	public static byte[] gifByteArrHolder;
	public static Bitmap indicatorBitmapHolder;
	
	private int id;
	private int expressionType; //can be one of the above 3 types
	
	private String thumbUrl;
	private String gifUrl;
	
	private Bitmap thumbBitmap;
	private Movie gifMovie;
	
	private static Context mContext;

	private static BroadcastReceiver receiveGIF = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.d(TAG, "Completed GIF download broadcast received.");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String res = intent.getStringExtra("GIF_DOWNLOADED");  
				Log.d(TAG, "OnRecieve: " + res);
				if (res.contains("OK")){
					Log.d(TAG, "Received successful broadcast from GIF download!");
					int expType = extras.getInt("EXP_TYPE");
					int resId = extras.getInt("ID");
					byte[] gifAsByteArr = gifByteArrHolder; //passing this on bundle got huge
					boolean launchPrev = extras.getBoolean("LAUNCH_PREV");
					addGIFResource(expType, resId, gifAsByteArr);
					
					boolean isAnimated;
					if (expType == Utils.EYE_SMIRK || expType == Utils.MOUTH_SMIRK){
						isAnimated = false;
					} else{
						isAnimated = true;
					}
					
					if (launchPrev == true){
						Intent previewIntent = new Intent(context, PreviewSmirkActivity.class);
						previewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
						previewIntent.putExtra("EXP_TYPE", expType);
						previewIntent.putExtra("EXP_ID", resId);
						previewIntent.putExtra(Utils.IS_ANIMATED, isAnimated);
						mContext.startActivity(previewIntent);
					}
					unregisterReceiver();
				}
				else if (res.contains("FAILED")){
					Log.d(TAG, "Received failure broadcast from GIF download!");
					Toast.makeText(context, "Animation download failed!", Toast.LENGTH_LONG).show();
					unregisterReceiver();
				}
			}
		}
	};
	
	public SmirkResource(int id, int expressionType, String thumbUrl, String gifUrl, Bitmap thumbBitmap, 
	Movie gifMovie){
		//set these 5 fields on downloading the Smirk bitmap
		this.id = id;
		this.expressionType = expressionType;
		this.thumbUrl = thumbUrl;
		this.gifUrl = gifUrl;
		this.thumbBitmap = thumbBitmap;
		
		//Only download and set this when it's specifically called for by app player
		this.gifMovie = gifMovie;
	}
	
	public static void registerReceiver(Context context){
		mContext = context;
		mContext.registerReceiver(receiveGIF, new IntentFilter("DOWNLOAD_GIF"));
	}
	
	public static void unregisterReceiver(){
		mContext.unregisterReceiver(receiveGIF);
	}
	
	public static void addSmirkToList(SmirkResource resource){
		Log.d(TAG, "Smirk resource added to list of type: "+resource.expressionType);
		smirkResources.add(resource);
		
		if (resource.expressionType == Utils.EYE_SMIRK){ eyeResources.add(resource); }
		else if (resource.expressionType == Utils.MOUTH_SMIRK){ mouthResources.add(resource); }
		else if (resource.expressionType == Utils.WORD_SMIRK){ wordResources.add(resource); }
	}
	
	public static String findGifUrl(int expressionType, int id){
		String gifDownloadUrl = "";
		if (expressionType == Utils.EYE_SMIRK){
			for (SmirkResource res : eyeResources){
				if (res.getId() == id){
					gifDownloadUrl = res.getGifDownloadUrl();
					Log.d(TAG, "Gif URL: "+gifDownloadUrl);
					break;
				}
			}
		}
		else if(expressionType == Utils.MOUTH_SMIRK){
			for (SmirkResource res : mouthResources){
				if (res.getId() == id){
					gifDownloadUrl = res.getGifDownloadUrl();
					Log.d(TAG, "Gif URL: "+gifDownloadUrl);
					break;
				}
			}
		}
		else if(expressionType == Utils.WORD_SMIRK){
			for (SmirkResource res : wordResources){
				if (res.getId() == id){
					gifDownloadUrl = res.getGifDownloadUrl();
					Log.d(TAG, "Gif URL: "+gifDownloadUrl);
					break;
				}
			}
		}
		return gifDownloadUrl;
	}
	
	public int getId(){
		return this.id;
	}
	
	public int getExpressionType(){
		return this.expressionType;
	}
	
	public Bitmap getThumbBitmap(){
		return this.thumbBitmap;
	}
	
	public String getGifDownloadUrl(){
		return this.gifUrl;
	}
	
	public Movie getGif(){
		return this.gifMovie;
	}
	
	public void setGifResource(Movie gifMovie){
		this.gifMovie = gifMovie;
	}
	
	public static void downloadGIF(int expressionType, int id, String gifUrl, boolean launchPrev){
		SmirkApiCalls.downloadSmirkGIF(expressionType, id, gifUrl, mContext, launchPrev);
	}
	
	public static void addGIFResource(int expressionType, int id, byte[] gifAsByteArr){
		InputStream is = new ByteArrayInputStream(gifAsByteArr);
		Movie gif = Movie.decodeStream(is);
		
		if (expressionType == Utils.EYE_SMIRK){
			for (SmirkResource res : eyeResources){
				if (res.getId() == id){
					res.setGifResource(gif);
					Log.d(TAG, "Gif set!");
					break;
				}
			}
		}
		else if(expressionType == Utils.MOUTH_SMIRK){
			for (SmirkResource res : mouthResources){
				if (res.getId() == id){
					res.setGifResource(gif);
					Log.d(TAG, "Gif set!");
					break;
				}
			}
		}
		else if(expressionType == Utils.WORD_SMIRK){
			for (SmirkResource res : wordResources){
				if (res.getId() == id){
					res.setGifResource(gif);
					Log.d(TAG, "Gif set!");
					break;
				}
			}
		}
	}
	
	public static Movie getMovieFromSmirkResList(int expressionType, int resId){
		Movie gif = null;
		if (expressionType == Utils.EYE_SMIRK){
			for (SmirkResource res : eyeResources){
				if (res.getId() == resId){
					gif = res.getGif();
					break;
				}
			}
		}
		else if (expressionType == Utils.MOUTH_SMIRK){
			for (SmirkResource res : mouthResources){
				if (res.getId() == resId){
					gif = res.getGif();
					break;
				}
			}
		}
		else if (expressionType == Utils.WORD_SMIRK){
			for (SmirkResource res : wordResources){
				if (res.getId() == resId){
					gif = res.getGif();
					break;
				}
			}
		}
		return gif;
	}

	public static Bitmap getBitmapFromSmirkResList(int expressionType, int resId){
		Bitmap bitmap = null;
		if (expressionType == Utils.EYE_SMIRK){
			for (SmirkResource res : eyeResources){
				if (res.getId() == resId){
					bitmap = res.getThumbBitmap();
					break;
				}
			}
		}
		else if (expressionType == Utils.MOUTH_SMIRK){
			for (SmirkResource res : mouthResources){
				if (res.getId() == resId){
					bitmap = res.getThumbBitmap();
					break;
				}
			}
		}
		else if (expressionType == Utils.WORD_SMIRK){
			for (SmirkResource res : wordResources){
				if (res.getId() == resId){
					bitmap = res.getThumbBitmap();
					break;
				}
			}
		}
		return bitmap;
	}

}