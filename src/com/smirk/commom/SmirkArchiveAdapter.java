package com.smirk.commom;

import java.util.List;
import java.util.ArrayList;

import android.widget.BaseAdapter;
import android.content.Context;
import android.widget.ImageView;
import android.widget.GridView;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.view.ViewGroup;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.content.res.Resources;
import android.graphics.drawable.LayerDrawable;

import com.smirk.SmirkMainActivity;

public class SmirkArchiveAdapter extends BaseAdapter{

	private Context mContext;
	private List<SmirkImage> smirkImages;
	private int mWidth, mHeight;
	private Bitmap reactionPicture;
	
	    public SmirkArchiveAdapter(Context c, List<SmirkImage> smirkImages, int width, int height){
	      	this.mContext = c;
			this.smirkImages = smirkImages;
			this.mWidth = width;
			this.mHeight = height;
	    }

	    public int getCount() {
	        return smirkImages.size();
	    }

	    public SmirkImage getItem(int position) {
	        return smirkImages.get(position);
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView;
			
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(mWidth, mHeight));
	            //imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
	        } else {
	            imageView = (ImageView) convertView;
	        }
	
			//get reactionPicture as a bitmap
			Bitmap picture = smirkImages.get(position).getPicture();

			imageView.setImageBitmap(picture);
			
			//recycle bitmaps!
			//reactionPicture.recycle();
			//indicatorBMP.recycle();
	        return imageView;
	    }
	

	 
}
