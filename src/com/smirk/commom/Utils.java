package com.smirk.commom;

import java.util.ArrayList;
import java.util.List;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.BufferedInputStream;
import java.net.URL;
import java.net.URLConnection;
import java.io.IOException;

import org.apache.http.util.ByteArrayBuffer;

import android.content.res.Resources;
import android.net.Uri;
import android.database.Cursor;
import android.graphics.Paint.Style;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Matrix;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.provider.MediaStore;
import android.provider.MediaStore.Files.FileColumns;
import android.text.TextUtils;
import android.util.TypedValue;
import android.util.Log;
import android.graphics.Bitmap.CompressFormat;

public class Utils {
	
	private static final String TAG = "Utils: ";
	
	public static final String SHARED_PREFS = "SmirkSharedPrefs";

	public enum Screens {
		HomeScreen, LoginScreen, PlayScreen, ReviewPictureScreen, ScroePictureScreen, ShareScreen, ArchiveScreen, FriendsScreen, AlertScreen, PhotoOptionsScreen
	}
	
	public static final int EYE_SMIRK = 1;
	public static final int MOUTH_SMIRK = 2;
	public static final int WORD_SMIRK = 3;

	//public static List<Bitmap> bitmaps = new ArrayList<Bitmap>();
	//public static Bitmap bitmap;
	public static boolean isCameraActivityOnTop = false;

	public final static int NUMBER_OF_REVIEW_PICS = 4;
	public final static int NUMBER_OF_TABS = 4;
	public final static int NUM_EXPRESSION_TYPES = 3;
	public final static int BACK_CAMERA = 1;
	public final static int FRONT_CAMERA = 0;
	public final static int DELAY_MILLIS = 3500;
	
	public final static int SHAKE_THRESHOLD = 100;

	public final static int NONE = -100;

	public final static String SELECTED_TAB_BTN_ID = "selected button id";
	public final static String CURRENT_SCREEN = "current screen";
	public final static String CURRENT_INDICATOR_IMG = "current indicator image";
	public final static String CURRENT_INDICATOR_IMG_LEFT = "current indicator image left";
	public final static String CURRENT_INDICATOR_IMG_RIGHT = "current indicator image right";
	
	//Logged-in Status -- should just keep user logged in at all times after profile creation
	public final static String LOGGED_IN = "logged_in_status";
	public final static String STAY_LOGGED_IN = "keep_loggedIn";
	
	//Lists of images and friends to store -- dummy users is only for demo purposes
	public final static String SMIRK_IMAGES_LIST = "smirkImagesList";
	public final static String PENDING_FRIENDS_LIST = "pendingFriendsList";
	public final static String EXISTING_FRIENDS_LIST = "existingFriendsList";
	public final static String LIST_DUMMY_USERS = "dummyUsersList";
	
	//User attributes to store in onSaveInstanceState (and, later, in the backend DB)
	public final static String PROFILE_PICTURE = "profilePicture";
	public final static String USER_NAME = "userName";
	public final static String PASSWORD = "password";
	public final static String FULL_NAME = "fullName";
	public final static String EMAIL = "email";
	public final static String GENDER = "gender";
	public final static String DOB = "dateOfBirth";
	public final static String MAILING_LIST = "mailingList";
	public final static String CAMERA_PERMISSION = "cameraPermission";
	public final static String USER_SKIN_TONE = "skinTone";
	
	//Identifiers for Smirk expression components
	public final static String THUMBID = "thumbid";
	public final static String GIFID = "gifid";
	public final static String IS_ANIMATED = "isAnimated";
	
	public final static String NO_PICTURE = "noPicture";

	//public static String skinToneRGB = "";
	public static Bitmap profilePic;
	public static int appliedRotations;

	public static int[] getSkinToneRGB(int[] rgb) {
		String skinToneRGB = CommonData.skinTone;
		//Handle case where user is wanting to run the camera-game, but has not yet set the Skintone.
		if ((skinToneRGB.length() == 0) || (skinToneRGB.equals(""))){
			skinToneRGB = "255:255:255";} //set background skintone to white as default
		String[] sAry = skinToneRGB.split(":");
		int temp[] = new int[sAry.length];
		for (int i = 0; i < temp.length; i++) {
			temp[i] = -1;
		}
		for (int i = 0; i < sAry.length; i++) {
			if (TextUtils.isDigitsOnly(sAry[i])) {
				temp[i] = Integer.parseInt(sAry[i]);
			} else {
				if (sAry[i].contains("\n")) {
					String[] t = sAry[i].split("\n");
					temp[i] = Integer.parseInt(t[0]);
					for (int k = 0; k < t.length; k++) {
						if (TextUtils.isDigitsOnly(t[k])) {
							temp[i] = Integer.parseInt(t[k]);
						}
					}
				}
			}
		}
		for (int i = 0, index = 0; i < temp.length; i++) {
			if (temp[i] != -1) {
				rgb[index++] = temp[i];
			}
		}
		return rgb;
	}

	public static int dpToPx(Resources res, int dp) {
		return (int) TypedValue.applyDimension(TypedValue.COMPLEX_UNIT_DIP, dp, res.getDisplayMetrics());
	}
	
	public static Bitmap getRoundedCornerBitmap(Bitmap bitmap, int roundingPixels) {
		//Depending on degree of rounding passed in, can make a slightly rounded corner image from a 4-sided
		//bitmap or a fully circular image (as in profile pictures)
   	   Bitmap output = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), Config.ARGB_8888);
	   Canvas canvas = new Canvas(output);

	   final int color = 0xff424242;
	   final Paint paint = new Paint();
	   final Rect rect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
	   final RectF rectF = new RectF(rect);
	   final float roundPx = roundingPixels;

	   paint.setAntiAlias(true);
	   canvas.drawARGB(0, 0, 0, 0);
	   paint.setColor(color);
	   canvas.drawRoundRect(rectF, roundPx, roundPx, paint);

	   paint.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));
	   canvas.drawBitmap(bitmap, rect, rect, paint);

	   return output;
  }

	public static byte[] getBitmapAsByteArray(Bitmap bitmap) {
	    ByteArrayOutputStream outputStream = new ByteArrayOutputStream();
	    bitmap.compress(CompressFormat.PNG, 0, outputStream);       
	    return outputStream.toByteArray();
	}


	public static Bitmap RotateBitmap(Bitmap source, float angle)
	{
	      Matrix matrix = new Matrix();
	      matrix.postRotate(angle);
	      return Bitmap.createBitmap(source, 0, 0, source.getWidth(), source.getHeight(), matrix, true);
	}
	
	public static Bitmap scaleBitmap(Bitmap bitmapToScale, float newWidth, float newHeight) {   
		if(bitmapToScale == null)
		    return null;
		//get the original width and height
		int width = bitmapToScale.getWidth();
		int height = bitmapToScale.getHeight();
		// create a matrix for the manipulation
		Matrix matrix = new Matrix();

		// resize the bit map
		matrix.postScale(newWidth / width, newHeight / height);

		// recreate the new Bitmap and set it back
		return Bitmap.createBitmap(bitmapToScale, 0, 0, bitmapToScale.getWidth(), bitmapToScale.getHeight(), matrix, true);  
	}
	
	public static Bitmap scaleBitmapWithConfig(Bitmap bitmap, int newWidth, int newHeight) {
	  Bitmap scaledBitmap = Bitmap.createBitmap(newWidth, newHeight, Config.ARGB_8888);

	  float scaleX = newWidth / (float) bitmap.getWidth();
	  float scaleY = newHeight / (float) bitmap.getHeight();
	  float pivotX = 0;
	  float pivotY = 0;

	  Matrix scaleMatrix = new Matrix();
	  scaleMatrix.setScale(scaleX, scaleY, pivotX, pivotY);

	  Canvas canvas = new Canvas(scaledBitmap);
	  canvas.setMatrix(scaleMatrix);
	  canvas.drawBitmap(bitmap, 0, 0, new Paint(Paint.FILTER_BITMAP_FLAG));

	  return scaledBitmap;
	}
	
	public static Bitmap addRectangularFrame(Bitmap bmp, int borderSize, int colorChoice){
	    Bitmap bmpWithBorder = Bitmap.createBitmap(bmp.getWidth() + borderSize * 2, bmp.getHeight() + borderSize * 2, bmp.getConfig());
	    Canvas canvas = new Canvas(bmpWithBorder);
		if (colorChoice == 0){ canvas.drawColor(Color.BLACK); }
		else if (colorChoice == 1){ canvas.drawColor(Color.WHITE); }
		else if (colorChoice == 2){ canvas.drawColor(Color.TRANSPARENT); } //could use to add margins where the usual programmatic route is being finnicky
	    canvas.drawBitmap(bmp, borderSize, borderSize, null);
	    return bmpWithBorder;	
	}
	
	public static Bitmap addCircleFrame(Bitmap bitmap){
		//TODO: This automatically adds a black frame, but would like to put in some choice as paramter later
		int w = bitmap.getWidth();                                          
		int h = bitmap.getHeight();                                         

		int radius = Math.min(h / 2, w / 2);                                
		Bitmap output = Bitmap.createBitmap(w + 8, h + 8, Config.ARGB_8888);

		Paint p = new Paint();                                              
		p.setAntiAlias(true);                                               

		Canvas c = new Canvas(output);                                      
		c.drawARGB(0, 0, 0, 0);                                             
		p.setStyle(Style.FILL);                                             

		c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);                  

		p.setXfermode(new PorterDuffXfermode(Mode.SRC_IN));                 

		c.drawBitmap(bitmap, 4, 4, p);                                      
		p.setXfermode(null);                                                
		p.setStyle(Style.STROKE);                                           
		p.setColor(Color.BLACK);                                            
		p.setStrokeWidth(5);                                                
		c.drawCircle((w / 2) + 4, (h / 2) + 4, radius, p);                  

		return output;
	}
	
	public static byte[] bitmapToByteArr(Bitmap bitmap){
		ByteArrayOutputStream stream = new ByteArrayOutputStream();
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, stream);
		byte[] byteArray = stream.toByteArray();
		bitmap.recycle();	
		return byteArray;
	}
	
	public static byte[] readBytes(InputStream inputStream) throws IOException {
	  // this dynamically extends to take the bytes you read
	  ByteArrayOutputStream byteBuffer = new ByteArrayOutputStream();

	  // this is storage overwritten on each iteration with bytes
	  int bufferSize = 1024;
	  byte[] buffer = new byte[bufferSize];

	  // we need to know how may bytes were read to write them to the byteBuffer
	  int len = 0;
	  while ((len = inputStream.read(buffer)) != -1) {
	    byteBuffer.write(buffer, 0, len);
	  }

	  // and then we can return your byte array.
	  return byteBuffer.toByteArray();
	}

	public static byte[] getImageFromURLAndMakeByteArr(String url){
	     try {
	             URL imageUrl = new URL(url);
	             URLConnection ucon = imageUrl.openConnection();

	             InputStream is = ucon.getInputStream();
	             BufferedInputStream bis = new BufferedInputStream(is);

	             ByteArrayBuffer baf = new ByteArrayBuffer(500);
	             int current = 0;
	             while ((current = bis.read()) != -1) {
	                     baf.append((byte) current);
	             }

	             return baf.toByteArray();
	     } catch (Exception e) {
	             Log.d("ImageManager", "Error: " + e.toString());
	     }
	     return null;
	}

}
