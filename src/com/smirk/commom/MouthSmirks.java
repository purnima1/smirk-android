package com.smirk.commom;

import java.util.List;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.content.res.Resources;
import android.util.Log;

import com.smirk.R;

public class MouthSmirks{
	
	public static final String TAG = "MouthSmirks";
	
	private int thumbnailBMP;
	private int smirkGIF;
	
	public static List<MouthSmirks> initializeMouthSmirks(Context context){
		
		List<MouthSmirks> list = new ArrayList<MouthSmirks>();
		
		return list;
	}
	
	public MouthSmirks(int thumbnailBMP, int smirkGIF){
		this.thumbnailBMP = thumbnailBMP;
		this.smirkGIF = smirkGIF;
	}
	
	public int getThumbId(){
		return this.thumbnailBMP;
	}
	
	public int getGifId(){
		return this.smirkGIF;
	}

	private static int getGifFromThumbId(int thumbId){
		return 0;
	}
	
	
}