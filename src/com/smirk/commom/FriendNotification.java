package com.smirk.commom;

import java.util.ArrayList;
import java.util.List;

import android.graphics.drawable.Drawable;
import android.util.Log;

public class FriendNotification{
	
	private static final String TAG = "FriendNotification: ";
	
	public static int countHolder;
	public static boolean hasNew;
	
	private int notificationId;
	private String senderUserName;
	private String senderProfilePictureURL;
	private Drawable drawableProfilePicture;
	private int id;
	
	public static List<FriendNotification> notificationsList;
	
	public FriendNotification(int notificationId, String senderUserName, String senderProfilePictureURL, Drawable profilePicture, int id){
		this.notificationId = notificationId;
		this.senderUserName = senderUserName;
		this.senderProfilePictureURL = senderProfilePictureURL;
		this.drawableProfilePicture = profilePicture;
		this.id = id;
	}
	
	public int getId(){
		return this.id;
	}
	
	public int getNotificationId(){
		return this.notificationId;
	}
	
	public String getSenderUserName(){
		return this.senderUserName;
	}
	
	public String getSenderPictureURL(){
		return this.senderProfilePictureURL;
	}
	
	public Drawable getProfilePictureDrawable(){
		return this.drawableProfilePicture;
	}
	
	public static void setList(List<FriendNotification> parsedNotificationsList){
		Log.d(TAG, "setList called in FriendNotification");
		notificationsList = parsedNotificationsList;
		if (notificationsList == null){
			Log.d(TAG, "notifications list is null");	
		}
		if (notificationsList.size() == 0){
			Log.d(TAG, "length of notificaitons list is 0");
		}
		else{
			Log.d(TAG, "Element 0 in list has Id: "+notificationsList.get(0).notificationId);
		}
	}
	
	public static List<FriendNotification> getList(){
		return notificationsList;
	}
	
	public static void removeMember(String userName){
		for (int i=0; i<notificationsList.size(); i++){
			if (notificationsList.get(i).senderUserName.equals(userName)){
				notificationsList.remove(i);
			}
		}
	}
	
	public String toString(){
		return this.getNotificationId()+" "+this.getSenderUserName()+" "+this.getSenderPictureURL();
	}
	
}