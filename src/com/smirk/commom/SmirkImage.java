package com.smirk.commom;

import java.util.ArrayList;
import java.util.List;

import android.net.Uri;
import android.graphics.Bitmap;
import android.widget.ImageView;

public class SmirkImage{

	private Bitmap picture;	//bitmap reaction image
	private int smirkExpression; //resource id of smirk expression as .png drawable
	private int score; //score assigned to reaction
	private String imageUrl;
	
	public SmirkImage(Bitmap picture, int score, String imageUrl){
		this.picture = picture;
		this.score = score;
		this.imageUrl = imageUrl;
	}
	
	public String getUrl(){
		return this.imageUrl;
	}	
	
	public Bitmap getPicture(){
		return this.picture;}
		
	public int getSmirkExpression(){
		return this.smirkExpression;}
		
	public int getScore(){
		return this.score;}

	public void setPicture(Bitmap picture){
		this.picture = picture;}
		
	public void setSmirkExpression(int smirkExpression){
		this.smirkExpression = smirkExpression;}
		
	public void setScore(int score){
		this.score = score;}

	
		
}