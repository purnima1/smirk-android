package com.smirk.commom;

import android.app.Application;
import android.content.Context;

public class MyApplication extends Application {
	private static MyApplication mAppInstance;
	private static Context mAppContext;

	@Override
	public void onCreate() {
		// TODO Auto-generated method stub
		super.onCreate();
		mAppInstance = this;

		this.setAppContext(getApplicationContext());
	}

	public static MyApplication getInstance() {
		return mAppInstance;
	}

	public static Context getAppContext() {
		return mAppContext;
	}

	public static void setAppContext(Context mAppContext) {
		mAppContext = mAppContext;
	}

}
