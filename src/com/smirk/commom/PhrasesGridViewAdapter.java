package com.smirk.commom;

import java.util.List;
import java.util.ArrayList;

import android.widget.BaseAdapter;
import android.content.Context;
import android.content.Intent;
import android.widget.ImageView;
import android.widget.GridView;
import android.view.ViewGroup.LayoutParams;
import android.view.View;
import android.view.ViewGroup;
import android.view.View.OnClickListener;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.graphics.Bitmap;
import android.graphics.Matrix;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.graphics.BitmapFactory.Options;
import android.content.res.Resources;
import android.graphics.drawable.LayerDrawable;
import android.util.Log;

import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils;
import com.smirk.screens.PreviewSmirkActivity;

public class PhrasesGridViewAdapter extends BaseAdapter{
	private static final String TAG = "PhrasesGridViewAdapter";

	private Context mContext;
	private List<SmirkResource> phraseSmirks;
	private int mWidth = 150;
	private int mHeight = 100; //dip size for thumbnail gridview
	
	    public PhrasesGridViewAdapter(Context c, List<SmirkResource> phraseSmirks){
	      	this.mContext = c;
			this.phraseSmirks = phraseSmirks;
	    }

	    public int getCount() {
	        return phraseSmirks.size();
	    }

	    public SmirkResource getItem(int position) {
	        return phraseSmirks.get(position);
	    }

	    public long getItemId(int position) {
	        return 0;
	    }

	    // create a new ImageView for each item referenced by the Adapter
	    public View getView(int position, View convertView, ViewGroup parent) {
	        ImageView imageView;
			final int pos = position;
			
	        if (convertView == null) {  // if it's not recycled, initialize some attributes
	            imageView = new ImageView(mContext);
				imageView.setLayoutParams(new GridView.LayoutParams(mWidth, mHeight));
	            imageView.setScaleType(ImageView.ScaleType.FIT_CENTER);
	        } else {
	            imageView = (ImageView) convertView;
	        }
	
			//get the thumbnail as a bitmap
			Bitmap thumbnail = phraseSmirks.get(position).getThumbBitmap();;
			thumbnail = Utils.addRectangularFrame(thumbnail, 2, 0);
			imageView.setImageBitmap(thumbnail);
			imageView.setOnClickListener(new OnClickListener(){
			            @Override
			            public void onClick(View v) {
							onImageClick(pos);
			            }});

	        return imageView;
	    }
	
		public void onImageClick(int position){
			if (position != -1){
				SmirkResource smirkRes = phraseSmirks.get(position);
				int expressionType = smirkRes.getExpressionType();
				int id = phraseSmirks.get(position).getId();
				String gifUrl = SmirkResource.findGifUrl(expressionType, id);
				if (gifUrl != null && smirkRes.getGif() == null){ //first time using this gif
					SmirkResource.registerReceiver(mContext);
					SmirkResource.downloadGIF(expressionType, id, gifUrl, true); //launchPrev == true
				}
				else if (smirkRes.getGif() != null){ //not first time using this gif
					Intent previewIntent = new Intent(mContext, PreviewSmirkActivity.class);
					previewIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
					previewIntent.putExtra("EXP_TYPE", expressionType);
					previewIntent.putExtra("EXP_ID", id);
					previewIntent.putExtra(Utils.IS_ANIMATED, true);
					mContext.startActivity(previewIntent);
				}
			}
		}

	 
}
