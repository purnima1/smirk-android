package com.smirk.commom;

import java.util.List;
import java.util.ArrayList;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.widget.ImageView;
import android.content.res.Resources;
import android.util.Log;

import com.smirk.R;

//Data class to deal with Eye Smirks -- this will help us modularize the dealings with the Smirk indicators 
//and their respective GIFs
public class EyeSmirks{
	
	public static final String TAG = "EyeSmirks";

	private int thumbnailBMP;
	private int smirkGIF;
	
	public static List<EyeSmirks> initializeEyeSmirks(Context context){
		
		List<EyeSmirks> list = new ArrayList<EyeSmirks>();
		
		return list;
	}
	
	public EyeSmirks(int thumbnailBMP, int smirkGIF){
		this.thumbnailBMP = thumbnailBMP;
		this.smirkGIF = smirkGIF;
	}
	
	public int getThumbId(){
		return this.thumbnailBMP;
	}
	
	public int getGifId(){
		return this.smirkGIF;
	}

	private static int getGifFromThumbId(int thumbId){
		return 0;
	}
		
}