package com.smirk.commom;

import java.util.List;
import java.util.ArrayList;

import android.graphics.Bitmap;
import android.util.Log;

import com.smirk.commom.SmirkImage;

public class CommonData {
	
	private static final String TAG = "CommonData: ";
	
	public static String sessionId = "";
	public static String userName = "";
	public static String email;
	public static String password;
	public static int points;
	public static String profileImageServerLocation; 
	public static String profileImageDownloadUrl;
	public static Bitmap profilePicture;
	public static String skinTone;
	public static String name;
	public static String gender;
	public static String birthdate;
	public static int offerEmail;
	public static int useCamera;
	public static List<SmirkImage> userSmirkedPictures = new ArrayList<SmirkImage>();
	public static List<SmirkImage> userSharedPictures = new ArrayList<SmirkImage>();
	public static int smirkedPicsCount;
	public static int sharedPicsCount;
	public static int profilePicRotations;
	public static int resourceCount;
	
	public static void setSessionId(String sessId){
		sessionId = sessId;
	}
	
	public static void setUserEmail(String uemail){
		email = uemail;
	}
	
	public static void setUserPassword(String upassword){
		password = upassword;
	}
	
	public static void setProfilePictureRotations(int rotations){
		profilePicRotations = rotations;
	}

	public static void addUserSmirkPicToList(SmirkImage image){
		userSmirkedPictures.add(image);
	}
	
	public static void addSharedPicToList(SmirkImage image){
		userSharedPictures.add(image);
	}
	
	public static void setUserName(String uName){
		userName = uName;
	}
	
	public static void setUseCamera(String useCameraS){
		useCamera = Integer.parseInt(useCameraS);
	}
	
	public static void setOfferEmail(String offerEmailS){
		offerEmail = Integer.parseInt(offerEmailS);
	}
	
	public static void setBirthdate(String birthdate){
		birthdate = birthdate;
	}
	
	public static void setGender(String gender){
		gender = gender;
	}
	
	public static void setName(String name){
		name = name;
	}
	
	public static void setPoints(String pts){
		//Points will come in as String, but cast to to int here
		points = Integer.parseInt(pts);
	}
	
	public static void updatePoints(int addedPoints){
		//This is just to locally update accumulated points, so that every time the user returns to the HomeScreen, we're not
		//making a network call. Points also accumulate on the backend.
		points += addedPoints;
	}
	
	public static void setProfileImageServerLocation(String location){
		profileImageServerLocation = location;
	}
	
	public static void setProfileImageDownloadUrl(String downloadUrl){
		if (!downloadUrl.contains("http")){
			profileImageDownloadUrl = "http://"+downloadUrl;
		} else {
			profileImageDownloadUrl = downloadUrl;
		}
	}
	
	public static void setProfilePicture(Bitmap profilePictureBMP){
		profilePicture = profilePictureBMP;
	}
	
	public static void setSkinTone(String skin){
		skinTone = skin;
	}
	
	
}
