package com.smirk;

import java.util.List;
import java.util.ArrayList;
import java.io.InputStream;
import java.io.ByteArrayInputStream;

import com.smirk.commom.HUDView;
import android.database.Cursor;
import com.smirk.commom.Utils;
import com.smirk.commom.Utils.Screens;
import com.smirk.network.SmirkApiCalls;
import com.smirk.screens.ArchiveScreen;
import com.smirk.screens.FriendsScreen;
import com.smirk.screens.HomeScreen;
import com.smirk.screens.PlayScreen;
import com.smirk.screens.LoginScreen;
import com.smirk.screens.play.ReviewPictureScreen;
import com.smirk.screens.play.ScorePictureScreen;
import com.smirk.screens.PhotoOptionsScreen;
import com.smirk.commom.CommonData;
import com.smirk.commom.Constants;
import com.smirk.commom.SmirkImage;
import com.smirk.commom.EyeSmirks;
import com.smirk.commom.Friend;
import com.smirk.commom.FriendNotification;
import com.smirk.commom.database.*;
import com.smirk.commom.MyApplication;
import com.smirk.R;

import org.json.JSONObject;
import org.json.JSONException;

import android.app.Activity;
import android.content.BroadcastReceiver;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.graphics.BitmapFactory;
import android.graphics.Movie;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.preference.PreferenceManager;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.ImageButton;
import android.widget.TextView;
import android.widget.Toast;
import android.graphics.Bitmap;
import android.content.Context;

import com.android.volley.*;

public class SmirkMainActivity extends Activity implements OnClickListener{

	private final static String TAG = "SmirkMainActivity";
	
	private int[] mTabBtnId={R.id.tab_home_id, R.id.tab_play_id, R.id.tab_archive_id, R.id.tab_friends_id};
	private Button[] mTabBtns = new Button[Utils.NUMBER_OF_TABS];
	private ImageView mBarTitleIV;
	private int mSelectedTabBtnId;
	private int mColorBlack, mColorRed;
	
	private LinearLayout mBottomBar;

	/*
		Note on architecture: The original codebase had it set up so that ActionBar was never used.
		Instead, you'd have a manually laid-out LinearLayout with buttons to emulate the 
		action bar's tab layout -- this being a part of the main activity's layout file. 
		And then, rather than have each page (ie: HomeScreen, PlayScreen, etc.) be a fragment controlled
		by a listener applied to that actionbar tab setup, you just have those screens as
		Java classes that can make use of Activity methods and other components by passing
		in a reference to the main activity when first instantiating them. A slightly backward
		and possibly less-than-ideal set up, but it does work and perform nicely, and avoids
		all the issues now happening with trying to support the ActionBar as Android Lollipop
		comes into further market share. So I could have torn this odd bit of architecture out
		and set up the ActionBar, but I suppose this turned out to be an experiment in 
		alternative architectures for Android. --Rachelle Felzien
	*/

	private Screens mCurrentScreen;
	private HomeScreen mHomeScreen;
	private PlayScreen mPlayScreen;
	private FriendsScreen mFriendsScreen;
	private ReviewPictureScreen mReviewPictureScreen;
	private ArchiveScreen mArchiveScreen;
	private ScorePictureScreen mScorePictureScreen;
	private PhotoOptionsScreen mPhotoOptionsScreen;
	private ImageButton mModifyUserButton;

	@Override
	protected void onCreate (Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		Log.d(TAG, "MAIN ACTIVITY CREATED");
		
		//We're using SharedPreferences to give the feel of a real SSOManager; 
		//one was never provided as a backend service, so here's yet another hack.
		
		SharedPreferences prefs = this.getSharedPreferences("com.smirk", Context.MODE_PRIVATE);
		//SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		String savedEmail = prefs.getString("email", "null");
		String savedPassword = prefs.getString("password", "null");
		if (!savedEmail.equals("null") && !savedPassword.equals("null")) 
		{
			Log.d(TAG, "Shared prefs storage for email and pwd were not null: "+savedEmail+", "+savedPassword);
			prefsLogin(savedEmail, savedPassword);
			initializeUIAndStartHome();
		} else{
			Log.d(TAG, "Shared prefs storage was null; user must sign in or make new account");
			Intent intent = new Intent(SmirkMainActivity.this, LoginScreen.class);
			startActivity(intent);
		}
	}
	
	private void initializeUIAndStartHome(){
		//This activity_smirk_main layout contains the bottom toolbar and top "actionbar";
		//as previously stated, both these things could (and probably should) have been
		//structured using the real ActionBar for 3.0 / 4.0 systems. 
		//Layouts for each of the screens (PlayScreen, HomeScreen, Archives, Friends)
		//will basically get nested inside this master layout as ViewGroups, and then
		//removed and replaced when user goes to another screen.
		
		setContentView(R.layout.activity_smirk_main);
		
		mBottomBar = (LinearLayout) findViewById(R.id.tab_layout_id);
		
		mModifyUserButton = (ImageButton) findViewById(R.id.modify_user_button);

		mColorBlack = getResources().getColor(R.color.black);
		mColorRed = getResources().getColor(R.color.red);
		
		for (int i=0; i<mTabBtnId.length; i++){
			mTabBtns[i]=((Button)findViewById(mTabBtnId[i]));
			mTabBtns[i].setOnClickListener(this);
		}

		mBarTitleIV = (ImageView)findViewById(R.id.top_bar_title_iv_id);
		
		//First tab is the HomeScreen; turn its background red.
		mTabBtns[0].setBackgroundColor(mColorRed);
		
		//Instantiate the HomeScreen
		mHomeScreen = new HomeScreen(this);
		mHomeScreen.showScreen();
	}
	
	private void prefsLogin(String email, String password){
		JSONObject loginUser = new JSONObject();				
		try{
			loginUser.put("email", email);
	        loginUser.put("password", password);
	        Log.d(TAG, "Before calling the login api:" + loginUser);
	    	SmirkApiCalls.signLogInJsonObjectRequestQueue(this, Constants.LOGIN_URL, null, loginUser, false);
	    } catch (JSONException e){
			throw new RuntimeException(e);
		}
	}
	
	@Override
	public void onBackPressed() {
	    finish();
	}
	
	@Override
	protected void onDestroy(){
		super.onDestroy();
	}

	private void resetTabBtnBg(Button btn){
		for (int i=0; i<mTabBtns.length; i++){
			if(btn.equals(mTabBtns[i]))
				mTabBtns[i].setBackgroundColor(mColorRed);
			else
				mTabBtns[i].setBackgroundColor(mColorBlack);
		}
	}
	public PlayScreen getPlayScreen(){
		return mPlayScreen;
	}
	
	public FriendsScreen getFriendsScreen(){
		return mFriendsScreen;
	}
	
	public ArchiveScreen getArchiveScreen(){
		return mArchiveScreen;
	}
	
	public PhotoOptionsScreen getPhotoOptionsScreen(){
		return mPhotoOptionsScreen;
	}
	
	public ScorePictureScreen getScorePictureScreen(){
		return mScorePictureScreen;
	}
	
	public void setScorePictureScreen(ScorePictureScreen screen){
		mScorePictureScreen = screen;
	}
	
	public void setCurrentScreen(Screens screen){
		mCurrentScreen = screen;
	}
		
	public void hideBottomBar(){
		mBottomBar.setVisibility(View.GONE);
	}
	
	public void showBottomBar(){
		mBottomBar.setVisibility(View.VISIBLE);
	}

	@Override
	public void onClick(View v) {
		mSelectedTabBtnId=v.getId();
		if(v.getId() == R.id.tab_home_id){
			//mBarTitleTV.setText(this.getResources().getString(R.string.home));
			if(mHomeScreen == null){
				mHomeScreen = new HomeScreen(this);
			}
			mModifyUserButton.setVisibility(View.VISIBLE);
			mHomeScreen.showScreen();
		}
		else if(v.getId() == R.id.tab_play_id){
			//mBarTitleTV.setText(this.getResources().getString(R.string.time_to_smirk));
			if(mPlayScreen == null){
				mPlayScreen = new PlayScreen(this);
			}
			mModifyUserButton.setVisibility(View.INVISIBLE);
			mPlayScreen.showScreen();
		}
		else if(v.getId() == R.id.tab_archive_id){
			if (mArchiveScreen == null){
				mArchiveScreen = new ArchiveScreen(this);
			}
			mModifyUserButton.setVisibility(View.INVISIBLE);
			mArchiveScreen.showScreen(); //pass instance of the smirkImages list to ArchiveScreen
		}
		else if(v.getId() == R.id.tab_friends_id){
			if (mFriendsScreen == null){
				mFriendsScreen = new FriendsScreen(this);
			}
			mModifyUserButton.setVisibility(View.INVISIBLE);
			mFriendsScreen.showScreen();
		}
		resetTabBtnBg((Button)v);
	}
	
	protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        
        outState.putInt(Utils.SELECTED_TAB_BTN_ID, mSelectedTabBtnId);
        outState.putSerializable(Utils.CURRENT_SCREEN, mCurrentScreen);
    }

	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
		
		mSelectedTabBtnId=savedInstanceState.getInt(Utils.SELECTED_TAB_BTN_ID);
		mCurrentScreen = (Screens) savedInstanceState.getSerializable(Utils.CURRENT_SCREEN);
	}

	@Override
    protected void onStart() {
        super.onStart();
    }
    @Override
    protected void onResume() {
        super.onResume();
		
        Log.i(TAG, "onResume()**** "+Utils.isCameraActivityOnTop);
        
        if(Utils.isCameraActivityOnTop){
        	Utils.isCameraActivityOnTop = false;
        	if(mReviewPictureScreen==null)
        		mReviewPictureScreen = new ReviewPictureScreen(this);
        	mReviewPictureScreen.showScreen();
        	return;
        }
        Button btn = (Button)findViewById(mSelectedTabBtnId);
        if(btn!=null){
        	resetTabBtnBg(btn);
        }
     }

    @Override
    protected void onPause() {
        super.onPause();
    }
    @Override
    protected void onStop() {
        super.onStop();
    }	

}
