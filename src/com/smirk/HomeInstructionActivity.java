package com.smirk;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.view.PagerAdapter;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.TextView;
import android.content.Context;

import com.smirk.screens.home.InstructionHorizontalViewPage;
import com.smirk.screens.home.InstructionHorizontalViewPage.TransitionEffect;
import com.smirk.screens.home.OutlineContainer;
import com.viewpagerindicator.CirclePageIndicator;

public class HomeInstructionActivity extends Activity {
	private InstructionHorizontalViewPage mInstructionPage;
	private int[] mInsctrctionLayoutId = { R.layout.view_page_instruction_0, R.layout.view_page_instruction_1,
	    R.layout.view_page_instruction_2, R.layout.view_page_instruction_3, R.layout.view_page_instruction_4,
	    R.layout.view_page_instruction_5, R.layout.view_page_instruction_6, R.layout.view_page_instruction_7};

	private ImageButton mSubmitBtn;
	private HomeInstructionActivity mActivity;
	
	private CirclePageIndicator circleIndicator;

	Context mContext;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);

		requestWindowFeature(Window.FEATURE_NO_TITLE);
		setContentView(R.layout.activity_home_instruction);

		setupHomeInstrutionPage(TransitionEffect.Tablet);

		mActivity = this;
	}

	private void setupHomeInstrutionPage(TransitionEffect effect) {
		mInstructionPage = (InstructionHorizontalViewPage) findViewById(R.id.pager);
		mInstructionPage.setTransitionEffect(effect);
		mInstructionPage.setAdapter(new InstructionAdapter());
		mInstructionPage.setPageMargin(30);
	}

	private class InstructionAdapter extends PagerAdapter implements OnClickListener {

		CirclePageIndicator mIndicator;

		@Override
		public Object instantiateItem(ViewGroup container, final int position) {
			setCircleIndicator();

			ViewGroup view = (ViewGroup) getLayoutInflater().inflate(getLayoutId(position), container, false);
			container.addView(view);
			mInstructionPage.setObjectForPosition(view, position);

			if (getLayoutId(position) == R.layout.view_page_instruction_7) {
				mSubmitBtn = (ImageButton) view.findViewById(R.id.btn_gotit);
				mSubmitBtn.setOnClickListener(this);
			}
			return view;

		}


		public void setCircleIndicator(){
			//Bind the circle indicator to the adapter
			mIndicator = (CirclePageIndicator) findViewById(R.id.indicator);
			mIndicator.setViewPager(mInstructionPage);
	
			//Set circle indicator attributes
			final float density = getResources().getDisplayMetrics().density;
			//mIndicator.setBackgroundColor(0xFFCCCCCC);
			mIndicator.setRadius(7 * density);
			//mIndicator.setPageColor(0x880000FF);
			mIndicator.setFillColor(0xFF000000);
			mIndicator.setStrokeColor(0xFF000000);
			mIndicator.setStrokeWidth(2 * density);
			
		}

		private int getLayoutId(int position) {
			return mInsctrctionLayoutId[position];
		}

		@Override
		public void destroyItem(ViewGroup container, int position, Object obj) {
			container.removeView(mInstructionPage.findViewFromObject(position));
		}

		@Override
		public int getCount() {
			return mInsctrctionLayoutId.length;
		}

		@Override
		public boolean isViewFromObject(View view, Object obj) {
			if (view instanceof OutlineContainer) {
				return ((OutlineContainer) view).getChildAt(0) == obj;
			} else {
				return view == obj;
			}
		}

		@Override
		public void onClick(View v) {
			if (v.getId() == R.id.btn_gotit) {
				finish();
			}
		}
	}

}
