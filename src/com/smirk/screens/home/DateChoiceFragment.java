package com.smirk.screens.home;

import android.widget.DatePicker;
import android.widget.EditText;
import android.app.Dialog;
//import android.app.DialogFragment;
import android.app.DatePickerDialog;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.DialogFragment;

import java.util.Calendar;

public class DateChoiceFragment extends DialogFragment implements DatePickerDialog.OnDateSetListener {

	EditText textField;

	public DateChoiceFragment(EditText field){
		this.textField = field;} 

    @Override
    public Dialog onCreateDialog(Bundle savedInstanceState) {
	super.onCreateDialog(savedInstanceState);
        // Use the current date as the default date in the picker
        final Calendar c = Calendar.getInstance();
        int year = c.get(Calendar.YEAR);
        int month = c.get(Calendar.MONTH);
        int day = c.get(Calendar.DAY_OF_MONTH);

        // Create a new instance of DatePickerDialog and return it
        return new DatePickerDialog(this.getActivity(), this, year, month, day);
    }

    public void onDateSet(DatePicker view, int year, int month, int day) {
          String year1 = String.valueOf(year);
          String month1 = String.valueOf(month); //Need to check for single-digit values -- if so, append a 0
	  month1 = formatDate(month1);
          String day1 = String.valueOf(day); //Need to check for single-digit values -- if so, append a 0
	  day1 = formatDate(day1);
          textField.setText(year1+"-"+month1+"-"+day1);
    }

   public String formatDate(String datePortion){
	if (datePortion.length() < 2){
		datePortion = "0"+datePortion;}
	return datePortion;
   }
}



