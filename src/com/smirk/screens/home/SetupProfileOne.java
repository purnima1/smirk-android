package com.smirk.screens.home;

import java.io.InputStream;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.BufferedInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.content.Intent;
import android.app.Activity;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.EditText;
import android.widget.Button;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.net.Uri;
import android.provider.MediaStore;
import android.content.DialogInterface;


import com.smirk.HomeInstructionActivity;
import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils;
import com.smirk.screens.home.SetupProfile;
import com.smirk.screens.LoginScreen;
import com.smirk.screens.SetQuestions;

public class SetupProfileOne extends Activity implements OnClickListener {
	
	private static final int GET_PICTURE = 1;
	private int[] mImgBtnId = {R.id.add_profile_picture, R.id.next_setup_profile};

	private ImageButton mImgBtn;
	private ImageButton profilePictureButton;
	
	private EditText username_field, password_field, verify_password_field;
	
	private String username, password1, password2;
	
	private Bitmap profilePicture;
	private String profilePictureURIString;
	
	private Context context = SetupProfileOne.this;
	private int duration = Toast.LENGTH_LONG;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setup_profile_one);
		profilePictureButton = (ImageButton) findViewById(R.id.add_profile_picture);
		
		for (int i = 0; i < mImgBtnId.length; i++) {
			mImgBtn = (ImageButton) findViewById(mImgBtnId[i]);
			mImgBtn.setOnClickListener(this);
		}
		
		username_field = (EditText)findViewById(R.id.new_screen_name_edittext);
		password_field = (EditText)findViewById(R.id.new_password_edittext);
		verify_password_field = (EditText)findViewById(R.id.verify_new_password_edittext);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.add_profile_picture:
				Intent intent = new Intent(this, ProfilePicture.class);
				startActivityForResult(intent, GET_PICTURE);
				break;
			case R.id.next_setup_profile:
				username = username_field.getText().toString();
				password1 = password_field.getText().toString();
				password2 = verify_password_field.getText().toString();
				
				if (profilePicNotNull(profilePicture) && checkUserNameLength(username) && 
				checkPasswordsLength(password1) && checkPasswordsMatch(password1, password2)){				
					Intent nextSetup = new Intent(this, SetupProfileTwo.class);
					nextSetup.putExtra(Utils.USER_NAME, username);
					nextSetup.putExtra(Utils.PASSWORD, password1);
					nextSetup.putExtra(Utils.PROFILE_PICTURE, profilePictureURIString);
					startActivity(nextSetup);
					this.finish();}
				break;
			default:
				break;
		}	
	}
	
	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		if (requestCode == GET_PICTURE && resultCode == Activity.RESULT_OK){
			profilePictureURIString = data.getStringExtra("profilePicture");
			Uri picURI = Uri.parse(profilePictureURIString);
			int appliedRotations = data.getIntExtra("profilePictureRotations", 0);
			try{
			profilePicture = MediaStore.Images.Media.getBitmap(this.getContentResolver(), picURI);
			profilePicture = Bitmap.createScaledBitmap(profilePicture, 350, 350, false);
			profilePicture = Utils.getRoundedCornerBitmap(profilePicture, profilePicture.getWidth());
			Utils.appliedRotations = appliedRotations;
			if (appliedRotations == 0){
				Utils.profilePic = profilePicture;
				profilePictureButton.setBackgroundResource(0); //make the old frame thing null
				profilePicture = Utils.addCircleFrame(profilePicture);
				profilePictureButton.setImageBitmap(profilePicture);
				//Drawable d = new BitmapDrawable(getResources(), profilePicture);
				profilePictureButton.setImageBitmap(profilePicture);
			} else {
				for (int i=0; i<appliedRotations; i++){
					profilePicture = Utils.RotateBitmap(profilePicture, 90);
				}
				Utils.profilePic = profilePicture;
				//For some reason, just on the smaller device, the profile picture shows up too small
				//on this screen. It looks fine on the HomeScreen. Blow it up to 2x size for display here.
				profilePicture =  Utils.scaleBitmap(profilePicture, 300, 300);
				profilePicture = Utils.addCircleFrame(profilePicture);
				profilePictureButton.setBackgroundResource(0); //make the old frame thing null
				//Drawable d = new BitmapDrawable(getResources(), profilePicture);
				profilePictureButton.setImageBitmap(profilePicture);
			}
    	} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		}
	}
	
	private boolean checkUserNameNoSpecialChars(String username){
		if (username.contains("@")){ 
			CharSequence output = "Please no @ symbols in the usernames!";
			Toast toast = Toast.makeText(context, output, duration);
			toast.show();
			return false;}
		else{ return true; }
	}
	
	private boolean profilePicNotNull(Bitmap profilePicture){
		if (profilePicture != null){ return true;}
		else{	
			CharSequence output = "Please take or choose a profile picture!";
			Toast toast = Toast.makeText(context, output, duration);
			toast.show(); 
			return false; }
		}
	
	private boolean checkUserNameLength(String username){
		if (username.length() >= 20){
			CharSequence output = "Screen name must be 20 characters or less";
			Toast toast = Toast.makeText(context, output, duration);
			toast.show(); 
			return false; }
		else { return true; }
	}
	
	private boolean checkPasswordsLength(String password1){
		if (password1.length() >= 6 && password1.length() <= 8){ return true; }
		else{ 
			CharSequence output = "Password must be between 6 and 8 characters";
			Toast toast = Toast.makeText(context, output, duration);
			toast.show();
			return false; }
	}
	
	private boolean checkPasswordsMatch(String password1, String password2){
		if (password1.equals(password2)){ return true;}
		else{
		 	CharSequence output = "Passwords must match; enter again";
			Toast toast = Toast.makeText(context, output, duration);
			toast.show();
			return false; }
	}
	
	@Override
	public void onBackPressed() {
	    Intent intent = new Intent(SetupProfileOne.this, LoginScreen.class);
		startActivity(intent);
	    finish();
	}

}