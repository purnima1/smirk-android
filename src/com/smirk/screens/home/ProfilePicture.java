package com.smirk.screens.home;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileOutputStream;
import java.io.File;

import android.app.Activity;
import android.hardware.Camera;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Paint;
import android.graphics.Rect;
import android.graphics.Matrix;
import android.graphics.PorterDuffXfermode;
import android.graphics.PorterDuff.Mode;
import android.graphics.RectF;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.ImageButton;
import android.widget.Button;
import android.widget.LinearLayout;
import android.view.View.OnClickListener;
import android.provider.MediaStore;
import android.provider.MediaStore.Files.FileColumns;
import android.net.Uri;
import android.util.Log;
import android.database.Cursor;
import android.os.Environment;

import com.smirk.R;
import com.smirk.commom.Utils;
import com.smirk.commom.CommonData;
import com.smirk.screens.LoginScreen;

public class ProfilePicture extends Activity implements OnClickListener{
	  private static final String TAG = "ProfilePicture";
	  private static final int REQUEST_CODE = 1;
	  private static final int GET_PICTURE = 1;
	  private static final int TAKE_PICTURE = 2;
	  private static final int MEDIA_TYPE_IMAGE = 1;
	  private Bitmap bitmap;
	  private ImageView imageView;
	  private Uri selectedImageURI;
	  private Uri capturedImageURI;
	  private int appliedRotations;

	  private int[] mBtnId = {R.id.button_take_selfie, R.id.button_select_existing, R.id.button_select_picture, 
		R.id.rotate_profile_pic};
	  private ImageButton mBtn;
	  private ImageButton next;
	  private ImageButton rotateArrow;
	  private LinearLayout currentView;
	
  @Override
  public void onCreate(Bundle savedInstanceState) {
    super.onCreate(savedInstanceState);
    setContentView(R.layout.profile_picture);
	imageView = (ImageView) findViewById(R.id.profile_pic_frame);
	currentView = (LinearLayout) findViewById(R.id.profile_pic_view);
	rotateArrow = (ImageButton) findViewById(R.id.rotate_profile_pic);
	appliedRotations = 0;
	 
	for (int i = 0; i < mBtnId.length; i++) {
		mBtn = (ImageButton) findViewById(mBtnId[i]);
		mBtn.setOnClickListener(this);
	}
  }

  @Override
  public void onClick(View v) {
	switch (v.getId()) {
		case R.id.button_take_selfie:
			Intent imageIntent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
       		File imagesFolder = new File(Environment.getExternalStorageDirectory(), "SmirkImages");
	        imagesFolder.mkdirs(); // <----
	        File image = new File(imagesFolder, "SmirkProfilePic.bmp");
	        capturedImageURI = Uri.fromFile(image);
	        imageIntent.putExtra(MediaStore.EXTRA_OUTPUT, capturedImageURI);
	        startActivityForResult(imageIntent, TAKE_PICTURE);
			break;
		case R.id.button_select_existing:	
  		   Intent intent = new Intent();
		   intent.setType("image/*");
		   intent.setAction(Intent.ACTION_GET_CONTENT);
		   intent.addCategory(Intent.CATEGORY_OPENABLE);
		   startActivityForResult(intent, REQUEST_CODE);
		   break;
		case R.id.button_select_picture:
			selectThisPicture();
		case R.id.rotate_profile_pic:
			rotatePicture();
		default:
			break;
	}
  }

	@Override
	protected void onActivityResult(int requestCode, int resultCode, Intent data){
		InputStream inputStream = null;
		if (requestCode ==  REQUEST_CODE && resultCode == Activity.RESULT_OK){
			//User chose picture from library
			try{ //recycle unused bitmaps
				if (bitmap != null){ bitmap.recycle();}
				selectedImageURI = data.getData();
				Log.d(TAG,"Image URI being handed to getRealPath: "+selectedImageURI);
				BitmapFactory.Options o = new BitmapFactory.Options();
				o.inSampleSize = 4; //few things from SOF posters recommended to avoid out of memory errors
				o.inDither=false;                   
				o.inPurgeable=true;                   
				bitmap = BitmapFactory.decodeFile(getRealPathFromURI(selectedImageURI));
				Log.d(TAG,"Real Path computed: "+getRealPathFromURI(selectedImageURI));
				//Resize chosen bitmap down to something suitable for a profile picture -- 120x120 px
				
				int currentapiVersion = android.os.Build.VERSION.SDK_INT;
				if (currentapiVersion > android.os.Build.VERSION_CODES.JELLY_BEAN){
					bitmap = Bitmap.createScaledBitmap(bitmap, 350, 350, false);
				}
				else{
					bitmap = Bitmap.createScaledBitmap(bitmap, 350, 350, false);
				}
				bitmap = Utils.getRoundedCornerBitmap(bitmap, bitmap.getWidth());
				bitmap = Utils.addCircleFrame(bitmap);
				imageView.setImageBitmap(bitmap);
				rotateArrow.setImageDrawable(getResources().getDrawable(R.drawable.arrows));
			}
			
			catch (Exception e){
				e.printStackTrace();}
			finally {
				if (inputStream != null){
					try{inputStream.close();}
					catch (IOException e){ e.printStackTrace();}	
				}	
			}	
		}
		
		else if (requestCode == TAKE_PICTURE && resultCode == Activity.RESULT_OK){
				bitmap = BitmapFactory.decodeFile(getRealPathFromURI(capturedImageURI));
				selectedImageURI = capturedImageURI;
				bitmap = Bitmap.createScaledBitmap(bitmap, 350, 350, false);
				bitmap = Utils.getRoundedCornerBitmap(bitmap, bitmap.getWidth());
				bitmap = Utils.addCircleFrame(bitmap);
				imageView.setImageBitmap(bitmap);
				CommonData.setProfilePicture(bitmap);
				rotateArrow.setImageResource(R.drawable.arrows);
		}	
	}
	
	private void rotatePicture(){
		bitmap = Utils.RotateBitmap(bitmap, 90);
		appliedRotations++;
		imageView.setImageBitmap(bitmap);
	}
	
	private void selectThisPicture(){
		if (bitmap != null){
			Intent resultIntent = new Intent();
			//Send URI instead to circle-cropped and rotated BMP which is saved on the SD card
			resultIntent.putExtra("profilePicture", selectedImageURI.toString());
			resultIntent.putExtra("profilePictureRotations", appliedRotations);
			CommonData.setProfilePictureRotations(appliedRotations);
			setResult(Activity.RESULT_OK, resultIntent);
			this.finish();
			 
			}
		else{
			CharSequence popUp = "Please take or select a picture!";
			int duration = Toast.LENGTH_SHORT;
			Context context = ProfilePicture.this;
			Toast toast = Toast.makeText(context, popUp, duration);
			toast.show();}
	}
	
	public String getRealPathFromURI(Uri contentURI) {
	    String result = "";
	    Cursor cursor = getContentResolver().query(contentURI, null, null, null, null);
		Log.d(TAG,"Content URI: "+contentURI);
		if (contentURI.toString().contains("com.sec.android.gallery3d.provider/picasa")){
			CharSequence popUp = "Whoops -- there was an error retrieving that image file. Please choose another!";
			int duration = Toast.LENGTH_LONG;
			Context context = ProfilePicture.this;
			Toast toast = Toast.makeText(context, popUp, duration);
			toast.show();
			result = contentURI.getPath();
		} else{
		    if (cursor == null) { // Source is Dropbox or other similar local file path
		        result = contentURI.getPath();
				Log.d(TAG,"Image URI: "+result);
		    } else { 
		        cursor.moveToFirst(); 
		        int idx = cursor.getColumnIndex(MediaStore.Images.ImageColumns.DATA); 
		        result = cursor.getString(idx);
				Log.d(TAG,"Image URI: "+result);
		        cursor.close();
		    }
		}
	    return result;
	}	
	
	
	@Override
	public void onBackPressed() {
		Intent intent = new Intent(ProfilePicture.this, LoginScreen.class);
		startActivity(intent);
	    finish();
	}	
}	