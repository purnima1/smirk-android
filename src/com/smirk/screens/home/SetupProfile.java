package com.smirk.screens.home;

import android.app.Dialog;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;
import android.widget.EditText;
import android.view.View;
import android.app.DialogFragment;
import android.widget.DatePicker;
import android.app.DatePickerDialog;
import android.app.Activity;

import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.screens.HomeScreen;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentActivity;

public class SetupProfile extends Activity{
	
	//private HomeActivity mActivity;
	private HomeScreen mHomeScreen; 
	private SmirkMainActivity mActivity;
	EditText dateOfBirth;

	public SetupProfile(SmirkMainActivity activity){
		//Will need to modify the view of the Home Screenß
		//HomeScreen mHomeScreen = homeScreen;
		mActivity = activity;
	}
	
	
	public void showSetupProfileDialog(){
		Dialog dialog = new Dialog(mActivity, R.style.ThemeDialogCustom);
		dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
	    dialog.setContentView(R.layout.setup_profile);
	    dialog.setCancelable(true);
	    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(dialog.getWindow().getAttributes());
	    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
	    lp.height = WindowManager.LayoutParams.MATCH_PARENT;

	    dialog.show();
	    dialog.getWindow().setAttributes(lp);	    
	}
	/*
	public void launchDatePickerDialog(){
		//Launch the datepicker
		// Create new fragment and transaction
		View v = this.findViewById(R.id.date_of_birth);
		EditText dateOfBirth = v.findViewById(R.id.date_of_birth);
		DateChoiceFragment newFragment = new DateChoiceFragment(dateOfBirth);
		//FragmentManager fragManager = mActivity.getApplicationContext().getSupportFragmentManager();
		newFragment.show(getFragmentManager(), "datePicker");
	}
	*/
}
