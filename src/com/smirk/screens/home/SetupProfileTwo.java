package com.smirk.screens.home;

import java.util.Calendar;
import java.util.Date;
import java.util.GregorianCalendar;

import android.content.Intent;
import android.app.Activity;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.CheckBox;
import android.widget.Toast;
import android.widget.Spinner;
import android.widget.ArrayAdapter;
import android.widget.EditText;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.os.AsyncTask;
import android.net.Uri;
import android.provider.MediaStore;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.util.Log;

import com.smirk.R;
import com.smirk.commom.Utils;
import com.smirk.SkinToneActivity;
import com.smirk.screens.LoginScreen;
import com.smirk.network.SmirkApiCalls;

public class SetupProfileTwo extends Activity implements OnClickListener {
	
	private static final String TAG = "SetupProfileTwo: ";

	private static int duration = Toast.LENGTH_LONG;
	Calendar cal = Calendar.getInstance();
	private int[] mImgBtnId = {R.id.next_setup_profile_two, R.id.facebook_login_button};
	private ImageButton mImgBtn;
	
	private EditText fullNameField, emailField1, emailField2, yearSuffixField;
	
	private String username, password, fullName, email, email1, email2, gender, month, day, yearPrefix, yearSuffix, dateOfBirth;
	
	private Bitmap profilePicture;
	private String profilePictureURIString;
	
	private int emailUpdates = 0;
	private int cameraAccess = 0;
	
	private CheckBox emailUpdatesBox;
	
	Spinner gender_spinner, month_spinner, day_spinner, year_prefix_spinner;
	
	private BroadcastReceiver checkDuplicateEmail = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.d(TAG, "Received duplicate email checker broadcast");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String res = intent.getStringExtra("EMAIL_DUPLICATE");
				if (res.equals("YES")){
					Toast.makeText(getApplicationContext(), "Email already exists!", Toast.LENGTH_LONG).show();
				} else{
					packAndFinish();
				}
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.setup_profile_two);
		
		registerReceiver(checkDuplicateEmail, new IntentFilter("DUPLICATE_EMAIL_CHECK"));
		
		username = getIntent().getStringExtra(Utils.USER_NAME);
		password = getIntent().getStringExtra(Utils.PASSWORD);
		profilePictureURIString = getIntent().getStringExtra(Utils.PROFILE_PICTURE); //this didn't throw a Java binder error when running on my two samsung devices. Will test on other devices though.
		
		fullNameField = (EditText) findViewById(R.id.full_name_edittext);
		emailField1 = (EditText) findViewById(R.id.new_email_edittext);
		emailField2 = (EditText) findViewById(R.id.verify_new_email_edittext);
		yearSuffixField = (EditText) findViewById(R.id.year_edittext);
		
		emailUpdatesBox = (CheckBox) findViewById(R.id.receive_email_updates);
		
		for (int i = 0; i < mImgBtnId.length; i++) {
			mImgBtn = (ImageButton) findViewById(mImgBtnId[i]);
			mImgBtn.setOnClickListener(this);
		}
		
		//Set up gender choice spinner
		gender_spinner = (Spinner) findViewById(R.id.gender_spinner);
		ArrayAdapter<CharSequence> gender_adapter = ArrayAdapter.createFromResource(this,
		        R.array.genders_array, android.R.layout.simple_spinner_item);
		gender_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		gender_spinner.setAdapter(gender_adapter);
		
		//Set up month choice spinner
		month_spinner = (Spinner) findViewById(R.id.month_spinner);
		ArrayAdapter<CharSequence> month_adapter = ArrayAdapter.createFromResource(this,
		        R.array.months_array, android.R.layout.simple_spinner_item);
		month_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		month_spinner.setAdapter(month_adapter);
		
		//Set up day choice spinner
		day_spinner = (Spinner) findViewById(R.id.day_spinner);
		ArrayAdapter<CharSequence> day_adapter = ArrayAdapter.createFromResource(this,
		        R.array.days_array, android.R.layout.simple_spinner_item);
		day_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		day_spinner.setAdapter(day_adapter);
		
		//Set up year prefix choice spinner
		year_prefix_spinner = (Spinner) findViewById(R.id.year_prefix_spinner);
		ArrayAdapter<CharSequence> year_prefix_adapter = ArrayAdapter.createFromResource(this,
		        R.array.year_prefixes_array, android.R.layout.simple_spinner_item);
		year_prefix_adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item);
		year_prefix_spinner.setAdapter(year_prefix_adapter);

	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.next_setup_profile_two:
				 //Get stuff from the Spinners and Edittexts and CheckBoxes
				 fullName = fullNameField.getText().toString();
				 email1 = emailField1.getText().toString();
				 email2 = emailField2.getText().toString();
				 gender = gender_spinner.getSelectedItem().toString();
				 month = month_spinner.getSelectedItem().toString();
				 day = day_spinner.getSelectedItem().toString();
				 yearPrefix = year_prefix_spinner.getSelectedItem().toString();
				 yearSuffix = yearSuffixField.getText().toString();
				
				 if (emailUpdatesBox.isChecked()){ emailUpdates = 1;}
				
				if (yearSuffix.equals("")){ yearSuffix = "14"; }
				
		        cameraAccess = 1;
		
				if (gender.equals("Male")){gender = "M";}
				else{gender = "F";}
		
				 if (verifyEmail(email1) && matchEmails(email1, email2) && dateNotFuture(month, day, yearPrefix, yearSuffix)){
						email = email1;
						dateOfBirth = yearPrefix+yearSuffix+"-"+month+"-"+day;	
						SmirkApiCalls.checkDuplicateEmail(getApplicationContext(), email);
					}
				 break;
			case R.id.facebook_login_button:
				Intent intent = new Intent(SetupProfileTwo.this, LoginScreen.class);
				intent.putExtra("SETUP_PROFILE_TWO_FB", true);
				startActivity(intent);
				finish();
			default:
				break;
		}
	}
	
	private void packAndFinish(){
			//Pack into bundle
			unregisterReceiver(checkDuplicateEmail);
			
			Intent intent = new Intent(this, SkinToneActivity.class);
			intent.putExtra(Utils.USER_NAME, username);
			intent.putExtra(Utils.PASSWORD, password);
			intent.putExtra(Utils.PROFILE_PICTURE, profilePictureURIString);
			intent.putExtra(Utils.FULL_NAME, fullName);
			intent.putExtra(Utils.EMAIL, email);
			intent.putExtra(Utils.GENDER, gender);
			intent.putExtra(Utils.DOB, dateOfBirth);
			intent.putExtra(Utils.MAILING_LIST, emailUpdates);
			intent.putExtra(Utils.CAMERA_PERMISSION, cameraAccess);
			startActivity(intent);
			this.finish();
	}
	
	private boolean dateNotFuture(String month, String day, String yearPrefix, String yearSuffix){
		Date now = cal.getTime();
		int y = Integer.parseInt(yearPrefix+yearSuffix);
		int m = Integer.parseInt(month);
		int d = Integer.parseInt(day);
		
		GregorianCalendar givenDate = new GregorianCalendar(y, m, d);
		if (givenDate.after(now)){ 
			CharSequence output = "Your date of birth can't be past the current date --  at least, not in the present space-time architecture";
			Toast toast = Toast.makeText(SetupProfileTwo.this, output, duration);
			toast.show();
			return false;
		} else {
			return true;}
	}	
	
	private boolean matchEmails(String email1, String email2){
		if (email1.equals(email2)){ return true; }
		else{
			CharSequence output = "Email addresses must match!";
			Toast toast = Toast.makeText(SetupProfileTwo.this, output, duration);
			toast.show();
			return false;
		}
	}
	
	private boolean verifyEmail(String email){
		if (email.contains("@") && email.contains(".")){ return true; }
		else{
				CharSequence output = "Please enter a well-formed email address";
				Toast toast = Toast.makeText(SetupProfileTwo.this, output, duration);
				toast.show();
				return false;}
	}
	
	@Override
	public void onBackPressed() {
	    Intent intent = new Intent(SetupProfileTwo.this, SetupProfileOne.class);
		startActivity(intent);
	    finish();
	}
	
}	