package com.smirk.screens;

import java.util.Arrays;

import android.content.Intent;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.SharedPreferences;
import android.content.SharedPreferences.Editor;
import android.view.*;
import android.preference.PreferenceManager;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.EditText;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.widget.CheckBox;
import android.database.Cursor;
import android.util.Log;

import org.json.JSONObject;
import org.json.JSONException;

import org.apache.http.protocol.HTTP;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import com.android.volley.Request.Method;

import com.smirk.HomeInstructionActivity;
import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils;
import com.smirk.commom.Utils.Screens;
import com.smirk.network.SmirkApiCalls;
import com.smirk.screens.home.SetupProfileOne;
import com.smirk.commom.database.*;
import com.smirk.commom.Constants;
import com.smirk.commom.CommonData;
import com.smirk.commom.VolleyController;

import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;

import com.facebook.widget.LoginButton;
import com.facebook.model.GraphUser;
import com.facebook.Response;
import com.facebook.Request;
import com.facebook.Request.GraphUserCallback;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Session.StatusCallback;
import com.facebook.UiLifecycleHelper;

public class LoginScreen extends Activity implements OnClickListener {
	
	private final static String TAG = "LoginScreen";
	
	private final static int FACEBOOK_LOGIN = 1;
	
	private UiLifecycleHelper uiHelper;
	
	private int[] mImgBtnId = {R.id.button_submit_login, R.id.forgot_password_link, R.id.create_new_profile_button};

	private ImageButton mImgBtn;
	
	private EditText username_field, password_field;
	private CheckBox stay_logged_in_button;
	
	private String email, password, id, profilePicUrl, firstName;
	private boolean stay_logged_in;
	
	LoginButton mFacebookLogin;
	
	Context context;
	int duration; //for Toast messages
	
	private boolean broadcastReceived;
	private boolean backFromSetupProfileTwo; //if user said FB login on 2nd profile setup page
	
	private Request.GraphUserCallback userCallBack = new Request.GraphUserCallback() {
		@Override
        public void onCompleted(GraphUser user, Response response){
			if (user != null){
				id = user.getId(); //get the id so we can download the profile picture
				email = user.getProperty("email").toString(); //email for signin
				password = "facebookuser"; //default password
				firstName = user.getFirstName();
				profilePicUrl = "https://graph.facebook.com/"+id+"/picture?type=large";
				
				Log.d(TAG, "Email: "+email);
				Log.d(TAG, "Firstname: "+firstName);
				Log.d(TAG, "Profile pic URL: "+profilePicUrl);
				
				tryFBLogin(email, password);
			}
			else{
				Log.d(TAG, "User was null");
			}
		}
	};
	
	private Session.StatusCallback statusCallback = new Session.StatusCallback() {
	    @Override
	    public void call(Session session, SessionState state, Exception exception) {
			Log.d(TAG, "Facebook Session status changed");
			//Open session and check state. If closed, then we couldn't get in 
			//for some reason (create notification). Otherwise, make login
			//request and get the user's information
			if (!session.isOpened()){
				Log.d(TAG, "Facebook Session was closed in the status callback");
				Session.OpenRequest openRequest = new Session.OpenRequest(LoginScreen.this);
				session.openForRead(openRequest);
				Request userRequest = Request.newMeRequest(session, userCallBack);
				userRequest.executeAsync();
			} else {
				Request userRequest = Request.newMeRequest(session, userCallBack);
				userRequest.executeAsync();
			}
	    }
	};
	
	private BroadcastReceiver signLogBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.d(TAG, "signLog - BR onReceive");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String res = intent.getStringExtra("AUTH_OKAY");  
				Log.d(TAG, " inside onReceive : " + res);
				if (res.contains("FIRSTTIMEFB")){
					//Login wasn't successful, because this is our first time and we must establish a user
					firstTimeFBSignup();
				}
				else if (res.contains("OK")){
					//This wasn't our first time, so we can login with what we have
					saveEmailAndPassword();
					Intent start = new Intent(LoginScreen.this, SmirkMainActivity.class);
					startActivity(start);
					finish();
				}
			}
		}
	};
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.login_screen);
		
		Intent intent = getIntent(); //Setup profile two lets user come back here for FB login
		if (intent.getExtras() != null){
			backFromSetupProfileTwo = intent.getBooleanExtra("SETUP_PROFILE_TWO_FB", false);
			if (backFromSetupProfileTwo){ onClickLogin(); }
		}
		
		uiHelper = new UiLifecycleHelper(this, statusCallback);
	    uiHelper.onCreate(savedInstanceState);
		
		for (int i = 0; i < mImgBtnId.length; i++) {
			mImgBtn = (ImageButton) findViewById(mImgBtnId[i]);
			mImgBtn.setOnClickListener(this);
		}
		
		username_field = (EditText)findViewById(R.id.login_screen_name_edittext);
		password_field = (EditText)findViewById(R.id.login_password_edittext);
		
		mFacebookLogin = (LoginButton) findViewById(R.id.facebook_login_button);
		mFacebookLogin.setReadPermissions(Arrays.asList("public_profile", "email"));
		
		context = getApplicationContext();
		duration = Toast.LENGTH_LONG;
		
	}
	
	@Override
	public void onBackPressed() {
	    finish();
	}
	
	private void saveEmailAndPassword(){
		//By the time we're calling this, email and password will have been set
		String savedEmail = CommonData.email;
		String savedPassword = CommonData.password;
		
		//SharedPreferences prefs = PreferenceManager.getDefaultSharedPreferences(getApplicationContext());
		SharedPreferences prefs = this.getSharedPreferences("com.smirk", Context.MODE_PRIVATE);
		SharedPreferences.Editor editor = prefs.edit();
		editor.putString("email", savedEmail);
		editor.putString("password", savedPassword);
		editor.apply();
	}
	
	private void generalLogin(String email, String password){
		JSONObject loginUser = new JSONObject();				
		try{
			loginUser.put("email", email);
	        loginUser.put("password", password);
	        Log.d(TAG, "Before calling the login api:" + loginUser);
	    	SmirkApiCalls.signLogInJsonObjectRequestQueue(this, Constants.LOGIN_URL, null, loginUser, false);
	    } catch (JSONException e){
			throw new RuntimeException(e);
		}
	}
	
	private void tryFBLogin(String email, String password){
		JSONObject facebookUser = new JSONObject();				
		try{
			facebookUser.put("email", email);
	       	facebookUser.put("password", password);
		} catch (JSONException e){
			e.printStackTrace();
		}
		SmirkApiCalls.signLogInJsonObjectRequestQueue(LoginScreen.this, Constants.LOGIN_URL, null, facebookUser, true);
	}
	
	private void firstTimeFBSignup(){
		Log.d(TAG, "Reached firstTimeSignup");
		JSONObject facebookUser = new JSONObject();				
		try{
			facebookUser.put("email", email);
	       	facebookUser.put("password", password);
			facebookUser.put("screenName", firstName);
			facebookUser.put("profilePicture", profilePicUrl);
			
			//Put dummy or empty data for the remainder of the user fields
			facebookUser.put("skinTone", "255:255:255");
			facebookUser.put("name", "Facebook User");
			facebookUser.put("gender", "M");
			facebookUser.put("birthDate", "2014-10-09");
			facebookUser.put("offerEmail", 1);
			facebookUser.put("useCamera", 1);
			
			CommonData.setProfileImageDownloadUrl(profilePicUrl);
	    	SmirkApiCalls.signLogInJsonObjectRequestQueue(LoginScreen.this, Constants.SIGNUP_URL, null, facebookUser, true);
			
	    } catch (JSONException e){
			throw new RuntimeException(e);
		}
		
	}
	
	private void onClickLogin() {
	    Session session = Session.getActiveSession();
	    if (!session.isOpened() && !session.isClosed()) {
			Log.d(TAG, "Session was active but not opened or closed");
	        session.openForRead(new Session.OpenRequest(this)
	            .setPermissions(Arrays.asList("public_profile", "email"))
	            .setCallback(statusCallback));
	    } else {
			Log.d(TAG, "Session was not active");
	        Session.openActiveSession(LoginScreen.this, true, null, statusCallback);
	    }
	}
	
	@Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        Session.getActiveSession().onActivityResult(LoginScreen.this, requestCode, resultCode, data);
    }
	
	@Override
	public void onResume() {
	    super.onResume();
	    uiHelper.onResume();
		registerReceiver(signLogBroadcastReceiver, new IntentFilter("LOGIN"));
	}
	
	@Override
	protected void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		uiHelper.onPause();
		unregisterReceiver(signLogBroadcastReceiver);
	}
	
	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}
	
	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}
	
	private void forgotPassword(){
		Intent forgotPwd = new Intent(this, ForgotPassword.class);
		startActivity(forgotPwd);
	}
	
	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.button_submit_login:
				email = username_field.getText().toString();
				password = password_field.getText().toString();

				JSONObject loginUser = new JSONObject();				
				try{
					loginUser.put("email", email);
			        loginUser.put("password", password);
			        Log.d(TAG, "before calling the login api" + loginUser);
			    	SmirkApiCalls.signLogInJsonObjectRequestQueue(this, Constants.LOGIN_URL, null, loginUser, false);
			    } catch (JSONException e){
					e.printStackTrace();
				}
				break;
			case R.id.forgot_password_link:
				forgotPassword();
			case R.id.facebook_login_button:
				break;
			case R.id.create_new_profile_button:
				Intent intent = new Intent(this, SetupProfileOne.class);
				startActivity(intent);
				finish();
				break;
			default:
				break;
		}
		
	}


}