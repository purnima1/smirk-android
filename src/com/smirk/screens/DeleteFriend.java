package com.smirk.screens;

import java.util.Set;
import java.util.HashSet;
import java.util.List;
import java.util.ArrayList;

import android.app.DialogFragment;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.graphics.drawable.Drawable;

import com.smirk.R;
import com.smirk.commom.CommonData;
import com.smirk.commom.Constants;
import com.smirk.commom.Utils;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Friend;
import com.smirk.network.SmirkApiCalls;

public class DeleteFriend extends DialogFragment {
	public static final String TAG = "DeleteFriend";
	ImageView userPictureIv;
	TextView userNameTv;
	ImageButton delete, cancel;
	int pictureId; 
	String userName;
	Friend pendingFriend;
	SmirkMainActivity mActivity;
	FragmentManager mFragmentManager;
	Friend existingFriend;
	Drawable friendPic;
	int id;
	
	private BroadcastReceiver removeFriendBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.d(TAG, "removeFriend BR onReceive");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String res = intent.getStringExtra("RESULT");
				Log.d(TAG, " inside onReceive : " + res);				
				// display friend removed message to the user
			}
		}
	};
	 
    public DeleteFriend(SmirkMainActivity activity, Friend existingFriend, FragmentManager fragmentManager) {
		//this.pictureId = existingFriend.getPictureId();
		existingFriend = existingFriend;
		userName = existingFriend.getScreenName();
		mActivity = activity;
		mFragmentManager = fragmentManager;
		friendPic = existingFriend.getUserBitmap();
		id = existingFriend.getId();
    }
    
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.delete_friend, container, false);
		
		userPictureIv = (ImageView) v.findViewById(R.id.existing_friend_pic);
		userNameTv = (TextView) v.findViewById(R.id.existing_friend_name);
		
		userPictureIv.setBackgroundDrawable(friendPic); 
		userNameTv.setText(this.userName);
	
        delete = (ImageButton)v.findViewById(R.id.delete_friend);
        delete.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
            	String myUrl = Constants.REMOVE_FRIEND_URL + id;
            	Log.d(TAG, " Before calling removeFriend api- url " + myUrl);
        		SmirkApiCalls.removeFriendJsonObjectRequestQueue(getActivity(), myUrl, CommonData.sessionId, null);

				for (int i=0; i<Friend.existingFriends.size(); i++){
					Log.d(TAG, Friend.existingFriends.get(i).toString());
					if (Friend.existingFriends.get(i).getScreenName().equals(userName)){
						Log.d(TAG, "Found matching deletion username");
						Friend.existingFriends.remove(i);
					}
				}

				mActivity.getFriendsScreen().redrawExistingFriends(); 
				
			  	mFragmentManager.popBackStack();
            }
        });

	    cancel = (ImageButton)v.findViewById(R.id.cancel_request);
        cancel.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
                mFragmentManager.popBackStack();
            }
        });
	

        return v;
    }	
}