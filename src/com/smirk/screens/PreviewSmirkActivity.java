package com.smirk.screens;

import java.io.InputStream;
import java.io.IOException;

import android.app.Activity;
import android.os.Build;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.view.View.OnClickListener;
import android.view.*;
import android.graphics.*;
import android.util.DisplayMetrics;
import android.content.res.Resources;
import android.graphics.Bitmap.Config;
import android.util.AttributeSet;
import android.util.Log;
import android.widget.ImageButton;
import android.widget.PopupWindow;
import android.widget.RelativeLayout;
import android.os.Handler;

import com.smirk.R;
import com.smirk.commom.Utils;
import com.smirk.commom.SmirkResource;
import com.smirk.screens.play.CameraActivity;
import com.smirk.screens.play.AnimatedTextCameraActivity;

public class PreviewSmirkActivity extends Activity{
	
	private static final String TAG = "PreviewSmirkActivity";
	
	private int mExpressionType, mResId;
	private boolean mIsAnimated;
	private MYGIFView mGifView;
	private RelativeLayout mGifContainer;
	private ImageButton mNotUse, mUse;
	
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.preview_smirk_activity);
		
		//Get thumbId and gifId from intent
		Intent intent = getIntent();
		mExpressionType = intent.getIntExtra("EXP_TYPE", 0);
		mResId = intent.getIntExtra("EXP_ID", 0);
		mIsAnimated = intent.getBooleanExtra(Utils.IS_ANIMATED, false);
		
		mGifContainer = (RelativeLayout) findViewById(R.id.GIF_container);
		
		
		mGifView = new MYGIFView(getApplicationContext());
		mGifView.loadGIFResource(getApplicationContext(), mExpressionType, mResId);
		mGifContainer.addView(mGifView);
		
		mNotUse = (ImageButton) findViewById(R.id.dont_use);
		mUse = (ImageButton) findViewById(R.id.use);
		
		mNotUse.setOnClickListener(new OnClickListener(){
		            @Override
		            public void onClick(View v) {
						finish();
		            }});
		
		mUse.setOnClickListener(new OnClickListener(){
		            @Override
		            public void onClick(View v) {
						if (!mIsAnimated){
							startCameraActivity();
							finish();
						}
						else{
							startAnimatedTextActivity();
							finish();
						}
		            }});
	}
	
	private void startCameraActivity(){
		Intent cam = new Intent(PreviewSmirkActivity.this, CameraActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("EXP_TYPE", mExpressionType);
		bundle.putInt("EXP_ID", mResId);
		cam.putExtras(bundle);
		Utils.isCameraActivityOnTop = true;
		startActivity(cam);
	}
	
	private void startAnimatedTextActivity(){
		Intent animate = new Intent(PreviewSmirkActivity.this, AnimatedTextCameraActivity.class);
		Bundle bundle = new Bundle();
		bundle.putInt("EXP_TYPE", mExpressionType);
		bundle.putInt("EXP_ID", mResId);
		animate.putExtras(bundle);
		Utils.isCameraActivityOnTop = true;
		startActivity(animate);
	}
	
	private class MYGIFView extends View {
		String TAG = "MYGIFView";
		
		Movie movie;
		MYGIFView instance;
		long moviestart, now; //These numbers can be fudged aroudn to change speeds
		boolean firstDraw;
		Handler mHandler = new Handler();
		int movieWidth, movieHeight;
		int movieDuration;

		public MYGIFView(Context context) {
			super(context);
			instance = this;
		}

		public MYGIFView(Context context, AttributeSet attrs)
				throws IOException {
			super(context, attrs);
			instance = this;

		}

		public MYGIFView(Context context, AttributeSet attrs, int defStyle)
				throws IOException {
			super(context, attrs, defStyle);
			instance = this;
		}


		public void loadGIFResource(Context context, int expressionType, int resId) {
			// turn off hardware acceleration, only for honeycomb and newer versions
			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}
			movie = SmirkResource.getMovieFromSmirkResList(expressionType, resId);
			movieWidth = movie.width();
		  	movieHeight = movie.height();
		  	movieDuration = movie.duration();
		}
		

		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			if (movie == null) {
				return;
			}

			canvas.translate(-25.0f, 0.0f);
			canvas.scale(0.6f, 0.6f); //runs off the screen a bit, scale down some
			if (!mIsAnimated){
				//animated text smirks don't use the skintone feature
				int[] skinToneRGB = new int[3];
				skinToneRGB = Utils.getSkinToneRGB(skinToneRGB);
				int r = Integer.valueOf(skinToneRGB[0]);
				int g = Integer.valueOf(skinToneRGB[1]);
				int b = Integer.valueOf(skinToneRGB[2]);
				canvas.drawRGB(r, g, b);
			}
			else{
				int r = 255;
				int g = 255;
				int b = 255;
				canvas.drawRGB(r, g, b);
			}
			
			now = android.os.SystemClock.uptimeMillis();
	        if (moviestart == 0) {   // first time
	            moviestart = now;
	        }

	        if (movie != null) {
	            int dur = movie.duration();
	            if (dur == 0) {
	                dur = 1000;
	            }
	            int relTime = (int)((now - moviestart) % dur);
	            movie.setTime(relTime);
	            movie.draw(canvas, 0, 0);
	            invalidate();

	        }
		}
	}
	
}