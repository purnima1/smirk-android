package com.smirk.screens;

import android.content.Intent;
import android.app.Activity;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.content.Context;
import android.graphics.Bitmap;
import android.util.Log;
import android.os.Bundle;

import com.smirk.commom.CommonData;
import com.smirk.R;

public class ViewSharePicture extends Activity{
	
	private static final String TAG = "ViewSharePicture: ";
	
	private ImageView sharedImage;
	private int imageIndex;
	private Bitmap sharedImageBitmap;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.view_shared_picture);
		
		sharedImage = (ImageView) findViewById(R.id.shared_image);
		
		Intent intent = getIntent();
		
		if (intent.getExtras() != null){
			imageIndex = intent.getIntExtra("IMAGE_INDEX", 0);
			sharedImageBitmap = CommonData.userSharedPictures.get(imageIndex).getPicture();
		} else{
			Log.d(TAG, "Image couldn't be pulled up");
			finish();
		}
		
		sharedImage.setImageBitmap(sharedImageBitmap);
	}
	
	@Override
	public void onBackPressed() {
	    finish();
	}
	
}