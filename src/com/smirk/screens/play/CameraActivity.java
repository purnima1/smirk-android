package com.smirk.screens.play;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.util.List;
import java.io.InputStream;
import java.io.IOException;
import java.util.Calendar;

import android.annotation.TargetApi;
import android.os.SystemClock;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.app.Activity;
import android.content.Intent;
import android.content.Context;
import android.graphics.*;
import android.graphics.Paint.Align;
import android.hardware.Camera;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.os.Looper;
import android.util.Log;
import android.view.*;
import android.widget.RelativeLayout;
import android.widget.ImageView;
import android.content.res.Resources;
import android.graphics.Bitmap.Config;
import com.smirk.R;
import com.smirk.commom.Utils;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.SmirkResource;

public class CameraActivity extends Activity implements SurfaceHolder.Callback, Camera.PreviewCallback, Camera.ErrorCallback{
	
	private static final String TAG = "CameraActivity";
	private RelativeLayout mSkinToneLayout;
	private Camera mCamera;
	boolean mPreviewRunning,mIsDone = false;
	private SurfaceView mSurfaceView;
	private SurfaceHolder mSurfaceHolder;
	private int mCounter = 0; //changed from 1
	private int mSavedNum = 0; //changed from 1
	private Handler mHandler = new Handler();
	private MYGIFView mGIFView;
	private CameraActivity instance = this;
	private View mView;

	private File mSmirkImgcacheDir = null;
	
	private int expressionType, resId;

	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		Log.e(TAG, "onCreate");
		instance = CameraActivity.this;
		
		getWindow().setFormat(PixelFormat.TRANSLUCENT);
		requestWindowFeature(Window.FEATURE_NO_TITLE);
		getWindow().setFlags(WindowManager.LayoutParams.FLAG_FULLSCREEN,
				WindowManager.LayoutParams.FLAG_FULLSCREEN);
		setContentView(R.layout.cameraview);

		mSkinToneLayout = (RelativeLayout) findViewById(R.id.relativecamera);
		int[] skinToneRGB = new int[3];
		skinToneRGB = Utils.getSkinToneRGB(skinToneRGB);
		mSkinToneLayout.setBackgroundColor(Color.rgb(skinToneRGB[0],
				skinToneRGB[1], skinToneRGB[2]));
				
		mGIFView = new MYGIFView(this);
	
		Intent intent = getIntent();
		Bundle extras = intent.getExtras();
		    if(extras != null)
		    {
		        Log.d(TAG, "Bundle not null");
				expressionType = extras.getInt("EXP_TYPE");
				resId = extras.getInt("EXP_ID");
				SmirkResource.indicatorBitmapHolder = SmirkResource.getBitmapFromSmirkResList(expressionType, resId);
		    }
		

		RelativeLayout mRelativeLayout = (RelativeLayout) findViewById(R.id.gifView);
		mGIFView.loadGIFResource(this, expressionType, resId);
		mRelativeLayout.addView(mGIFView);

		mSurfaceView = (SurfaceView) findViewById(R.id.surface_camera);
		mSurfaceHolder = mSurfaceView.getHolder();
		mSurfaceHolder.addCallback(instance);
		mSurfaceHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
	}

	@Override
	protected void onRestoreInstanceState(Bundle savedInstanceState) {
		super.onRestoreInstanceState(savedInstanceState);
	}
	
	@Override
	public void onPreviewFrame(final byte[] data, final Camera camera) {
		if(mPreviewRunning){
			mCamera.stopPreview();}
		mPreviewRunning = false;
		
		Log.d(TAG, "Preview Frame called. Size: " + data.length);

		new Thread(new Runnable() {
			public void run() {
				Looper.prepare();//Need this to show dialog in gifView
				if (data != null) {
					try {
						Log.d(TAG, "Supported preview formats: " + mCamera.getParameters().getSupportedPreviewFormats().toString());
						
						//Wierd funk to go from NV21 to bmp http://stackoverflow.com/questions/9325861/converting-yuv-rgbimage-processing-yuv-during-onpreviewframe-in-android
						List<Camera.Size> sizes = camera.getParameters().getSupportedPreviewSizes();
						Camera.Size cs = sizes.get(0);
						
						//Setting camera up to take landscape, rather than portrait-orientation pictures
						Camera.Parameters parameters = camera.getParameters();
						parameters.set("orientation", "landscape");
						camera.setParameters(parameters);
															
						ByteArrayOutputStream out = new ByteArrayOutputStream();
						YuvImage yuv = new YuvImage(data, ImageFormat.NV21, cs.width, cs.height, null);
						yuv.compressToJpeg(new Rect(0, 0, cs.width, cs.height), 50, out);
						byte[] bytes = out.toByteArray();
						
						//Will base the sampling on screen resolution -- lower resolution, do less sampling
						//Higher resolution, have a lower sampling rate (out of memory errors & resultant
						//pic sizing, which has been an issue, anyway) Options.inDensity might be a better
						//thing to mess with in the future 
						DisplayMetrics dm = new DisplayMetrics(); 
						getWindowManager().getDefaultDisplay().getMetrics(dm);
						int sampling = 1;
						if (dm.heightPixels < 650){ sampling = 1; }
						else if (dm.heightPixels > 650){ sampling = 2; }
						
						//repeated out of memory errors -- going to try changing colors to RGB and 
						//reducing the sampling here
						BitmapFactory.Options options = new BitmapFactory.Options();
						options.inPreferredConfig = Config.RGB_565;
						options.inSampleSize = sampling;
						
						Bitmap bitmap = BitmapFactory.decodeByteArray(bytes, 0, bytes.length, options);
						saveBitmapToFile(bitmap);
					} catch (Exception e) {
						Log.d(TAG,"Bitmap failed from preview byte array");
						e.printStackTrace();
					}
				}
				if (mCounter >= Utils.NUMBER_OF_REVIEW_PICS) { // Hold here while saving TODO: Move the saving to an Intent Service -mStanford
					Log.d(TAG, "Picture #: " + mCounter + " Saved # " + mSavedNum);
					//mGIFView.showDialog("Processing images please wait...");
					while(mSavedNum < mCounter){
						try {
							Thread.sleep(50); //Waiting for the images to save, if it was in a  service we wouldnt have to do this
						} catch (InterruptedException e) {
							e.printStackTrace();
						}
					}
					if(mCamera != null){
						//mSurfaceHolder.removeCallback(mPreview);
						//mCamera.release(); moved these calls (also ones in onSurfaceDestroyed) to onStop()
						//mCamera = null;
					}
			
					finish();
					return;
				}
			}
		}).start();
		
			
	}

	@Override
	public void onError(int error, Camera camera) {
		if(error == 100)
			mCamera = Camera.open(Utils.BACK_CAMERA);
	}

	private synchronized void saveBitmapToFile(Bitmap bitmap) {
		Log.d(TAG, "Saving bitmap. Size: " + bitmap.getByteCount());
		File file = null;
		if (android.os.Environment.getExternalStorageState().equals(
				android.os.Environment.MEDIA_MOUNTED))
			mSmirkImgcacheDir = new File(
					android.os.Environment.getExternalStorageDirectory(),
					"smirk");
		if (!mSmirkImgcacheDir.exists())
			mSmirkImgcacheDir.mkdirs();
		file = new File(mSmirkImgcacheDir, "reviewImage_" + mCounter++ + ".bmp");
		Log.d(TAG, "Saving bitmap. Location: " + file.toString());
		try {
			OutputStream os = new FileOutputStream(file);
			bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
			os.flush();
			os.close();
			Log.d(TAG, "Saving bitmap successful");
		} catch (Exception ex) {
			Log.d(TAG, "Saving bitmap failed");
			ex.printStackTrace();
		} finally{
			mSavedNum++;
		}
	}

	protected void onResume() {
		Log.e(TAG, "onResume");
		super.onResume();
	}

	protected void onSaveInstanceState(Bundle outState) {
		super.onSaveInstanceState(outState);
	}

	protected void onStop() {
		Log.e(TAG, "onStop");
		super.onStop();
		mCamera.release(); 
		mCamera = null;
	}

	@TargetApi(9)
	public void surfaceCreated(SurfaceHolder holder) {
		Log.d(TAG, "surfaceCreated");
	}

	public void surfaceChanged(SurfaceHolder holder, int format, int w, int h) {
		Log.d(TAG, "surfaceChanged");
		
		mCamera = Camera.open(Utils.BACK_CAMERA);
		
		while(mCamera == null){}

		if (mPreviewRunning) {
			mCamera.stopPreview();
		}

		Camera.Parameters parameters = mCamera.getParameters();
		parameters.setPreviewFormat(ImageFormat.NV21);						//Need this for onPreview decoding to BMP, otherwise we don't know the default format
		List<Camera.Size> sizes = parameters.getSupportedPreviewSizes();	//The default may be anything, but every camera 
		Camera.Size cs = sizes.get(0);										//support nv21 so that's what we will shoot for, for decoding -mStanford
		parameters.setPreviewSize(cs.width, cs.height);

		mCamera.setParameters(parameters);
		try {
			mCamera.setPreviewDisplay(holder);
		} catch (Exception e) {
			e.printStackTrace();
		}
		

		mHandler.post(new Runnable() {
			@Override
			public void run() {
				if (mCamera != null && mCounter < Utils.NUMBER_OF_REVIEW_PICS + 1) {
					Log.d(TAG, "One shot set");
					mCamera.setOneShotPreviewCallback(instance);
					mCamera.startPreview();
					mPreviewRunning = true;
					mHandler.postDelayed(this, Utils.DELAY_MILLIS);
				} else {
					mHandler.removeCallbacks(this);
				}
			}
		});
	}

	public void surfaceDestroyed(SurfaceHolder holder) {
		Log.e(TAG, "surfaceDestroyed");
		if (mCamera != null) {
			if(mPreviewRunning)
				mCamera.stopPreview();
			mPreviewRunning = false;
			//mCamera.release(); gonna move these calls to onStop()
			//mCamera = null;
		}
	}

	private class MYGIFView extends View {
		String TAG = "MYGIFView";
		
		Movie movie;
		MYGIFView instance;
		long moviestart, now; //These numbers can be fudged aroudn to change speeds
		boolean firstDraw = true;
		boolean showDialog = false;
		String mMessage = "";
		Handler mHandler = new Handler();
		Bitmap endSplash;

		public MYGIFView(Context context) {
			super(context);
			instance = this;
		}

		public MYGIFView(Context context, AttributeSet attrs)
				throws IOException {
			super(context, attrs);
			instance = this;

		}

		public MYGIFView(Context context, AttributeSet attrs, int defStyle)
				throws IOException {
			super(context, attrs, defStyle);
			instance = this;
		}
		
		public void loadEndSplash(){
			Resources res = getResources();
			endSplash = BitmapFactory.decodeResource(res, R.drawable.youvebeensmirked);
			endSplash = Utils.RotateBitmap(endSplash, 90);
		}

		public void loadGIFResource(Context context, int expressionType, int resId) {
			// turn off hardware acceleration, only for honeycomb and newer versions
			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}
			movie = SmirkResource.getMovieFromSmirkResList(expressionType, resId);
		}
		
		public void showDialog(String msg){
			showDialog = true;
			mMessage = msg;
		}

		
		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			
			loadEndSplash();
			
			DisplayMetrics dm = new DisplayMetrics(); 
			getWindowManager().getDefaultDisplay().getMetrics(dm);
			
			int screenWidth = dm.widthPixels; 
			int screenHeight = dm.heightPixels;
			
			if (movie == null) {
				return;
			}
			
			if(firstDraw){
				firstDraw = false;
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						if(movie != null){
							now += 50;
							instance.invalidate();
							mHandler.postDelayed(this, 10); //17 ms makes it 60 fps
						}else{
							mHandler.removeCallbacks(this);
						}
					}
				});
			}

			if (moviestart == 0){
				moviestart = now;}
			
			int relTime;
			relTime = (int) ((now - moviestart) % movie.duration());
			movie.setTime(relTime);
			
			int drawLocation = 0;
			
			if (screenHeight < 650 ){ //if we have a lower resolution device, namely the Tab2 7" tester
				drawLocation = 0;}		
			else if (screenHeight > 650){ //if we have a higher resolution, GIF doesn't auto-fill screen nicely
				drawLocation = 100;}
				
			movie.draw(canvas, drawLocation, drawLocation);

			if (now - moviestart > 2000){
				Rect dest = new Rect(0, 0, screenWidth, screenHeight);
				canvas.drawBitmap(endSplash, null, dest, new Paint());
			}
		}
	}
}
