package com.smirk.screens.play;

import android.app.Dialog;
import android.view.Window;
import android.view.WindowManager;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.widget.ImageView;

import com.smirk.R;
import com.smirk.SmirkMainActivity;

public class SpinningEyes {
	
	private static final String TAG = "SpinningEyes";
	
	private SmirkMainActivity mActivity;
    
	public SpinningEyes(SmirkMainActivity activity){
		mActivity = activity;
	}

	public void playSpinningEyes(){
		Dialog dialog = new Dialog(mActivity, R.style.ThemeDialogCustom);
	    dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);

	    dialog.setContentView(R.layout.spnning_eyes);
	    dialog.setCancelable(true);
	    WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
	    lp.copyFrom(dialog.getWindow().getAttributes());
	    lp.width = WindowManager.LayoutParams.MATCH_PARENT;
	    lp.height = WindowManager.LayoutParams.MATCH_PARENT;

	    //ImageView spinningEyesImg = (ImageView) dialog.findViewById(R.id.spinning_eyes_iv_id);
		//Animation animation = AnimationUtils.loadAnimation(mActivity, R.anim.spnning_anim);
        //spinningEyesImg.startAnimation(animation);
	    
	    dialog.show();
	    dialog.getWindow().setAttributes(lp);
	    

	}
	
}
