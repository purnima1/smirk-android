package com.smirk.screens.play;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.List;

import android.graphics.Bitmap;
import android.content.res.Resources;
import android.content.Context;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.View.OnTouchListener;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.TextView;
import android.os.Vibrator;

import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.HUDView;
import com.smirk.commom.Utils;
import com.smirk.commom.SmirkResource;
import com.smirk.commom.Utils.Screens;
import com.smirk.screens.ArchiveScreen;
import com.smirk.screens.PlayScreen;

public class ReviewPictureScreen implements OnTouchListener, OnClickListener{

	private final String TAG = "ReviewPictureScreen";
	
	private SmirkMainActivity mActivity;
	private LayoutInflater mInflater;
	private FrameLayout mContentView;
	
	private ImageView mReviewImg;
	private ImageView mIndicatorImg; 
	private Bitmap mIndicatorBitmap;
	private ImageButton mApproveImgBtn, mDiscardImgBtn, mBackBtn;
	
	private List<Bitmap> mReviewImgs = new ArrayList<Bitmap>();
	
	private float originalPos, currentPos;
	private int mCounter = 0;
	
	private ScorePictureScreen mScorePictureScreen;
	private int mCurrentIndicatorId;
	
	
	public ReviewPictureScreen(SmirkMainActivity activity) {
		mActivity = activity;
		mContentView = (FrameLayout) mActivity.findViewById(R.id.content_layout_id);
	}

	public void showScreen(){
		mReviewImgs.clear();
		mCounter = 0;
		
		mIndicatorBitmap = SmirkResource.indicatorBitmapHolder;
	
		Vibrator v = (Vibrator) mActivity.getSystemService(Context.VIBRATOR_SERVICE);
		v.vibrate(500);
		
		mActivity.setCurrentScreen(Screens.ReviewPictureScreen);
		((ImageView)mActivity.findViewById(R.id.top_bar_title_iv_id)).setImageDrawable(mActivity.getResources().getDrawable(R.drawable.time_to_smirk));
		
		mContentView.removeAllViews();
		
		ViewGroup view = (ViewGroup) mActivity.getLayoutInflater().inflate(R.layout.review_picture, mContentView,false);
		
		mContentView.addView(view);
		
		mReviewImg = (ImageView) view.findViewById(R.id.review_picture_iv_id);
			
		mReviewImg.setImageBitmap(getReviewBitmaps().get(mCounter)); 
		
		mReviewImg.setOnTouchListener(this);
		
		mApproveImgBtn = (ImageButton) view.findViewById(R.id.imgbtn_approve_id);
		mApproveImgBtn.setOnClickListener(this);
		mDiscardImgBtn = (ImageButton) view.findViewById(R.id.imgbtn_discard_id);
		mDiscardImgBtn.setOnClickListener(this);
	}
	
	private List<Bitmap> getReviewBitmaps(){
		File smirkImgcacheDir=null, file=null;
		if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED))
	    	smirkImgcacheDir=new File(android.os.Environment.getExternalStorageDirectory(),"smirk");
		if(!smirkImgcacheDir.exists())
			return null;
		for(int i = 0; i <= Utils.NUMBER_OF_REVIEW_PICS-1; i++){ //start from 0, go to len-1 
			file=new File(smirkImgcacheDir, "reviewImage_"+i+".bmp");
			if(file!=null){
				Bitmap smirkedBitmap = decodeFile(file);
				smirkedBitmap = makeComposite(smirkedBitmap, mIndicatorBitmap);
				mReviewImgs.add(smirkedBitmap);
			}
		}
		Log.i(TAG, "**********************  size = "+mReviewImgs.size());
		if(mReviewImgs.size()==0) 
			return null;
		return mReviewImgs;
	}
	
	public Bitmap makeComposite(Bitmap base, Bitmap overlay){
		Bitmap finalBitmap = Bitmap.createBitmap(base.getWidth(), base.getHeight(), base.getConfig());

		Canvas canvas = new Canvas(finalBitmap);

		canvas.drawBitmap(base, new Matrix(), null);
		
		Matrix overlayMatrix = new Matrix();
		
		overlay = Utils.scaleBitmapWithConfig(overlay, 150, 100);
		
		float moveXdistance = base.getWidth()-overlay.getWidth()-20;
		float moveYdistance = base.getHeight()-overlay.getHeight()-20;
		overlayMatrix.postTranslate(moveXdistance, moveYdistance);
		
		canvas.drawBitmap(overlay, overlayMatrix, null);

		return finalBitmap;
	}

	private Bitmap decodeFile(File f){
        try {
            BitmapFactory.Options o = new BitmapFactory.Options();
            return BitmapFactory.decodeStream(new FileInputStream(f),null,o);
        } catch (FileNotFoundException e) {
        	
        } catch (Exception e){
        	Log.d(TAG,"Couldn't decode bitmap");
        }
        return null;
    }

	@Override
	public boolean onTouch(View v, MotionEvent event) {
		//TODO: clean this up. Use a gesture detector with standard path, off path and velocity modifiers - mStanford
		if(v.getId() == R.id.review_picture_iv_id){
			switch(event.getAction())
            {
            case MotionEvent.ACTION_DOWN:
            	originalPos = event.getRawX();
                return true;
            case MotionEvent.ACTION_UP:
            	currentPos = event.getRawX();
            	Log.i(TAG, "**********************  size = "+mReviewImgs.size()+" ori.x="+originalPos+" cur.x="+currentPos+" mCounter="+mCounter);
            	//move to right pic
            	if(originalPos >= currentPos && mCounter < Utils.NUMBER_OF_REVIEW_PICS-1 ){ // This was NUM-1, changed because couldn't scroll to last one. -mStanford
 					Log.i(TAG, "mCounter = "+mCounter);						
           			mReviewImg.setImageBitmap(mReviewImgs.get(++mCounter));
            	}
            	else if (originalPos < currentPos && mCounter > 0){
					Log.i(TAG, "mCounter = "+mCounter);
            		mReviewImg.setImageBitmap(this.mReviewImgs.get(--mCounter));
            	}
            	else{
            		
            	}
                return true;
            }
            return false;
		}
		return false;
	}

	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.imgbtn_approve_id){
			mScorePictureScreen = mActivity.getScorePictureScreen();
			if(mScorePictureScreen==null){
				mScorePictureScreen = new ScorePictureScreen (mActivity);
				mActivity.setScorePictureScreen(mScorePictureScreen);
			}
			mScorePictureScreen.showScreen(mReviewImgs.get(mCounter), mCurrentIndicatorId); //removed the ++ from mCounter	
			return;
		}
		else if(v.getId() == R.id.imgbtn_discard_id){
			//PlayScreen has been init in the SmirkMainActivity;
			mActivity.getPlayScreen().showScreen();
		}
	}
	

}
