package com.smirk.screens.play;

import java.util.Random;
import java.io.File;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Canvas;
import android.graphics.Matrix;
import android.graphics.Paint;
import android.graphics.Color;
import android.view.Gravity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.Window;
import android.view.WindowManager;
import android.view.View.OnClickListener;
import android.view.animation.Animation;
import android.view.animation.AnimationUtils;
import android.view.ViewGroup;
import android.content.pm.PackageManager;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.LinearLayout;
import android.media.FaceDetector;
import android.media.FaceDetector.Face;
import android.util.Base64;
import android.util.Log;
import android.widget.Toast;
import android.net.Uri;
import android.os.AsyncTask;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.os.Bundle;
import android.content.IntentFilter;

import org.json.JSONException;
import org.json.JSONObject;

import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Session.StatusCallback;
import com.facebook.UiLifecycleHelper;

import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils;
import com.smirk.commom.Utils.Screens;
import com.smirk.screens.PlayScreen;
import com.smirk.commom.SmirkImage;
import com.smirk.commom.CommonData;
import com.smirk.network.SmirkApiCalls;

public class ScorePictureScreen implements OnClickListener{
	private static final String TAG = "ScorePicturesScreen";
	
	private SmirkMainActivity mActivity;
	private LayoutInflater mInflater;
	private FrameLayout mContentView;
	
	private ImageView mSmirkPic;
	private ImageView mIndicatorImg;
	private Bitmap mIndicatorBitmap;
	private TextView mScore;
	private int mCurrentIndicatorId;
	private ImageButton mPlayAgainImgBtn, mShareImgBtn, mBackBtn;
	private ImageButton mFacebookShare, mTwitterShare, mInstagramShare, mSmirkShare, mMessageShare, mEmailShare;
	
	private LinearLayout mShareLayout;
	
	private static final int MAX_FACES = 4;
    private FaceDetector.Face[] faces;
    private int face_count;
	private int mScoreInt;
	private String mBase64ByteStream;
	private String mImageUrl;
	
	private UiLifecycleHelper uiHelper;
	private File mFile;
	SmirkImage mSavedImage;
	Bitmap mComposite;
	
	private BroadcastReceiver uploadImageBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "Received broadcast from result of Smirk upload API call.");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String url = intent.getStringExtra("url");
				Log.d(TAG, "Url of image: "+url);
				CommonData.userSmirkedPictures.add(mSavedImage);
				CommonData.smirkedPicsCount++;
				mImageUrl = url;
				unregisterReceiver();
			}
		}
	};
	
	public void unregisterReceiver(){
		mActivity.unregisterReceiver(uploadImageBroadcastReceiver);
	}
	
	public ScorePictureScreen(SmirkMainActivity activity) {
		mActivity = activity;
		mContentView = (FrameLayout) mActivity.findViewById(R.id.content_layout_id);
		uiHelper = new UiLifecycleHelper(mActivity, null);
		mActivity.registerReceiver(uploadImageBroadcastReceiver, new IntentFilter("UPLOAD_PICTURE"));
	}
	
	public void showScreen(Bitmap bitmap, int currentIndicatorId){
		mCurrentIndicatorId = currentIndicatorId;
		
		mActivity.setCurrentScreen(Screens.ScroePictureScreen);
		((ImageView)mActivity.findViewById(R.id.top_bar_title_iv_id)).setImageDrawable(mActivity.getResources().getDrawable(R.drawable.time_to_smirk));
		
		mContentView.removeAllViews();
		
		ViewGroup view = (ViewGroup) mActivity.getLayoutInflater().inflate(R.layout.score_picture, mContentView,false);
		
		mContentView.addView(view);
		
		mPlayAgainImgBtn = (ImageButton) view.findViewById(R.id.imgbtn_play_again_id);
		mPlayAgainImgBtn.setOnClickListener(this);
		mShareImgBtn = (ImageButton) view.findViewById(R.id.imgbtn_share_id);
		mShareImgBtn.setOnClickListener(this);
		
		mSmirkPic = (ImageView) view.findViewById(R.id.smirkpic);
		mSmirkPic.setImageBitmap(bitmap);
		
		mShareLayout = (LinearLayout)view.findViewById(R.id.share_overlay_layout);
		mShareLayout.setVisibility(View.INVISIBLE);
		
		bitmap = convert(bitmap, Bitmap.Config.RGB_565);
		mScoreInt = scorePicture(bitmap);
		
		final boolean facebookInstalled = appInstalledOrNot("com.facebook");
		
		Random rand = new Random();
		int randomNum = rand.nextInt((10 - 0) + 1);
		
		mScoreInt += randomNum;

		mScore = (TextView) view.findViewById(R.id.score_tv_id);
		mScore.setText(mScoreInt+"/100");
		
		SmirkApiCalls.smirkImageUploadHTTPClient(bitmap, mScoreInt, mActivity.getApplicationContext());
		
		CommonData.updatePoints(mScoreInt);
		
		mSavedImage = new SmirkImage(bitmap, mScoreInt, "");
		
		saveBitmapToFile(bitmap);
		
		mFacebookShare = (ImageButton) view.findViewById(R.id.facebook_imgbtn_id);

		mFacebookShare.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				 //Need to check if  user has FB installed
				 if (!facebookInstalled){
					Toast.makeText(mActivity.getApplicationContext(), "The Facebook app must be installed for this to work, please download in Google Play!"
					, Toast.LENGTH_LONG).show();
				 }
				
				 if (mImageUrl == null){
					Toast.makeText(mActivity.getApplicationContext(), "Image hasn't been uploaded to the server yet -- please try again soon."
					, Toast.LENGTH_LONG).show();
				} 	
		
				  FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(mActivity)
				        .setLink(mImageUrl)
						.setCaption("You've been Smirked!")
				        .build();
				  uiHelper.trackPendingDialogCall(shareDialog.present());
	            }
	         });
	
		mTwitterShare = (ImageButton) view.findViewById(R.id.twitter_imgbtn_id);
		
		mTwitterShare.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  twitterShare();
	            }
	         });
	
		mInstagramShare = (ImageButton) view.findViewById(R.id.intagram_imgbtn_id);
		
		mInstagramShare.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  instagramShare();
	            }
	         });
	
		mSmirkShare = (ImageButton) view.findViewById(R.id.smirk_imgbtn_id);
		
		mSmirkShare.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  smirkShare();
	            }
	         });
	
		mMessageShare = (ImageButton) view.findViewById(R.id.message_imgbtn_id);
		
		mMessageShare.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  messageShare();
	            }
	         });
	
		mEmailShare = (ImageButton) view.findViewById(R.id.mail_imgbtn_id);
		
		mEmailShare.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  mailShare();
	            }
	         });
	}
	
	private void mailShare(){
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
		intent.putExtra(Intent.EXTRA_SUBJECT, "subject here");
		intent.putExtra(Intent.EXTRA_TEXT, "body text");
		try{
			if (mFile != null){
				intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mFile.toURL().toString()));
			}
		} catch (MalformedURLException e){
			e.printStackTrace();
		}
		mActivity.startActivity(Intent.createChooser(intent, "Send email"));
	}
	
	private void messageShare(){
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("image/*");
		intent.putExtra(Intent.EXTRA_TEXT, "You've been Smirked!");
		try{
			if (mFile != null){
				intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mFile.toURL().toString()));
			}
		} catch (MalformedURLException e){
			e.printStackTrace();
		}
		mActivity.startActivity(Intent.createChooser(intent, "Send sms"));
	}
	
	private void smirkShare(){
		SmirkApiCalls.imageShare(mActivity.getApplicationContext(), mImageUrl);
		Toast.makeText(mActivity.getApplicationContext(), "Image was shared with your Smirk friends!"
		, Toast.LENGTH_LONG).show();
	}
	
	private synchronized void saveBitmapToFile(final Bitmap bitmap) {		
		new AsyncTask<Void, Void, Void>() {
           @Override
           protected Void doInBackground(final Void ... params ) {
         		//Log.d(TAG, "Saving bitmap. Size: " + bitmap.getByteCount());
				File file = null;
				File selfieImageLocation = null;
				if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
					selfieImageLocation = new File(android.os.Environment.getExternalStorageDirectory(), "Smirk/");}
				if (!selfieImageLocation.exists()){ selfieImageLocation.mkdirs();}
				//filenaming: selfieSurprise_before_systemtimemillis
				file = new File(selfieImageLocation, "selfieSurprise_after_"+System.currentTimeMillis()+".bmp");
				Log.d(TAG, "Saving bitmap. Location: " + file.toString());
				try {
					OutputStream os = new FileOutputStream(file);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
					os.flush();
					os.close();
					Log.d(TAG, "Saving bitmap successful");
				} catch (Exception ex) {
					Log.d(TAG, "Saving bitmap failed");
					ex.printStackTrace();
				}
				mFile = file;
                return null;
           }
       }.execute();
	}
	
	private void instagramShare(){
		if (appInstalledOrNot("com.instagram.android")){
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType("image/*");
			shareIntent.putExtra(Intent.EXTRA_TEXT, "You've been smirked!");
			shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + mFile.getAbsolutePath()));
			shareIntent.setPackage("com.instagram.android");
			mActivity.startActivity(shareIntent);
		}
		else{
			Toast.makeText(mActivity.getApplicationContext(), "The Instagram app must be installed for this to work, please download in Google Play!"
			, Toast.LENGTH_LONG).show();
		}
	}
	
	private void twitterShare(){
		if (appInstalledOrNot("com.twitter.android")){
			//Intent tweet = mActivity.getPackageManager().getLaunchIntentForPackage("com.twitter.android");
			Intent tweetIntent = new Intent(Intent.ACTION_SEND);
			tweetIntent.putExtra(Intent.EXTRA_TEXT, "You've been Smirked!");
			try{
				if (mFile != null){
					tweetIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mFile.toURL().toString()));
					tweetIntent.setType("image/png"); //mmsIntent.setType("image/*");
				}
			} catch (MalformedURLException e){
				e.printStackTrace();
			}
			
			tweetIntent.setType("text/plain");
			mActivity.startActivity(tweetIntent);
		} else{
			Toast.makeText(mActivity.getApplicationContext(), "Twitter not installed -- download from Google Play to share with it", Toast.LENGTH_LONG).show();
		}
	}
	
	private int scorePicture(Bitmap smirkPicture){
	//Use the Android Face detector to determine if there even is a face in the picture, and if so, give a score
		FaceDetector face_detector = new FaceDetector(smirkPicture.getWidth(), smirkPicture.getHeight(), MAX_FACES);
		faces = new FaceDetector.Face[MAX_FACES];
		face_count = face_detector.findFaces(smirkPicture, faces);
		Log.i(TAG, "scorePictures entered");
		if (face_count == 0){ 
			Log.i(TAG, "0 faces found");
			return 0; //0 points for 0 faces 
		} 
		else if (face_count == 1){
			Log.i(TAG, "1 face found"); 
			return 50; 
		}
		else if (face_count > 1){
			Log.i(TAG, ">1 face found"); 
			return 100; 
		}
		else{ return 0; }
	}
	
	private Bitmap convert(Bitmap bitmap, Bitmap.Config config) {
		//Must convert the bitmap to RGB_565 in order for the FaceDetector tools to work
	    Bitmap convertedBitmap = Bitmap.createBitmap(bitmap.getWidth(), bitmap.getHeight(), config);
	    Canvas canvas = new Canvas(convertedBitmap);
	    Paint paint = new Paint();
	    paint.setColor(Color.BLACK);
	    canvas.drawBitmap(bitmap, 0, 0, paint);
	    return convertedBitmap;
	}
	
	@Override
	public void onClick(View v) {
		if(v.getId() == R.id.imgbtn_play_again_id){
			PlayScreen playScreen = mActivity.getPlayScreen();
			if(playScreen == null){
				playScreen = new PlayScreen(mActivity);
			}
			playScreen.showScreen();
		}
		else if(v.getId() == R.id.imgbtn_share_id){
			mShareLayout.setVisibility(View.VISIBLE);
		}
	}
	
	private boolean appInstalledOrNot(String uri) {
        PackageManager pm = mActivity.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed ;
    }
}
