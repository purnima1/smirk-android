package com.smirk.screens.play;

import java.io.InputStream;
import java.io.IOException;

import android.app.DialogFragment;
import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.os.Build;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.RelativeLayout;
import android.widget.LinearLayout;
import android.widget.FrameLayout;
import android.view.View.OnClickListener;
import android.view.ViewGroup.LayoutParams;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.util.Log;
import android.os.Handler;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.content.Context;
import android.graphics.*;
import android.graphics.Paint.Align;

import com.smirk.R;
import com.smirk.commom.Utils;
import com.smirk.commom.Friend;
import com.smirk.SmirkMainActivity;

public class ViewSmirkFragment extends Fragment{
	
	private static final String TAG = "ViewSmirkFragment: ";
	
	SmirkMainActivity mActivity;
	FragmentManager mFragmentManager;
	private MYGIFView mGIFView;
	private View mView;
	private ViewGroup mContentView;
	private int mGIFid;
	private ImageButton approve, cancel;
	private LayoutInflater mInflater;
	private RelativeLayout mRelativeLayout;

    public ViewSmirkFragment(SmirkMainActivity activity, ViewGroup contentView, int gifID, FragmentManager fragmentManager) {
		mActivity = activity;
		mFragmentManager = fragmentManager;
		mGIFid = gifID;
		mContentView = contentView;
    }

	@Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
	}
	
	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {

		View view = inflater.inflate(R.layout.view_smirk_fragment, null);
		/*
		approve = (ImageButton) view.findViewById(R.id.approve_smirk);
        approve.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View view) {
                //remove Friend from pending list, add them to existing list, and then redraw the screen
				Intent intent = new Intent(mActivity, CameraActivity.class);	
				intent.putExtra("expression", mGIFid);
				mActivity.startActivity(intent);
				mFragmentManager.popBackStack();
            }
        });

		cancel = (ImageButton) view.findViewById(R.id.cancel_smirk);
        cancel.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View view) {
                //remove Friend from pending list, add them to existing list, and then redraw the screen
			  	mFragmentManager.popBackStack();
            }
        });
		*/

		mGIFView = new MYGIFView(mActivity);
		
		mRelativeLayout = (RelativeLayout) view.findViewById(R.id.smirk_preview);
		mGIFView.loadGIFResource(mActivity, mGIFid);
		mRelativeLayout.addView(mGIFView);
		return view;
	}
	
	private class MYGIFView extends View {
		String TAG = "MYGIFView";
		
		Movie movie;
		MYGIFView instance;
		long moviestart, now; //These numbers can be fudged aroudn to change speeds
		boolean firstDraw = true;
		boolean showDialog = false;
		String mMessage = "";
		Handler mHandler = new Handler();

		public MYGIFView(Context context) {
			super(context);
			instance = this;
		}

		public MYGIFView(Context context, AttributeSet attrs)
				throws IOException {
			super(context, attrs);
			instance = this;

		}

		public MYGIFView(Context context, AttributeSet attrs, int defStyle)
				throws IOException {
			super(context, attrs, defStyle);
			instance = this;
		}

		public void loadGIFResource(Context context, int id) {
			// turn off hardware acceleration, only for honeycomb and newer versions
			if (android.os.Build.VERSION.SDK_INT >= Build.VERSION_CODES.HONEYCOMB) {
				this.setLayerType(View.LAYER_TYPE_SOFTWARE, null);
			}
			InputStream is = context.getResources().openRawResource(id);
			movie = Movie.decodeStream(is);
		}

		public void loadGIFAsset(Context context, String filename) {
			InputStream is;
			try {
				is = context.getResources().getAssets().open(filename);
				movie = Movie.decodeStream(is);
			} catch (IOException e) {
				e.printStackTrace();
			}
		}
		
		public void showDialog(String msg){
			showDialog = true;
			mMessage = msg;
		}

		
		@Override
		protected void onDraw(Canvas canvas) {
			super.onDraw(canvas);
			
			DisplayMetrics dm = new DisplayMetrics(); 
			getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);
			int screenWidth = dm.widthPixels; 
			int screenHeight = dm.heightPixels;

			
			if (movie == null) {
				return;
			}
			
			if(firstDraw){
				firstDraw = false;
				mHandler.post(new Runnable() {
					@Override
					public void run() {
						if(movie != null){
							now += 50;
							instance.invalidate();
							mHandler.postDelayed(this, 10); //17 ms makes it 60 fps
						}else{
							mHandler.removeCallbacks(this);
						}
					}
				});
			}

			if (moviestart == 0){
				moviestart = now;}
			
			int relTime;
			relTime = (int) ((now - moviestart) % movie.duration());
			movie.setTime(relTime);
			
			int drawLocation = 0;
			
			if (screenHeight < 650 ){ //if we have a lower resolution device, namely the Tab2 7" tester
				drawLocation = 0;}		
			else if (screenHeight > 650){ //if we have a higher resolution, GIF doesn't auto-fill screen nicely
				drawLocation = 100;}

			canvas.scale(0.5f, 0.5f); //floats to scale animation by half
			movie.draw(canvas, drawLocation, drawLocation);
			}
		}
	
}