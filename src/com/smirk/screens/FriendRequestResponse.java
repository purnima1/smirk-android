package com.smirk.screens;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.view.View.OnClickListener;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.drawable.Drawable;
import android.graphics.Bitmap.Config;
import android.graphics.BitmapFactory;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.util.Log;

import com.smirk.R;
import com.smirk.commom.CommonData;
import com.smirk.commom.Constants;
import com.smirk.commom.Utils;
import com.smirk.commom.Friend;
import com.smirk.commom.FriendNotification;
import com.smirk.network.SmirkApiCalls;
import com.smirk.SmirkMainActivity;

public class FriendRequestResponse extends Fragment{
	
	private static final String TAG = "FriendsRequestResponse: ";
	
	ImageView userPictureIv;
	TextView userNameTv;
	ImageButton approve, deny, cancel;
	int pictureId; 
	String userName;
	Drawable profilePicture;
	int notificationId;
	//Friend pendingFriend;
	SmirkMainActivity mActivity;
	FragmentManager mFragmentManager;
	FriendNotification notification;
	
	private BroadcastReceiver inviteResponseBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.d(TAG, "inviteResponse BR onReceive");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String res = intent.getStringExtra("RESULT");
				Log.d(TAG, " inside onReceive : " + res);				
			}
		}
	};
	
	public FriendRequestResponse() {
		
	}
	
	public FriendRequestResponse(SmirkMainActivity activity, FriendNotification notification, Drawable picture, FragmentManager fragmentManager) {
		this.notification = notification;
		this.notificationId = notification.getNotificationId();
		this.userName = notification.getSenderUserName();
		this.profilePicture = picture;
		mActivity = activity;
		mFragmentManager = fragmentManager;
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.friend_request, container, false);
		
		userPictureIv = (ImageView) v.findViewById(R.id.pending_friend_pic);
		userNameTv = (TextView) v.findViewById(R.id.pending_friend_name);
	
		Bitmap friendPicBMP = Bitmap.createBitmap(profilePicture.getIntrinsicWidth(), profilePicture.getIntrinsicHeight(), Config.ARGB_8888);

		userPictureIv.setImageDrawable(profilePicture);
		userNameTv.setText(userName);
	
        approve = (ImageButton)v.findViewById(R.id.approve_request);
        approve.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View v) {
                //remove Friend from pending list, add them to existing list, and then redraw the screen
            	String myUrl = Constants.INVITE_RESPONSE_URL + notificationId + "/accept_friend_request";
            	Log.d(TAG, " Before calling inviteResponse api- url " + myUrl);
            	SmirkApiCalls.inviteResponse(getActivity(), myUrl, userName);
				FriendNotification.notificationsList.remove(notification);
				Friend friend = new Friend(notification.getSenderUserName(), notification.getSenderPictureURL(), 
				notification.getId(), profilePicture);
				Friend.existingFriends.add(friend);

				mActivity.getFriendsScreen().redrawPendingFriends();
				mActivity.getFriendsScreen().redrawExistingFriends(); 	
			  	mFragmentManager.popBackStack();
            }
        });

	    deny = (ImageButton)v.findViewById(R.id.deny_request);
        deny.setOnClickListener(new ImageButton.OnClickListener() {
            public void onClick(View v) {
           		//remove Friend from pending list
				String myUrl = Constants.INVITE_RESPONSE_URL + notificationId + "/decline_friend_request";
            	Log.d(TAG, " Before calling inviteResponse api- url " + myUrl);
            	SmirkApiCalls.inviteResponse(getActivity(), myUrl, userName);
				FriendNotification.notificationsList.remove(notification);
				
				//mActivity.getFriendsScreen().showScreen();
				mActivity.getFriendsScreen().redrawPendingFriends(); 	
			  	mFragmentManager.popBackStack();
            }
        });


	    cancel = (ImageButton)v.findViewById(R.id.cancel_request);
        cancel.setOnClickListener(new OnClickListener() {
            public void onClick(View v) {
				mFragmentManager.popBackStack();
            }
        });
	

        return v;
    }

}