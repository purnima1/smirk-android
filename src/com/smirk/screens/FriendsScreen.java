package com.smirk.screens;

import java.util.List;
import java.util.ArrayList;
import java.util.Random;
import java.util.Set;
import java.util.HashSet;
import java.io.ByteArrayOutputStream;
import java.io.InputStream;
import java.io.BufferedInputStream;

import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.app.Activity;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.os.AsyncTask;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridLayout;
import android.widget.LinearLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.HorizontalScrollView;
import android.graphics.Color;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.Typeface;
import android.text.Spannable;
import android.text.TextWatcher;
import android.text.Editable;
import android.view.View.OnClickListener;
import android.app.Dialog;
import android.view.Window;
import android.view.WindowManager;
import android.view.ViewGroup.LayoutParams;
import android.view.ViewGroup.MarginLayoutParams;
import android.support.v4.app.FragmentActivity;
import android.os.Handler;
import android.app.Fragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.view.Gravity;

import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.CommonData;
import com.smirk.commom.Constants;
import com.smirk.commom.Utils;
import com.smirk.commom.Utils.Screens;
import com.smirk.commom.Friend;
import com.smirk.commom.FriendNotification;
import com.smirk.network.SmirkApiCalls;

public class FriendsScreen extends FragmentActivity {
	private static final String TAG = "FriendsScreen: ";
	private boolean initialPopulation = true;

	private SmirkMainActivity mActivity;
	private LayoutInflater mInflater;
	private FrameLayout mContentView;

	private LinearLayout pendingFriendsView;
	private GridLayout existingFriendsLayout;

	private TextView mTextView;
	private Button mExistingFriend, mPendingFriend;

	private EditText findFriends;

	private ViewGroup mView;

	private int mImgViewId = R.drawable.profile_pic_sample;
	private Friend friend;

	int totalUserCount = 10;

	private Random rand = new Random();

	public List<FriendNotification> friendInvites;
	public List<Friend> existingFriends;
	
	public List<Friend> findableFriends = new ArrayList<Friend>();
	private List<Friend> matchingUsersList = new ArrayList<Friend>();
	
	String mySearchUrl = null;

	private FragmentManager fragmentManager;
	private FragmentTransaction fragmentTransaction;
	
	private Bitmap friendProfilePic;
	
	int numberNotifications;
	int numberExisting;
	
	private boolean firstDraw;
	
	MarginLayoutParams params = new MarginLayoutParams(LayoutParams.WRAP_CONTENT, LayoutParams.WRAP_CONTENT);
	
	private BroadcastReceiver receiveFriends = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			Log.d(TAG, "Received broadcast for FriendsScreen");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String res = intent.getStringExtra("FRIEND_BROADCAST"); 
				Log.d(TAG, "Res: "+res); 
				if (res.equals("NEW_PENDING_FRIENDS")){
					redrawPendingFriends();
				}
				else if (res.equals("NEW_EXISTING_FRIENDS") && !firstDraw){
					redrawExistingFriends();
				}
			}
			mActivity.unregisterReceiver(receiveFriends);
		}
	};

	public FriendsScreen(SmirkMainActivity activity) {
		mActivity = activity;
		mContentView = (FrameLayout) mActivity.findViewById(R.id.content_layout_id);
		mView = (ViewGroup) mActivity.getLayoutInflater().inflate(R.layout.friends, mContentView, false);
		pendingFriendsView = (LinearLayout) mView.findViewById(R.id.friend_requests);
		existingFriendsLayout = (GridLayout) mView.findViewById(R.id.existing_friends_layout_id);
		params.setMargins(10, 0, 10, 0);
		
		firstDraw = true;
	}

	public void showScreen() {
		mContentView.removeAllViews();

		mActivity.setCurrentScreen(Screens.FriendsScreen);
		
		((ImageView) mActivity.findViewById(R.id.top_bar_title_iv_id)).setImageDrawable(mActivity.getResources().getDrawable(R.drawable.heading_topbar_friends));
		
		findFriends = (EditText) mView.findViewById(R.id.find_friends_edittext);
		findFriendsView();
		
		mActivity.registerReceiver(receiveFriends, new IntentFilter("FRIEND_SCREEN_BROADCAST"));
		
		checkNotifications();
		firstDrawPendingFriends(); //redraws will be ordered after accept/decline of request
		
		checkExisting();
		firstDrawExistingFriends();
		
		mContentView.addView(mView);
	}
	
	private void replaceTaskbar(){
		mActivity.findViewById(R.id.tab_layout_id).setVisibility(View.VISIBLE);
	}
	
	private void checkNotifications(){
		SmirkApiCalls.notificationsGetJsonObjectRequestQueue(mActivity.getApplicationContext(), Constants.NOTIFICATIONS_URL, CommonData.sessionId, null);
	}
	
	private void firstDrawPendingFriends(){
		if (FriendNotification.getList() != null && friendInvites == null){ //first-time draw
			friendInvites = FriendNotification.getList();
			fillPendingFriendsView();
		}
	}
	
	public void redrawPendingFriends(){
		removeAllPendingFriends();
		fillPendingFriendsView();
	}
	
	private void removeAllPendingFriends(){
		pendingFriendsView.removeAllViews();
		pendingFriendsView.invalidate();
	}
	
	private void fillPendingFriendsView(){
		if (FriendNotification.notificationsList != null){
			for (FriendNotification notification : FriendNotification.notificationsList){
				mPendingFriend = new Button(mActivity);
				setPendingFriendButton(notification);
				pendingFriendsView.addView(mPendingFriend);
			}
		}
	}
	
	private void setPendingFriendButton(final FriendNotification notification){
		//mPendingFriend.setBackgroundDrawable(notification.getProfilePictureDrawable());
		mPendingFriend.setCompoundDrawablesWithIntrinsicBounds(null, notification.getProfilePictureDrawable(), null, null);
		
		mPendingFriend.setText(notification.getSenderUserName());
		mPendingFriend.setTextColor(Color.BLACK);
		mPendingFriend.setBackgroundColor(Color.TRANSPARENT);
		mPendingFriend.setTypeface(Typeface.DEFAULT_BOLD);
		mPendingFriend.setGravity(Gravity.CENTER);
		mPendingFriend.setLayoutParams(params);
		//mPendingFriend.setLayoutParams(new LinearLayout.LayoutParams(150, 150));

		mPendingFriend.setOnClickListener(new OnClickListener() {
			@Override public void onClick(View v) {
				approveOrDenyRequest(v, notification, notification.getProfilePictureDrawable());
			}
		});
	}
	
	private void checkExisting(){
		SmirkApiCalls.getFriendsJsonObjectRequestQueue(mActivity.getApplicationContext(), Constants.FRIENDS_URL, CommonData.sessionId, null);
	}
	
	private void firstDrawExistingFriends(){
		if (Friend.getList() != null && existingFriends == null){
			Log.d(TAG, "Existing friends list was null");
			existingFriends = Friend.getList();
			createExistingFriendsView();
			firstDraw = false;
		}
	}
	
	public void redrawExistingFriends(){
		existingFriendsLayout.removeAllViews();
		existingFriendsLayout.invalidate();
		createExistingFriendsView();
	}
	
	private void createExistingFriendsView(){
		if (Friend.existingFriends != null){
			for (Friend friend : Friend.existingFriends){
				String friendImageURL = checkAndPrepareProfileURL(friend.getProfilePicURL());
				downloadProfileImage(friendImageURL, friend);
			}
		}
	}
	
	private void findFriendsView() {
		findFriends.addTextChangedListener(new TextWatcher() {
			@Override
			public void afterTextChanged(Editable s) {
				FragmentManager fragmentManager = mActivity.getFragmentManager();
				FragmentTransaction fragmentTransaction = fragmentManager.beginTransaction();

				String initialInput = s.toString();

				if (initialInput.length() > 2) {
					mySearchUrl = Constants.USERS_SEARCH_URL + initialInput;
					Bundle bundle = new Bundle();
					Log.d(TAG, " pass the argument string : " + initialInput);
					bundle.putString("Search", initialInput);
					FindFriendsFragment findFriendsFrag = FindFriendsFragment.getInstance();
					findFriendsFrag.setArguments(bundle);
					
					fragmentManager.popBackStack();
					
					fragmentTransaction.add(R.id.content_layout_id, findFriendsFrag);
					fragmentTransaction.addToBackStack("findFriends");
					fragmentTransaction.commit();
					findFriends.setText("");
				}
			}

			@Override
			public void beforeTextChanged(CharSequence s, int start, int count, int after) {}

			@Override
			public void onTextChanged(CharSequence s, int start, int before, int count) {}
		});
	}

	private void approveOrDenyRequest(View v, FriendNotification notification, Drawable profilePic) {
		fragmentManager = mActivity.getFragmentManager();
		fragmentTransaction = fragmentManager.beginTransaction();
		FriendRequestResponse requestFrag = new FriendRequestResponse(mActivity, notification, profilePic, fragmentManager);
		fragmentManager.popBackStack();
		fragmentTransaction.add(R.id.content_layout_id, requestFrag);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}
	
	private void drawFriends(final Friend existingFriend){
		mExistingFriend = new Button(mActivity);
		mExistingFriend.setCompoundDrawablesWithIntrinsicBounds(null, existingFriend.getUserBitmap(), null, null);
		//mExistingFriend.setBackgroundDrawable(existingFriend.getUserBitmap());
		mExistingFriend.setText(existingFriend.getScreenName());
		Log.d(TAG, "Existing friend's name: "+existingFriend.getScreenName());
		mExistingFriend.setBackgroundColor(Color.TRANSPARENT);
		mExistingFriend.setTextColor(Color.BLACK);
		mExistingFriend.setTypeface(Typeface.DEFAULT_BOLD);
		mExistingFriend.setGravity(Gravity.CENTER);
		mExistingFriend.setLayoutParams(params);

		mExistingFriend.setOnClickListener(new OnClickListener() {

			@Override
			public void onClick(View v) {
				deleteFriend(v, existingFriend);
			}
		});
		existingFriendsLayout.setPadding(0, 0, 15, 0);
		existingFriendsLayout.addView(mExistingFriend);
		Log.d(TAG, "Friend added to existing friends layout");
	}

	public void deleteFriend(View v, Friend existingFriend) {
		fragmentManager = mActivity.getFragmentManager();
		fragmentTransaction = fragmentManager.beginTransaction();
		DeleteFriend deleteFrag = new DeleteFriend(mActivity, existingFriend, fragmentManager);
		fragmentManager.popBackStack();
		fragmentTransaction.add(R.id.content_layout_id, deleteFrag);
		fragmentTransaction.addToBackStack(null);
		fragmentTransaction.commit();
	}

	
	@Override
    protected void onPause() {
        super.onPause();
        mActivity.findViewById(R.id.tab_layout_id).setVisibility(View.GONE);
    }

	@Override
    protected void onResume() {
        super.onPause();
        mActivity.findViewById(R.id.tab_layout_id).setVisibility(View.VISIBLE);
		//mActivity.registerReceiver(notificationsAndExisting, new IntentFilter("NotificationsAndExisting"));
    }

	public String checkAndPrepareProfileURL(String profilePicURL){
		String preparedURL = "";
		if (profilePicURL.contains("23.253.166.61") && !profilePicURL.contains("http://")){
			preparedURL = "http://"+profilePicURL;
		}
		else if (!profilePicURL.contains("http://23.253.166.61")){ //if it's just the server location
			preparedURL = "http://23.253.166.61"+profilePicURL;
		}
		else if (profilePicURL.contains("graph.facebook")){
			preparedURL = profilePicURL;
		}
		else{
			preparedURL = profilePicURL;
		}
		return preparedURL;
	}

	public void downloadProfileImage(final String imageURL, final Friend existingFriend){
		//If iUserOrFriend = true, it's the user's own profile picture. If false, it's a friend's picture.
		new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(final Void ... params) {
		        Bitmap profilePicture = null;
		        try {
						//String preparedUrl = checkAndPrepareProfileURL(imageURL);
						Log.d(TAG, "Before downloading the image at: "+imageURL);
						URL url = new URL(imageURL);
						URLConnection urlConnection = url.openConnection();
						InputStream in = new BufferedInputStream(urlConnection.getInputStream());

						profilePicture = BitmapFactory.decodeStream(in);

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        return profilePicture;
			}
			@Override
			protected void onPostExecute(Bitmap result) {
				setFriendProfileImageResult(result, existingFriend);
		    }
		}.execute();	
	}

	private void setFriendProfileImageResult(Bitmap result, Friend existingFriend){
		Log.d(TAG, "Entered setFriendProfileImageResult in FindFriendsFragment");
		Drawable friendPicDrawable = null;
		//Friend friendItem = null;
		
		//crop and make into circle
		if (result != null){
			friendProfilePic = Utils.getRoundedCornerBitmap(result, result.getWidth());
			friendProfilePic = Utils.scaleBitmap(friendProfilePic, 200f, 200f);
			friendPicDrawable = new BitmapDrawable(mActivity.getResources(), friendProfilePic);

		} else{
			//set Drawable friendPicDrawable to the default
			Log.d(TAG, "Friend image was null");
			friendPicDrawable = mActivity.getResources().getDrawable(R.drawable.icon_profile_pic_mini);
		}
		//friendItem = new Friend(existingFriend.getScreenName(), "", existingFriend.getId(), friendPicDrawable);
		existingFriend.setProfilePicture(friendPicDrawable);
		drawFriends(existingFriend);
	}


}
