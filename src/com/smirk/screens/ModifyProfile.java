package com.smirk.screens;

import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.provider.MediaStore;
import android.provider.MediaStore.Files.FileColumns;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.CheckBox;
import android.widget.Button;
import android.view.View;
import android.view.View.OnClickListener;
import android.util.Base64;
import android.util.Log;

import org.json.JSONObject;
import org.json.JSONException;

import com.smirk.screens.home.ProfilePicture;
import com.smirk.commom.Utils;
import com.smirk.commom.CommonData;
import com.smirk.network.SmirkApiCalls;

import com.smirk.R;

public class ModifyProfile extends Activity{
	
	private static final String TAG = "ModifyProfile: ";
	
	private ImageButton mProfilePicture;
	private EditText mScreenNameET;
	private EditText mRealNameET;
	private CheckBox mEmailUpdatesBox;
	private Button mChangeSkintone, mSetQuestions;
	private ImageButton mSubmitChanges;
	
	private Uri mProfilePicUri;
	private Bitmap mProfilePicBitmap;
	private byte[] mProfilePicByteArr;
	private String mBase64ProfilePic;
	private String mProfileImageServerLocation;
	private int mAppliedRotations;
	
	private String mSkinTone;
	
	private String mNewScreenName;
	private String mRealName;
	
	private int mEmailUpdates;
	
	private JSONObject mUpdatedUser;
	
	private int pictureRequestCode = 1;
	private int skintoneRequestCode = 2;
	
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.modify_profile_activity);
		
		mProfilePicture = (ImageButton) findViewById(R.id.profile_pic_button);
		mScreenNameET = (EditText) findViewById(R.id.screen_name_et);
		mRealNameET = (EditText) findViewById(R.id.full_name_et);
		mEmailUpdatesBox = (CheckBox) findViewById(R.id.receive_email_updates);
		mChangeSkintone = (Button) findViewById(R.id.choose_skintone);
		mSubmitChanges = (ImageButton) findViewById(R.id.profile_submit);	
		mSetQuestions = (Button) findViewById(R.id.set_secret_questions);
		
		mSetQuestions.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        setQuestions();
		    }
		});
		
		mProfilePicture.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        getNewProfilePic();
		    }
		});
		
		mChangeSkintone.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        getNewSkintone();
		    }
		});
		
		mSubmitChanges.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        submitChanges();
				finish();
		    }
		});
	
	}
	
	private void setQuestions(){
		Intent setQuestions = new Intent(this, SetQuestions.class);
		startActivity(setQuestions);
	}
	
	private void getNewProfilePic(){
		Intent getPicture = new Intent(this, ProfilePicture.class);
		getPicture.putExtra("requestCode", pictureRequestCode);
		startActivityForResult(getPicture, pictureRequestCode);
	}
	
	private void getNewSkintone(){
		Intent getSkintone = new Intent(this, UpdateSkintoneActivity.class);
		getSkintone.putExtra("requestCode", skintoneRequestCode);
		startActivityForResult(getSkintone, skintoneRequestCode);
	}
	
	private void submitChanges(){
		getEditTextContent();
		getCheckBoxContent();
		//If user didn't not choose a new profile picture
		if (mProfilePicBitmap == null){
			mProfilePicBitmap = CommonData.profilePicture;
		}
		//If they didn't choose new skintone
		if (mSkinTone == null){
			mSkinTone = CommonData.skinTone;
		}
		prepareJSONObject();
		sendJSONObject();
	}
	
	private void sendJSONObject(){
		SmirkApiCalls.updateProfileHTTP(mProfilePicBitmap, mUpdatedUser, getApplicationContext());
	}
	
	private void prepareJSONObject(){
		mUpdatedUser = new JSONObject();
		try{
			mUpdatedUser.put("email", CommonData.email);
			mUpdatedUser.put("password", CommonData.password);
			mUpdatedUser.put("points", CommonData.points);
			mUpdatedUser.put("screenName", mNewScreenName);
			mUpdatedUser.put("name", mRealName);
			mUpdatedUser.put("skinTone", mSkinTone);
			mUpdatedUser.put("offerEmail", mEmailUpdates);
			mUpdatedUser.put("useCamera", CommonData.useCamera);
		} catch (JSONException e){
			e.printStackTrace();
		}
	}
	
	private void getEditTextContent(){
		mNewScreenName = mScreenNameET.getText().toString();
		mRealName = mRealNameET.getText().toString();
		//If new screen name or real name not entered, use old ones
		if (mNewScreenName.length() == 0){ mNewScreenName = CommonData.userName; }
		else{ CommonData.setUserName(mNewScreenName); }
		if (mRealName.length() == 0){ mRealName = CommonData.name; }
		else{ CommonData.setName(mRealName); }
	}
	
	private void getCheckBoxContent(){
		if (mEmailUpdatesBox.isChecked()){ mEmailUpdates = 1;}
		else {mEmailUpdates = 0;}
	}
	
	protected void onActivityResult(int requestCode, int resultCode, Intent data) {
	    if (requestCode == pictureRequestCode) {
	        if(resultCode == RESULT_OK){
	            mProfilePicUri = Uri.parse(data.getStringExtra("profilePicture"));
				mAppliedRotations = data.getIntExtra("profilePictureRotations", 0);
				mProfilePicBitmap = getBitmapFromUri(mProfilePicUri);
				
				mProfilePicBitmap = Bitmap.createScaledBitmap(mProfilePicBitmap, 250, 250, false);
				mProfilePicBitmap = Utils.getRoundedCornerBitmap(mProfilePicBitmap, mProfilePicBitmap.getWidth());
				mProfilePicBitmap = Utils.addCircleFrame(mProfilePicBitmap);
				
				for (int i=0; i<mAppliedRotations; i++){
					mProfilePicBitmap = Utils.RotateBitmap(mProfilePicBitmap, 90);
				}
				
				mProfilePicture.setImageBitmap(mProfilePicBitmap);
				CommonData.setProfilePicture(mProfilePicBitmap);
	        }
	
	        if (resultCode == RESULT_CANCELED){}
	
	 	if (requestCode == skintoneRequestCode) {
			if(resultCode == RESULT_OK){
				mSkinTone = data.getStringExtra("skinTone");
			}
		}
	    }
	}
	
	public Bitmap getBitmapFromUri (Uri imageUri){
		Bitmap bitmap = null;
		try{
			bitmap = MediaStore.Images.Media.getBitmap(this.getContentResolver(), imageUri);
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		if (bitmap == null){Log.d(TAG, "Profile Bitmap gotten from the Uri was null!");}
		return bitmap;
	}
	
	public byte[] getByteArr(Bitmap bitmap){
		ByteArrayOutputStream byteArrayOutputStream = new ByteArrayOutputStream();  
		bitmap.compress(Bitmap.CompressFormat.PNG, 100, byteArrayOutputStream);
		byte[] byteArray = byteArrayOutputStream .toByteArray();
		return byteArray;
	}
	
	public String getBase64String(byte[] array){
		String encoded = Base64.encodeToString(array, Base64.DEFAULT);
		return encoded;
	}
	
}