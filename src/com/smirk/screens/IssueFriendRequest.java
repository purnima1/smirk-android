package com.smirk.screens;

import org.json.JSONException;
import org.json.JSONObject;

import android.app.DialogFragment;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Toast;

import com.smirk.R;
import com.smirk.commom.CommonData;
import com.smirk.commom.Constants;
import com.smirk.network.SmirkApiCalls;

public class IssueFriendRequest extends DialogFragment {
	public static final String TAG = "IssueFriendRequest";
	String myInviteUrl = null;

	private BroadcastReceiver inviteFriendPostBroadcastReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.d(TAG, "inviteFriend - BR onReceive");
			Bundle extras = intent.getExtras();
			if (extras != null) {
				String res = intent.getStringExtra("RESULT");  
				
				//String res contains success or failure codes
				//0 = success, 1013 = failure
				Log.d(TAG, " inside onReceive : " + res);
				
				if (res.contains("1013")){
					
					getDialog().setTitle("You've already invited this person!");
				}
				else{
					getDialog().setTitle("Invitation Sent");
				}
				
				//dismiss();
				// display invite success to the user
			}
			else{
				getDialog().setTitle("Invitation could not be sent; please try again later");
				//dismiss();
			}
		}
	};

    public static IssueFriendRequest newInstance() {
		//Could put user's screen name or picture in here
        IssueFriendRequest f = new IssueFriendRequest();
        return f;
    }

	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
            Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.invite_friend, container, false);
        int id = getArguments() != null ? getArguments().getInt("Id") : null;
        myInviteUrl = Constants.INVITE_FRIEND_URL + id;
		Log.d(TAG, " get the argument -friend id : " + id + " url : " + myInviteUrl);
        
		getDialog().setCanceledOnTouchOutside(true);
		v.findViewById(R.id.msg_invite).setOnClickListener(new OnClickListener() {			
			@Override
			public void onClick(View v) {
				// TODO Auto-generated method stub				
				Log.d(TAG, "OnClick");
				SmirkApiCalls.inviteFriendJsonObjectRequestQueue(getActivity(), myInviteUrl, CommonData.sessionId, null);
			}
		});
        return v;
    }

	
	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		Log.d(TAG, "Register the BR");
		getActivity().registerReceiver(inviteFriendPostBroadcastReceiver,
				new IntentFilter("InviteFriend"));
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		Log.d(TAG, "Unregister the BR");
		getActivity().unregisterReceiver(inviteFriendPostBroadcastReceiver);
	}
	
	
}