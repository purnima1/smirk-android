package com.smirk.screens;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;

import com.smirk.R;
import com.smirk.commom.CommonData;

public class UpdateSkintoneActivity extends Activity implements OnClickListener {
	
	public final static String TAG = "UpdateSkinToneActivity";
	
	private int[] mImgBtnId = { R.id.colorwheel_goldenfair_imgbtn_id,
			R.id.colorwheel_fair_imgbtn_id, R.id.colorwheel_light_imgbtn_id,
			R.id.colorwheel_medium_imgbtn_id,
			R.id.colorwheel_mediumcool_imgbtn_id,
			R.id.colorwheel_bronzemedium_imgbtn_id,
			R.id.colorwheel_bronzedark_imgbtn_id,
			R.id.colorwheel_lighttan_imgbtn_id, R.id.colorwheel_tan_imgbtn_id,
			R.id.colorwheel_dark_imgbtn_id, R.id.colorwheel_deep_imgbtn_id,
			R.id.submit_imgbtn_id };
	private ImageButton mImgBtn;
	// default RGB values
	private int[] mRGB = { 243, 241, 191 };
	private ImageView mImageView;
	private int mTvId = R.id.colorwheel_goldenfair_tv_id;
	private String skinTone;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_skin_tone);
		
		for (int i = 0; i < mImgBtnId.length; i++) {
			mImgBtn = (ImageButton) findViewById(mImgBtnId[i]);
			mImgBtn.setOnClickListener(this);
		}

		mImageView = (ImageView) findViewById(R.id.color_selected_iv_id);	
	}
	
	@Override
	public void onClick(View v) {
		int drawableId = 0;
		switch (v.getId()) {
		case R.id.colorwheel_goldenfair_imgbtn_id:
			mTvId = R.id.colorwheel_goldenfair_tv_id;
			drawableId = R.drawable.colorsample_goldenfair;
			Log.i("***********************", "************** tvid = " + mTvId);
			// parseRGB(R.id.colorwheel_goldenfair_tv_id, mRGB);
			// mImageView.setImageDrawable(this.getResources().getDrawable(R.drawable.colorsample_goldenfair));
			break;
		case R.id.colorwheel_fair_imgbtn_id:
			mTvId = R.id.colorwheel_fair_tv_id;
			drawableId = R.drawable.colorsample_fair;
			break;
		case R.id.colorwheel_light_imgbtn_id:
			mTvId = R.id.colorwheel_light_tv_id;
			drawableId = R.drawable.colorsample_light;
			break;
		case R.id.colorwheel_medium_imgbtn_id:
			mTvId = R.id.colorwheel_medium_tv_id;
			drawableId = R.drawable.colorsample_medium;
			break;
		case R.id.colorwheel_mediumcool_imgbtn_id:
			mTvId = R.id.colorwheel_mediumcool_tv_id;
			drawableId = R.drawable.colorsample_mediumcool;
			break;
		case R.id.colorwheel_bronzemedium_imgbtn_id:
			mTvId = R.id.colorwheel_bronzemedium_tv_id;
			drawableId = R.drawable.colorsample_bronzemedium;
			break;
		case R.id.colorwheel_bronzedark_imgbtn_id:
			mTvId = R.id.colorwheel_bronzedark_tv_id;
			drawableId = R.drawable.colorsample_bronzedark;
			break;
		case R.id.colorwheel_lighttan_imgbtn_id:
			mTvId = R.id.colorwheel_lighttan_tv_id;
			drawableId = R.drawable.colorsample_lighttan;
			break;
		case R.id.colorwheel_tan_imgbtn_id:
			mTvId = R.id.colorwheel_tan_tv_id;
			drawableId = R.drawable.colorsample_tan;
			break;
		case R.id.colorwheel_dark_imgbtn_id:
			mTvId = R.id.colorwheel_dark_tv_id;
			drawableId = R.drawable.colorsample_dark;
			break;
		case R.id.colorwheel_deep_imgbtn_id:
			mTvId = R.id.colorwheel_deep_tv_id;
			drawableId = R.drawable.colorsample_deep;
			break;
		case R.id.submit_imgbtn_id:
			TextView tv = (TextView) findViewById(mTvId);
			skinTone = tv.getText().toString();
			Intent resultIntent = new Intent();
			resultIntent.putExtra("skinTone", skinTone);
			setResult(Activity.RESULT_OK, resultIntent);
			CommonData.setSkinTone(skinTone);
			this.finish();
			break;

		}
		if (v.getId() != R.id.submit_imgbtn_id) {
			mImageView.setImageDrawable(this.getResources().getDrawable(drawableId));
		}
	}
	
	
}