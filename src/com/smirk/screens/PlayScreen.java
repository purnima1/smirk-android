package com.smirk.screens;

import java.util.Random;
import java.util.List;
import java.util.ArrayList;

import android.app.Dialog;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.View.OnClickListener;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.GridView;
import android.widget.FrameLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.ImageButton;
import android.app.Fragment;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.widget.TabHost;
import android.util.Log;

import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils;
import com.smirk.commom.Utils.Screens;
import com.smirk.screens.play.CameraActivity;
import com.smirk.screens.play.ViewSmirkFragment;
import com.smirk.screens.play.AnimatedTextCameraActivity;
import com.smirk.commom.*;


public class PlayScreen{
	public static final String TAG = "PlayScreen";
	
	public static int mPlayIndicatorId;

	private SmirkMainActivity mActivity;
	private LayoutInflater mInflater;
	private FrameLayout mContentView;
	
	private GridView mEyesGridView;
	private GridView mMouthsGridView;
	private GridView mPhrasesGridView; //These will be mini views that can be set visible or invisible, depending on what's selected
	
	//private int[] mExpressionChoices = {R.id.eyes, R.id.mouths, R.id.phrases};
	//private Button[] mExpressionBtns;
	private Button eyeButton, mouthButton, phraseButton;
	
	private Dialog mDialog;
	
	private Random rand = new Random();
	
	private FragmentManager fragmentManager;
 	private FragmentTransaction fragmentTransaction;

	private ViewGroup mView;
	
	private List<SmirkResource> eyeSmirks;
	private List<SmirkResource> mouthSmirks;
	private List<SmirkResource> phraseSmirks;
	
	public PlayScreen(SmirkMainActivity activity){
		mActivity = activity;
		mContentView = (FrameLayout) mActivity.findViewById(R.id.content_layout_id);
		
		mView = (ViewGroup) mActivity.getLayoutInflater().inflate(R.layout.play, mContentView,false);		

		eyeButton = (Button) mView.findViewById(R.id.eyes);
		mouthButton = (Button) mView.findViewById(R.id.mouths);
		phraseButton = (Button) mView.findViewById(R.id.phrases);

	}
	
	public void showScreen(){
		mActivity.setCurrentScreen(Screens.PlayScreen);
		((ImageView)mActivity.findViewById(R.id.top_bar_title_iv_id)).setImageDrawable(mActivity.getResources().getDrawable(R.drawable.time_to_smirk));
		
		mActivity.findViewById(R.id.modify_user_button).setVisibility(View.GONE);
		
		mContentView.removeAllViews();
				
		//initialize and set up the adapters for each expression's gridview
		if (mEyesGridView == null){
			//initialize eyeSmirks
			eyeSmirks = SmirkResource.eyeResources;
			mEyesGridView = (GridView) mView.findViewById(R.id.eyes_gridlayout);
			final EyesGridViewAdapter eyesAdapter = new EyesGridViewAdapter(mActivity, eyeSmirks);
		    mEyesGridView.setAdapter(eyesAdapter);
			Log.d(TAG, "eyeSmirks adapter set to gridview");
		}
		
		if (mMouthsGridView == null){
			//initialize mouthSmirks
			mouthSmirks = SmirkResource.mouthResources;
			mMouthsGridView = (GridView) mView.findViewById(R.id.mouths_gridlayout);
			final MouthsGridViewAdapter mouthsAdapter = new MouthsGridViewAdapter(mActivity, mouthSmirks);
			mMouthsGridView.setAdapter(mouthsAdapter);
			Log.d(TAG, "mouthSmirks adapter set to gridview");
		}
		
		if (mPhrasesGridView == null){
			//initialize phraseSmirks
			phraseSmirks = SmirkResource.wordResources;
			mPhrasesGridView = (GridView) mView.findViewById(R.id.phrases_gridlayout);
			final PhrasesGridViewAdapter phrasesAdapter = new PhrasesGridViewAdapter(mActivity, phraseSmirks);
			mPhrasesGridView.setAdapter(phrasesAdapter);
			Log.d(TAG, "phraseSmirks adapter set to gridview");
		}
		
		
		eyeButton.setOnClickListener(new OnClickListener(){
		            @Override
		            public void onClick(View v) {
						mEyesGridView.setVisibility(View.VISIBLE);
						mMouthsGridView.setVisibility(View.GONE);
						mPhrasesGridView.setVisibility(View.GONE);
		            }});
		
		mouthButton.setOnClickListener(new OnClickListener(){
		            @Override
		            public void onClick(View v) {
						mMouthsGridView.setVisibility(View.VISIBLE);
						mEyesGridView.setVisibility(View.GONE);
						mPhrasesGridView.setVisibility(View.GONE);
		            }});
		
		phraseButton.setOnClickListener(new OnClickListener(){
		            @Override
		            public void onClick(View v) {
						mPhrasesGridView.setVisibility(View.VISIBLE);
						mEyesGridView.setVisibility(View.GONE);
						mMouthsGridView.setVisibility(View.GONE);
		            }});
		
		mContentView.addView(mView);
	}
	
	public static void setIndicatorId(int indicatorId){
		mPlayIndicatorId = indicatorId;
	}
	
	public static int getIndicatorId(){
		return mPlayIndicatorId;
	}
}
