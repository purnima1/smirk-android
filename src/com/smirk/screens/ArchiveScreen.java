package com.smirk.screens;

import java.util.List;
import java.util.ArrayList;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.graphics.Bitmap;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Color;
import android.util.Log;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.app.FragmentManager;
import android.app.FragmentTransaction;
import android.support.v4.app.FragmentActivity;

import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils.Screens;
import com.smirk.commom.SmirkImage;
import com.smirk.commom.SmirkArchiveAdapter;
import com.smirk.commom.CommonData;
import com.smirk.network.SmirkApiCalls;

public class ArchiveScreen extends FragmentActivity{
	private static final String TAG = "ArchiveScreen";

	private SmirkMainActivity mActivity;
	LayoutInflater mInflater;
	private Button mPicture;
	private Bitmap mPictureBitmap;
	private FrameLayout mContentView;
	private GridView mPicturesGrid;
	private int mWidth, mHeight;
	
	private FragmentManager fragmentManager;
 	private FragmentTransaction fragmentTransaction;

	private GridView gridview;
	private ViewGroup view;
	
	public ArchiveScreen(SmirkMainActivity activity){
		mActivity = activity;
		mContentView = (FrameLayout) mActivity.findViewById(R.id.content_layout_id);
		view = (ViewGroup) mActivity.getLayoutInflater().inflate(R.layout.archive, mContentView, false);
		gridview = (GridView) view.findViewById(R.id.archive_gridlayout);
	}
	
    public void onFragmentFinish() {
        getFragmentManager().popBackStack();
		this.showScreen(); //may've removed elements from smirkImages, and so must redraw the screen
    }

	public void showScreen(){
		mActivity.setCurrentScreen(Screens.ArchiveScreen);
		((ImageView)mActivity.findViewById(R.id.top_bar_title_iv_id)).setImageDrawable(mActivity.getResources().getDrawable(R.drawable.header_text_archive));

		checkPictures();
		
		if (CommonData.userSmirkedPictures.size() != 0){
			drawPicturesGrid();
		}
		
		mContentView.removeAllViews();
	
		getWindowingMetrics();
		
		mContentView.addView(view);		
	}
	
	private void checkPictures(){	
		SmirkApiCalls.getUserSmirkedPics();
	}
	
	private void drawPicturesGrid(){
		//final SmirkArchiveAdapter archiveAdapter = new SmirkArchiveAdapter(mActivity, mActivity.smirkImages, mWidth, mHeight);
		final SmirkArchiveAdapter archiveAdapter = new SmirkArchiveAdapter(mActivity, CommonData.userSmirkedPictures, mWidth, mHeight);
	    gridview.setAdapter(archiveAdapter);

	    gridview.setOnItemClickListener(new OnItemClickListener() {
			@Override
	        public void onItemClick(AdapterView<?> parent, View v, int position, long id) {	
				fragmentManager = mActivity.getFragmentManager();
				fragmentTransaction = fragmentManager.beginTransaction();
							
				PhotoOptionsScreen photoScreen = new PhotoOptionsScreen(mActivity, CommonData.userSmirkedPictures, position, fragmentManager);
				fragmentTransaction.add(R.id.content_layout_id, photoScreen);
				fragmentTransaction.addToBackStack(null);
				fragmentTransaction.commit();
				
	        }
	    });
	}
	
	private void getWindowingMetrics(){
		//This returns the widths and heights of the pictures to be rendered by the GridView adapter
		DisplayMetrics dm = new DisplayMetrics(); 
		mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		
		if (dm.widthPixels < 650){
			//Lower resolution device, namely the Tab2 7.0
			mWidth = 175;
			mHeight = 125;}
			
		else if (dm.widthPixels > 650){
			//Higher resolution device, namely the Galaxy S3
			mWidth = 200;
			mHeight = 150;}
	}
}
