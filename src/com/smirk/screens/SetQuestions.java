package com.smirk.screens;

import android.app.Activity;
import android.os.Bundle;
import android.content.Context;
import android.content.Intent;
import android.widget.TextView;
import android.widget.EditText;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.Toast;
import android.view.View;
import android.view.View.OnClickListener;
import android.util.Log;

import org.json.JSONArray;
import org.json.JSONObject;
import org.json.JSONException;

import com.smirk.commom.Utils;
import com.smirk.commom.CommonData;
import com.smirk.network.SmirkApiCalls;

import com.smirk.R;

public class SetQuestions extends Activity{
	
	private static final String TAG = "SetQuestions: ";
	
	private EditText mQuestionOne, mQuestionTwo;
	private ImageButton mSubmit;
	
	public void onCreate(Bundle bundle) {
		super.onCreate(bundle);
		setContentView(R.layout.set_questions);
		
		mQuestionOne = (EditText) findViewById(R.id.question_one);
		mQuestionTwo = (EditText) findViewById(R.id.question_two);
		mSubmit = (ImageButton) findViewById(R.id.submit);
		
		mSubmit.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        submitAnswers();
		    }
		});
	}
	
	private void submitAnswers(){
		String questionOneAnswer = mQuestionOne.getText().toString();
		String questionTwoAnswer = mQuestionTwo.getText().toString();
		
		if (questionOneAnswer.equals("") || questionTwoAnswer.equals("")){
			Toast.makeText(getApplicationContext(), "One or both questions were missing answers!", Toast.LENGTH_LONG).show();
		}
		else{
			Log.d(TAG, "User Email: "+CommonData.email);
			String email = CommonData.email;
			
			JSONObject answers = new JSONObject();
			JSONArray answersArray = new JSONArray();
			JSONObject answerOne = new JSONObject();
			JSONObject answerTwo = new JSONObject();
			try{
				//answerOne.put("id", "1");
				answerOne.put("answer", questionOneAnswer);
				answerOne.put("question", 1);
				//answerOne.put("email", email);
				
				answersArray.put(answerOne);
				
				//answerTwo.put("id", "2");
				answerTwo.put("answer", questionTwoAnswer);
				answerTwo.put("question", 2);
				//answerTwo.put("email", email);
				
				answersArray.put(answerTwo);
				
				answers.put("secretQAList", answersArray);
			} catch (JSONException e){
				e.printStackTrace();
			}
			Log.d(TAG, "Answer JSON object string: "+answers.toString());
			SmirkApiCalls.setSecretAnswers(getApplicationContext(), answers);	
			finish();
		}
	}
	
}