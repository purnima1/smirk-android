package com.smirk.screens;

import java.util.ArrayList;
import java.util.List;

import java.io.InputStream;
import java.io.BufferedInputStream;

import java.net.URL;
import java.net.URLConnection;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import android.os.AsyncTask;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;
import android.os.Bundle;
import android.graphics.drawable.Drawable;
import android.widget.TextView;
import android.widget.Button;
import android.widget.ImageButton;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.GridLayout;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.database.Cursor;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Color;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Typeface;
import android.view.View.OnClickListener;
import android.view.Window;
import android.view.WindowManager;

import com.smirk.R;
import com.smirk.commom.CommonData;
import com.smirk.commom.Constants;
import com.smirk.commom.Friend;
import com.smirk.commom.Utils;
import com.smirk.network.SmirkApiCalls;

public class FindFriendsFragment extends Fragment {
	private static final String TAG = "FindFriendsFragment";
	View view;
	String initialInput = null;
	List<Friend> matchingUsersList;
	//EditText narrowSearch;
	Button userButton;
	GridLayout matchingUsersLayout;
	int matchingUsers;
	ImageButton backX;
	TextView textDisplay;

	String mySearchUrl = null;
	ArrayList<Friend> usersList = new ArrayList<Friend>();;
	int inviteId;
	Bundle bundle;
	
	Bitmap friendProfilePic;
	
	boolean receiverRegistered;
	
	public FindFriendsFragment() {}
	
	public static FindFriendsFragment getInstance(){
		return new FindFriendsFragment();
	}
	
	private BroadcastReceiver findFriendGetBroadcastReceiver = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				// TODO Auto-generated method stub
				Log.d(TAG, "BR onReceive");
				Bundle extras = intent.getExtras();
				if (extras != null) {
					String res = intent.getStringExtra("RESULT");
					try {
						if (res.equals("NOUSERS")){
							notifyFailure();
						} else{
							parseResult(new JSONObject(res));
						}
					} catch (JSONException e) {
						// TODO Auto-generated catch block
						Log.d(TAG, e.toString());
					}

				}
			}
		};
	

	@Override
	public View onCreateView(LayoutInflater inflater, ViewGroup container,
			Bundle savedInstanceState) {
		// Inflate the layout for this fragment
		view = inflater.inflate(R.layout.find_friends, container, false);
		backX = (ImageButton) view.findViewById(R.id.x_out);
		
		backX.setOnClickListener(new OnClickListener() {
			@Override
			public void onClick(View v){
				//showScreen(); 	
			  	getActivity().getFragmentManager().popBackStack();
			}
		});

		initialInput = getArguments() != null ? getArguments().getString(
				"Search") : null;
		Log.d(TAG, " get the argument string : " + initialInput);

		matchingUsersLayout = (GridLayout) view.findViewById(R.id.matching_users_view);
		populateUsers();

		return view;
	}

	@Override
	public void onResume() {
		// TODO Auto-generated method stub
		super.onResume();
		//Log.d(TAG, "Register the BR");
		getActivity().registerReceiver(findFriendGetBroadcastReceiver, new IntentFilter("UsersGet"));
		receiverRegistered = true;
	}

	@Override
	public void onPause() {
		// TODO Auto-generated method stub
		super.onPause();
		if (receiverRegistered == true){
			Log.d(TAG, "Unregister the BR");
			getActivity().unregisterReceiver(findFriendGetBroadcastReceiver);
		}
	}
	
	private void notifyFailure(){
		textDisplay = (TextView) view.findViewById(R.id.failure_tv);
		textDisplay.setVisibility(View.VISIBLE);
		textDisplay.setText("No matching users found.");
	}

	private void drawUsers() {
		matchingUsersLayout.removeAllViews();
		int columnCount = 3;
		int rowCount = getRowCount(usersList.size());
		matchingUsersLayout.setColumnCount(columnCount);
		matchingUsersLayout.setRowCount(rowCount);
		
		for (Friend user : usersList) {
			userButton = new Button(this.getActivity());

			userButton.setCompoundDrawablesWithIntrinsicBounds(null, user.getUserBitmap(), null, null);
			
			userButton.setBackgroundColor(Color.TRANSPARENT);
			userButton.setText(user.getScreenName());
			inviteId = user.getId();
			userButton.setTextColor(Color.WHITE);
			userButton.setTypeface(Typeface.DEFAULT_BOLD);

			userButton.setOnClickListener(new OnClickListener() {
				@Override
				public void onClick(View v) {
					Log.d(TAG, " Id to pass is : " + inviteId);								        
					issueFriendRequest(v);				}
			});

			// userButton.setPadding(0,0,20,0);
			matchingUsersLayout.addView(userButton);
			matchingUsersLayout.invalidate();
		}
	}

	private void populateUsers() {
		mySearchUrl = Constants.USERS_SEARCH_URL + initialInput;
		Log.d(TAG, " Before calling search api- url " + mySearchUrl);
		SmirkApiCalls.getJsonObjectRequestQueue(getActivity(), mySearchUrl, CommonData.sessionId, null);
	}

	private void parseResult(JSONObject obj) {
		Log.d(TAG, "Inside parse");
		JSONArray usersJArray;
		JSONObject usersJObject = null;
		
		Log.d(TAG, "Unregister the BR");
		getActivity().unregisterReceiver(findFriendGetBroadcastReceiver);
		receiverRegistered = false;

		if (obj != null) {
			Log.d(TAG, " obj is not null");

			try {
				if (obj.has("userList")) {
					Log.d(TAG, " has userList");

					usersJArray = obj.getJSONArray("userList");
					Log.d(TAG, "list size is :" + usersJArray.length());

					for (int i = 0; i < usersJArray.length(); i++) {

						usersJObject = usersJArray.getJSONObject(i);
						Log.d(TAG,"Value is Name:"	+ usersJObject.getString("screenName"));
						Log.d(TAG,"Value is picUrl:" + usersJObject.getString("profilePicture"));
						
						String profilePicURL = usersJObject.getString("profilePicture");
						profilePicURL = checkAndPrepareProfileURL(profilePicURL);
						downloadProfileImage(profilePicURL, usersJObject);
					}
				} // has userList
			} catch (JSONException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}

		} // end if

	}
	
	public String checkAndPrepareProfileURL(String profilePicURL){
		String preparedURL = "";
		if (profilePicURL.contains("23.253.166.61")){
			preparedURL = "http://"+profilePicURL;
		}
		else if (profilePicURL.contains("graph.facebook.com")){
			preparedURL = profilePicURL;
		}
		else{
			preparedURL = "http://23.253.166.61"+profilePicURL;
		}
		return preparedURL;
	}

	public int getRowCount(int matchingUsers) {
		// We have 3 columns: compute how many rows we need
		if (matchingUsers <= 3) {
			// only one row needed
			return 1;
		} else if (matchingUsers % 3 == 0) {
			// matchingUsers divisible by 3
			return matchingUsers / 3;
		} else {
			// matchingUsers has remainder to be put on next line
			return (matchingUsers / 3) + 1;
		}
	}

	public void issueFriendRequest(View v) {
	    bundle = new Bundle();
	    bundle.putInt("Id", inviteId);
	    FragmentTransaction ft = getFragmentManager().beginTransaction();
		IssueFriendRequest requestDialog = IssueFriendRequest.newInstance();
		requestDialog.setArguments(bundle);
		requestDialog.show(ft, "dialog");
	}
	
	public void downloadProfileImage(final String imageURL, final JSONObject usersJObject){
		//If iUserOrFriend = true, it's the user's own profile picture. If false, it's a friend's picture.
		new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(final Void ... params) {
		        Bitmap profilePicture = null;
		        try {
						Log.d(TAG, "Before downloading the image at: "+imageURL);
						URL url = new URL(imageURL);
						URLConnection urlConnection = url.openConnection();
						InputStream in = new BufferedInputStream(urlConnection.getInputStream());

						profilePicture = BitmapFactory.decodeStream(in);

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        return profilePicture;
			}
			@Override
			protected void onPostExecute(Bitmap result) {
				setFriendProfileImageResult(result, usersJObject);
		    }
		}.execute();	
	}

	private void setFriendProfileImageResult(Bitmap result, JSONObject usersJObject){
		Log.d(TAG, "Entered setFriendProfileImageResult in FindFriendsFragment");
		Drawable friendPicDrawable = null;
		Friend friendItem = null;
		
		//crop and make into circle
		if (result != null){
			friendProfilePic = Utils.getRoundedCornerBitmap(result, result.getWidth());
			friendProfilePic = Utils.scaleBitmap(friendProfilePic, 120f, 120f);
			friendPicDrawable = new BitmapDrawable(getActivity().getResources(), friendProfilePic);

		} else{
			//set Drawable friendPicDrawable to the default
			Log.d(TAG, "Friend image was null");
			friendPicDrawable = getActivity().getResources().getDrawable(R.drawable.button_profilepic_default_white);
		}
		
		try{
			friendItem = new Friend(usersJObject.getString("screenName"), usersJObject.getString("profilePictureUrl"), usersJObject.getInt("id"),
			 friendPicDrawable);
		} catch (JSONException e){
			Log.d(TAG, "Error creating Friend object from JSON");
			e.printStackTrace();
		}

		usersList.add(friendItem);
		Log.d(TAG, "Friend added to list");
		drawUsers();
	}
	

}