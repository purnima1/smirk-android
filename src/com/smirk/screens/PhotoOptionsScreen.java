package com.smirk.screens;

import java.util.List;
import java.util.ArrayList;
import java.io.File;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.net.MalformedURLException;

import android.view.LayoutInflater;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.GridView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;
import android.widget.ImageButton;
import android.view.View.OnClickListener;
import android.view.View;
import android.widget.AdapterView;
import android.widget.AdapterView.OnItemClickListener;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Color;
import android.graphics.drawable.LayerDrawable;
import android.graphics.drawable.Drawable;
import android.content.res.Resources;
import android.content.pm.PackageManager;
import android.util.Log;
import android.net.Uri;
import android.net.ConnectivityManager;
import android.content.Intent;
import android.content.Context;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.FragmentManager;
import android.os.AsyncTask;
import android.os.Bundle;

import com.facebook.widget.FacebookDialog;
import com.facebook.widget.LoginButton;
import com.facebook.Session;
import com.facebook.SessionState;
import com.facebook.Session.StatusCallback;
import com.facebook.UiLifecycleHelper;

import twitter4j.Twitter;
import twitter4j.TwitterException;
import twitter4j.TwitterFactory;
import twitter4j.TwitterException;
import twitter4j.StatusUpdate;
import twitter4j.auth.AccessToken;
import twitter4j.conf.Configuration;
import twitter4j.conf.ConfigurationBuilder;
import twitter4j.media.ImageUpload;
import twitter4j.media.ImageUploadFactory;
import twitter4j.media.MediaProvider;

import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils;
import com.smirk.commom.SmirkImage;
import com.smirk.commom.CommonData;
import com.smirk.network.SmirkApiCalls;

public class PhotoOptionsScreen extends Fragment{
	
	private static final String TAG = "PhotoOptionsScreen";
	
	View view;

	private SmirkMainActivity mActivity;
	LayoutInflater mInflater;
	private ImageView reactionPictureIV, indicatorPictureIV;
	private Bitmap mReactionPicture, mIndicatorPicture;
	private ImageButton mCancel, mDelete;
	private ImageButton mFacebook, mTwitter, mInstagram, mSmirk, mMessage, mMail;
	private int mIndicatorResource;
	private int mPosition;
	private FragmentManager mFragmentManager;
	private String imageUrl;
	
	private File mFile;
	
	private UiLifecycleHelper uiHelper;
	
	public PhotoOptionsScreen(SmirkMainActivity activity, List<SmirkImage> smirkImages, int position, FragmentManager fragmentManager){
		mActivity = activity;
		mReactionPicture = smirkImages.get(position).getPicture();
		imageUrl = smirkImages.get(position).getUrl();
		mPosition = position;
		mFragmentManager = fragmentManager;
	}
	
	
	@Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        // Inflate the layout for this fragment
		Drawable[] layers = new Drawable[2];
		Resources r = mActivity.getResources();
		
		mActivity.hideBottomBar();
		
        view = inflater.inflate(R.layout.photo_options_screen, container, false);

		uiHelper = new UiLifecycleHelper(mActivity, null);
		uiHelper.onCreate(savedInstanceState);
		
		reactionPictureIV = (ImageView) view.findViewById(R.id.reaction_picture);
		mCancel = (ImageButton) view.findViewById(R.id.cancel_picture);
		mDelete = (ImageButton) view.findViewById(R.id.delete_picture);
		
		//mFacebook, mTwitter, mInstagram, mSmirk, mMessage, mMail;
		mFacebook = (ImageButton) view.findViewById(R.id.facebook_imgbtn_id);
		mTwitter = (ImageButton) view.findViewById(R.id.twitter_imgbtn_id);
		mInstagram = (ImageButton) view.findViewById(R.id.intagram_imgbtn_id);
		mSmirk = (ImageButton) view.findViewById(R.id.smirk_imgbtn_id);
		mMessage = (ImageButton) view.findViewById(R.id.message_imgbtn_id);
		mMail = (ImageButton) view.findViewById(R.id.mail_imgbtn_id);
		
		saveBitmapToFile(mReactionPicture);
		
		mFacebook.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				 //Need to check if  user has FB installed
				 if (!appInstalledOrNot("com.facebook")){
					Toast.makeText(mActivity.getApplicationContext(), "The Facebook app must be installed for this to work, please download in Google Play!"
					, Toast.LENGTH_LONG).show();
				 }
				
				 if (imageUrl.equals("null")){
					Toast.makeText(mActivity.getApplicationContext(), "Image hasn't been uploaded to the server yet -- please try again soon."
					, Toast.LENGTH_LONG).show();
				} 	
		
				  FacebookDialog shareDialog = new FacebookDialog.ShareDialogBuilder(mActivity)
				        .setLink(imageUrl)
						.setCaption("You've been Smirked!")
				        .build();
				  uiHelper.trackPendingDialogCall(shareDialog.present());
				  
	              //mFragmentManager.popBackStack();
				  //mActivity.showBottomBar();
	            }
	         });
	
   		mTwitter.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  twitterShare();
	              //mFragmentManager.popBackStack();
				  //mActivity.showBottomBar();
	            }
	         });
	
		mInstagram.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  instagramShare();
	              //mFragmentManager.popBackStack();
				  //mActivity.showBottomBar();
	            }
	         });
	
		mSmirk.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  smirkShare();
	              //mFragmentManager.popBackStack();
				  //mActivity.showBottomBar();
	            }
	         });
	
		mMail.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  mailShare();
	              //mFragmentManager.popBackStack();
				  //mActivity.showBottomBar();
	            }
	         });
	
		mMessage.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
				  messageShare();
	              //mFragmentManager.popBackStack();
				  //mActivity.showBottomBar();
	            }
	         });
			
		mCancel.setOnClickListener(new ImageButton.OnClickListener() {  
	        public void onClick(View v){
	              mFragmentManager.popBackStack();
				  mActivity.showBottomBar();
	            }
	         });
	
		mDelete.setOnClickListener(new ImageButton.OnClickListener() {
			public void onClick(View v){
				CommonData.userSmirkedPictures.remove(mPosition);
				CommonData.smirkedPicsCount--;
				String serverPath = imageUrl.replace("http://", "");
				SmirkApiCalls.deleteSmirkImage(mActivity.getApplicationContext(), serverPath);
				mFragmentManager.popBackStack();
				mActivity.showBottomBar();
			}
		});
		
		mIndicatorPicture = BitmapFactory.decodeResource(mActivity.getResources(), mIndicatorResource);
		
		//mIndicatorPicture = Utils.addRectangularFrame(mIndicatorPicture, 5, 0);
		mReactionPicture = Utils.addRectangularFrame(mReactionPicture, 5, 1);
		
		Drawable reactionDrawable = new BitmapDrawable(r, mReactionPicture);
		Drawable indicatorDrawable = new BitmapDrawable(r, mIndicatorPicture);
		
		layers[0] = reactionDrawable;
		layers[1] = indicatorDrawable;
		
		LayerDrawable layeredDrawable = new LayerDrawable(layers);
		layeredDrawable.setLayerInset(1, 375, 300, 10, 10);
		
		reactionPictureIV.setImageDrawable(layeredDrawable);
		//reactionPictureIV.setImageBitmap(mReactionPicture);
		
		return view;
	
	}
	
	private void mailShare(){
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("text/plain");
		intent.putExtra(Intent.EXTRA_EMAIL, new String[] {"email@example.com"});
		intent.putExtra(Intent.EXTRA_SUBJECT, "subject here");
		intent.putExtra(Intent.EXTRA_TEXT, "body text");
		try{
			if (mFile != null){
				intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mFile.toURL().toString()));
			}
		} catch (MalformedURLException e){
			e.printStackTrace();
		}
		startActivity(Intent.createChooser(intent, "Send email"));
	}
	
	private void messageShare(){
		Intent intent = new Intent(Intent.ACTION_SEND);
		intent.setType("image/*");
		intent.putExtra(Intent.EXTRA_TEXT, "You've been Smirked!");
		try{
			if (mFile != null){
				intent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mFile.toURL().toString()));
			}
		} catch (MalformedURLException e){
			e.printStackTrace();
		}
		startActivity(Intent.createChooser(intent, "Send sms"));
	}
	
	private void smirkShare(){
		Toast.makeText(mActivity.getApplicationContext(), "Image shared with your Smirk friends.", Toast.LENGTH_LONG).show();
		SmirkApiCalls.imageShare(getActivity().getApplicationContext(), imageUrl);
	}
	
	private void instagramShare(){
		if (appInstalledOrNot("com.instagram.android")){
			Intent shareIntent = new Intent(Intent.ACTION_SEND);
			shareIntent.setType("image/*");
			shareIntent.putExtra(Intent.EXTRA_TEXT, "You've been smirked!");
			shareIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse("file://" + mFile.getAbsolutePath()));
			shareIntent.setPackage("com.instagram.android");
			startActivity(shareIntent);
		}
		else{
			Toast.makeText(mActivity.getApplicationContext(), "The Instagram app must be installed for this to work, please download in Google Play!"
			, Toast.LENGTH_LONG).show();
		}
	}

	private void twitterShare(){
		if (appInstalledOrNot("com.twitter.android")){
			//Intent tweet = mActivity.getPackageManager().getLaunchIntentForPackage("com.twitter.android");
			Intent tweetIntent = new Intent(Intent.ACTION_SEND);
			tweetIntent.putExtra(Intent.EXTRA_TEXT, "You've been Smirked!");
			try{
				if (mFile != null){
					tweetIntent.putExtra(Intent.EXTRA_STREAM, Uri.parse(mFile.toURL().toString()));
					tweetIntent.setType("image/png"); //mmsIntent.setType("image/*");
				}
			} catch (MalformedURLException e){
				e.printStackTrace();
			}
			
			tweetIntent.setType("text/plain");
			mActivity.startActivity(tweetIntent);
		} else{
			Toast.makeText(mActivity.getApplicationContext(), "Twitter not installed -- download from Google Play to share with it", Toast.LENGTH_LONG).show();
		}
	}
	
	
	private boolean appInstalledOrNot(String uri) {
        PackageManager pm = mActivity.getPackageManager();
        boolean app_installed = false;
        try {
            pm.getPackageInfo(uri, PackageManager.GET_ACTIVITIES);
            app_installed = true;
        }
        catch (PackageManager.NameNotFoundException e) {
            app_installed = false;
        }
        return app_installed ;
    }
	
	@Override
	public void onActivityResult(int requestCode, int resultCode, Intent data) {
	    super.onActivityResult(requestCode, resultCode, data);

	    uiHelper.onActivityResult(requestCode, resultCode, data, new FacebookDialog.Callback() {
	        @Override
	        public void onError(FacebookDialog.PendingCall pendingCall, Exception error, Bundle data) {
	            Log.e("Activity", String.format("Error: %s", error.toString()));
	        }

	        @Override
	        public void onComplete(FacebookDialog.PendingCall pendingCall, Bundle data) {
	            Log.i("Activity", "Success!");
	        }
	    });
	}
	
	private synchronized void saveBitmapToFile(final Bitmap bitmap) {		
		new AsyncTask<Void, Void, Void>() {
           @Override
           protected Void doInBackground(final Void ... params ) {
         		Log.d(TAG, "Saving bitmap. Size: " + bitmap.getByteCount());
				File file = null;
				File selfieImageLocation = null;
				if (android.os.Environment.getExternalStorageState().equals(android.os.Environment.MEDIA_MOUNTED)){
					selfieImageLocation = new File(android.os.Environment.getExternalStorageDirectory(), "SelfieSurprise/");}
				if (!selfieImageLocation.exists()){ selfieImageLocation.mkdirs();}
				//filenaming: selfieSurprise_before_systemtimemillis
				file = new File(selfieImageLocation, "selfieSurprise_after_"+System.currentTimeMillis()+".bmp");
				Log.d(TAG, "Saving bitmap. Location: " + file.toString());
				try {
					OutputStream os = new FileOutputStream(file);
					bitmap.compress(Bitmap.CompressFormat.PNG, 100, os);
					os.flush();
					os.close();
					Log.d(TAG, "Saving bitmap successful");
				} catch (Exception ex) {
					Log.d(TAG, "Saving bitmap failed");
					ex.printStackTrace();
				}
				mFile = file;
                return null;
           }
       }.execute();
	}
	
	@Override
	public void onResume() {
	    super.onResume();
	    uiHelper.onResume();
	}

	@Override
	public void onSaveInstanceState(Bundle outState) {
	    super.onSaveInstanceState(outState);
	    uiHelper.onSaveInstanceState(outState);
	}

	@Override
	public void onPause() {
	    super.onPause();
	    uiHelper.onPause();
	}

	@Override
	public void onDestroy() {
	    super.onDestroy();
	    uiHelper.onDestroy();
	}
}