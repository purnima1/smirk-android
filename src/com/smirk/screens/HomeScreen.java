package com.smirk.screens;

import java.util.List;
import java.util.Set;
import java.util.HashSet;
import java.util.ArrayList;

import android.content.Intent;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.FrameLayout;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.graphics.Bitmap;
import android.widget.TextView;
import android.widget.Space;
import android.util.DisplayMetrics;
import android.util.AttributeSet;
import android.util.Log;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.IntentFilter;

import com.smirk.HomeInstructionActivity;
import com.smirk.R;
import com.smirk.SmirkMainActivity;
import com.smirk.commom.Utils;
import com.smirk.commom.Utils.Screens;
import com.smirk.screens.home.SetupProfile;
import com.smirk.commom.SmirkImage;
import com.smirk.commom.CommonData;
import com.smirk.network.SmirkApiCalls;

public class HomeScreen implements OnClickListener {
	
	private static final String TAG = "HomeScreen: ";

	private SmirkMainActivity mActivity;
	private LayoutInflater mInflater;
	private FrameLayout mContentView;
	private LinearLayout mSampleLayout;

	private int[] mImgBtnId = { R.id.button_instruction_id };
	
	private ImageButton mImgBtn;
	private ImageButton modifyProfile;

	//private SetupProfile mSetupProfile;
	
	private String username;
	private String points = "{points}";
	private Bitmap profilePicture;
	private Bitmap sampleSmirk;
	
	private int mWidth;
	private int mHeight;

	public static List<SmirkImage> smirkImages;
	
	private TextView usernameView;
	private TextView pointsView;
	private ImageView profilePic;
	
	private BroadcastReceiver homeScreenReceiver = new BroadcastReceiver() {
		@Override
		public void onReceive(Context context, Intent intent) {
			// TODO Auto-generated method stub
			Log.d(TAG, "Receiving broadcast with homescreen stuff");
			showScreen();
		}
	};

	public HomeScreen(SmirkMainActivity activity) {
		mActivity = activity;
		mContentView = (FrameLayout) mActivity.findViewById(R.id.content_layout_id);
		mActivity.registerReceiver(homeScreenReceiver, new IntentFilter("HOMESCREEN"));
	}

	public void showScreen() {
		mActivity.setCurrentScreen(Screens.HomeScreen);
		((ImageView) mActivity.findViewById(R.id.top_bar_title_iv_id)).setImageDrawable(mActivity.getResources()
		    .getDrawable(R.drawable.home));
		ViewGroup view = (ViewGroup) mActivity.getLayoutInflater().inflate(R.layout.home, mContentView, false);
		
		mContentView.removeAllViews();
		
		usernameView = (TextView) view.findViewById(R.id.screen_name_tv_id);
		usernameView.setText(CommonData.userName);
		usernameView.invalidate();
		view.refreshDrawableState();
		
		pointsView = (TextView) view.findViewById(R.id.points_tv_id);
		pointsView.setText(String.valueOf(CommonData.points));
		pointsView.invalidate();
		view.refreshDrawableState();
		
		modifyProfile = (ImageButton) mActivity.findViewById(R.id.modify_user_button);
		modifyProfile.setVisibility(View.VISIBLE);
		
		mSampleLayout = (LinearLayout) view.findViewById(R.id.preview_sample_scroll_one);
		
		getWindowingMetrics();
		
		checkNewSharedPics();
		
		if (CommonData.profilePicture != null){ 
			profilePic = (ImageView) view.findViewById(R.id.icon_profile_bimgbtn_id);
			this.profilePicture = CommonData.profilePicture;
			profilePicture = Utils.addCircleFrame(profilePicture);
			profilePic.setImageBitmap(profilePicture);
			profilePic.invalidate();
			view.refreshDrawableState();
		}
		
		modifyProfile.setOnClickListener(new View.OnClickListener() {
		    @Override
		    public void onClick(View v) {
		        Intent intent = new Intent(mActivity, ModifyProfile.class);
				mActivity.startActivity(intent);
		    }
		});

		for (int i = 0; i < mImgBtnId.length; i++) {
			mImgBtn = (ImageButton) view.findViewById(mImgBtnId[i]);
			mImgBtn.setOnClickListener(this);
		}
	
		mContentView.addView(view);
	}
	
	
	private void checkNewSharedPics(){
		SmirkApiCalls.refreshSignIn(mActivity.getApplicationContext());
		
		if (CommonData.userSharedPictures.size() != 0 && smirkImages == null){
			//First drawing
			smirkImages = CommonData.userSharedPictures;
			Log.d(TAG, "Shared images count: "+smirkImages.size());
			drawSharedPics();
		} else if (CommonData.userSharedPictures.size() != 0 && smirkImages != null){
			//Subsequent checks for new images, in which case we check only for new pictures
			smirkImages = CommonData.userSharedPictures;
			Log.d(TAG, "Shared images count: "+smirkImages.size());
			cleanDuplicates(); //hash set solution wasn't working after new download
			Log.d(TAG, "Shared images count: "+smirkImages.size());
			mSampleLayout.removeAllViews();
			mSampleLayout.invalidate();
			drawSharedPics();
		}
	}
	
	private void cleanDuplicates(){
		List<SmirkImage> cleanList = new ArrayList<SmirkImage>();
		for (int i=0; i<smirkImages.size(); i++){
			if (! cleanList.contains(smirkImages.get(i))){
				cleanList.add(smirkImages.get(i));
			}
		}
		smirkImages.clear();
		smirkImages.addAll(cleanList);
		CommonData.userSharedPictures = smirkImages;
	}
	
	private void drawSharedPics(){
		for (int i=0; i<smirkImages.size(); i++){
			ImageButton sampleView = new ImageButton(mActivity);
			Space spacerView = new Space(mActivity);
			LinearLayout.LayoutParams spaceParams = new LinearLayout.LayoutParams(10, 10); //space of 10dip width and height
			spacerView.setLayoutParams(spaceParams);
			
			sampleSmirk = smirkImages.get(i).getPicture(); //get sample Smirk
			
			if (sampleSmirk != null){
				sampleSmirk = Bitmap.createScaledBitmap(sampleSmirk, mWidth, mHeight, false); //resize as needed for form factor
				sampleSmirk = Utils.addRectangularFrame(sampleSmirk, 3, 0); //frame width = 3; color = black
			
				sampleView.setImageBitmap(sampleSmirk);
				
				final int index = i;
				
				sampleView.setOnClickListener(new View.OnClickListener() {
				    @Override
				    public void onClick(View v) {
				        Intent intent = new Intent(mActivity, ViewSharePicture.class);
						intent.putExtra("IMAGE_INDEX", index);
						mActivity.startActivity(intent);
				    }
				});
				
				mSampleLayout.addView(sampleView);
				mSampleLayout.addView(spacerView);
				Log.d(TAG, "Drew new shared pic on Home screen!");
			}
		}
		mSampleLayout.invalidate();
	}

	@Override
	public void onClick(View v) {
		switch (v.getId()) {
			case R.id.button_instruction_id:
				Intent intent = new Intent(mActivity, HomeInstructionActivity.class);
				mActivity.startActivity(intent);
				break;
			default:
				// TODO:
				break;
		}	
	}
	
	private void getWindowingMetrics(){
		//I'm keeping seperate copies of this method throughout the app for now just because i want to 
		//return different values for heights and widths of things based on what I'm trying to render.
		//Will generalize and make such that this can live in a common location (ie: Utils.java) later on
		
		//Here, this returns the sizes for the images to be displayed as sample Smirks on the homescreen
		
		//This returns the widths and heights of the pictures to be rendered by the adapter
		DisplayMetrics dm = new DisplayMetrics(); 
		mActivity.getWindowManager().getDefaultDisplay().getMetrics(dm);
		
		if (dm.heightPixels < 650){
			//Lower resolution device, namely the Tab2 7.0
			mWidth = 200;
			mHeight = 175;}
			
		else if (dm.heightPixels > 650){
			//Higher resolution device, namely the Galaxy S3
			mWidth = 250;
			mHeight = 200;}
	}
}
