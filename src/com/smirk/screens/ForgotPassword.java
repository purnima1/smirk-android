package com.smirk.screens;

import android.content.Intent;
import android.app.Activity;
import android.app.AlertDialog;
import android.view.*;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.Toast;
import android.widget.EditText;
import android.os.Bundle;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.util.Log;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;

import com.smirk.R;
import com.smirk.network.SmirkApiCalls;

public class ForgotPassword extends Activity{
	
		private final static String TAG = "ForgotPassword: ";
		
		private EditText mEmailField, mQuestionOneField, mQuestionTwoField, mNewPasswordField;
		private ImageButton mSubmitEmail, mSubmitAnswers, mSubmitPassword;
		
		private String mEmail;
		
		private String questionOne;
		private String questionTwo;
		
		private String questionOneAnswer;
		private String questionTwoAnswer;
		
		private String userAnswerOne;
		private String userAnswerTwo;
		
		private BroadcastReceiver receiveAnswers = new BroadcastReceiver() {
			@Override
			public void onReceive(Context context, Intent intent) {
				Log.d(TAG, "Received answer broadcast in ForgotPassword");
				Bundle extras = intent.getExtras();
				if (extras != null) {
					String res = intent.getStringExtra("SECRET_QA_RESPONSE");  
					Log.d(TAG, " inside onReceive : " + res);
					if (res.contains("ANSWERS_NOT_FOUND")){
						//Display a Toast saying the answers couldn't be pulled
						Log.d(TAG, "Couldn't get the answers from the backend");
						Toast.makeText(getApplicationContext(), "Answers couldn't be retrived from the backend.", Toast.LENGTH_LONG).show();
					}
					else if (res.contains("OK")){
						//Set questionOneAnswer and questionTwoAnswer with the contents of the response
						Log.d(TAG, "Succesffully got the answers from the backend");
						questionOneAnswer = intent.getStringExtra("answerOne");
						questionTwoAnswer = intent.getStringExtra("answerTwo");
						promptForAnswers();
					}
				}
			}
		};
	
		@Override
		protected void onCreate(Bundle savedInstanceState) {
			super.onCreate(savedInstanceState);
			setContentView(R.layout.forgot_password);
			
			mEmailField = (EditText) findViewById(R.id.email_for_forgotpwd);
			mQuestionOneField = (EditText) findViewById(R.id.question_one_et);
			mQuestionTwoField = (EditText) findViewById(R.id.question_two_et);
			mNewPasswordField = (EditText) findViewById(R.id.new_password_field);
			
			mSubmitEmail = (ImageButton) findViewById(R.id.email_submit);
			mSubmitAnswers = (ImageButton) findViewById(R.id.answer_submit);
			mSubmitPassword = (ImageButton) findViewById(R.id.password_submit);
			
			mSubmitEmail.setOnClickListener(new View.OnClickListener() {
			    @Override
			    public void onClick(View v) {
			        submitEmail();
			    }
			});
			
			mSubmitAnswers.setOnClickListener(new View.OnClickListener() {
			    @Override
			    public void onClick(View v) {
			        submitAnswers();
			    }
			});
			
			mSubmitPassword.setOnClickListener(new View.OnClickListener() {
			    @Override
			    public void onClick(View v) {
			        resetPassword();
			    }
			});
		}
		
		@Override
		public void onResume(){
		    super.onResume();
			registerReceiver(receiveAnswers, new IntentFilter("SECRET_QA"));
		}
		
		@Override
		protected void onPause(){
			super.onPause();
			unregisterReceiver(receiveAnswers);
		}
		
		private void promptForAnswers(){
			mSubmitEmail.setVisibility(View.GONE);
			mEmailField.setVisibility(View.GONE);
			
			mQuestionOneField.setVisibility(View.VISIBLE);
			mQuestionTwoField.setVisibility(View.VISIBLE);
			
			mQuestionOneField.setHint("What is your favorite color?");
			mQuestionTwoField.setHint("In what city were you born?");
			
			mSubmitAnswers.setVisibility(View.VISIBLE);
		}
		
		private void submitAnswers(){
			userAnswerOne = mQuestionOneField.getText().toString();
			userAnswerTwo = mQuestionTwoField.getText().toString();
			if (userAnswerOne.equals("") || userAnswerTwo.equals("")){
				Toast.makeText(getApplicationContext(), "Must answer both questions to reset password", Toast.LENGTH_LONG).show();
			} else{
				//check that answers are equal, ignoring case
				if (userAnswerOne.equalsIgnoreCase(questionOneAnswer) && 
					userAnswerTwo.equalsIgnoreCase(questionTwoAnswer)){
						mQuestionOneField.setVisibility(View.GONE);
						mQuestionTwoField.setVisibility(View.GONE);
						mSubmitAnswers.setVisibility(View.GONE);
						
						mNewPasswordField.setVisibility(View.VISIBLE);
						mSubmitPassword.setVisibility(View.VISIBLE);
				}else{
					Toast.makeText(getApplicationContext(), "Answers given were incorrect.", Toast.LENGTH_LONG).show();
				}
			}
		}
		
		private void resetPassword(){
			String newPassword = mNewPasswordField.getText().toString();
			if (newPassword.equals("")){
				Toast.makeText(getApplicationContext(), "Enter a new password!", Toast.LENGTH_LONG).show();
			} else {
				Log.d(TAG, "Entered: "+mEmail+", "+newPassword);
				JSONObject passwordObject = new JSONObject();
				try{
					passwordObject.put("newpassword", newPassword);
				} catch (JSONException e){
					e.printStackTrace();
				}
				SmirkApiCalls.setNewPassword(getApplicationContext(), mEmail, passwordObject);
				finish();
			}
		}
		
		private void submitEmail(){
			mEmail = mEmailField.getText().toString();
			if (mEmail.equals("")){
				//User didn't enter anything for an email
				Toast.makeText(getApplicationContext(), "Enter your email to reset your password!", Toast.LENGTH_LONG).show();
			} else {
				SmirkApiCalls.getUserPwdRetrivalAnswers(ForgotPassword.this, mEmail);
			}	
		}
}