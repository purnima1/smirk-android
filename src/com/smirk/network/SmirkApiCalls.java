package com.smirk.network;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.UnsupportedEncodingException;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.InputStream;
import java.io.OutputStream;
import java.io.FileOutputStream;
import java.io.BufferedInputStream;
import java.io.StringWriter;
import java.io.FileNotFoundException;
import java.io.File;

import java.net.URLConnection;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.entity.mime.HttpMultipartMode;
import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.ResponseHandler;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.client.methods.HttpDelete;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.apache.http.util.EntityUtils;
import org.apache.http.entity.mime.content.StringBody;
import org.apache.http.entity.mime.content.FileBody;

import org.apache.commons.io.IOUtils;
//import org.apache.commons.codec.binary.Base64InputStream;

import java.util.HashMap;
import java.util.Map;
import java.util.List;
import java.util.ArrayList;
import java.io.DataOutputStream;
import java.net.HttpURLConnection;
import java.net.URL;

import org.json.JSONException;
import org.json.JSONObject;
import org.json.JSONArray;
import org.json.JSONTokener;

import android.os.AsyncTask;
import android.util.Log;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.BitmapDrawable;
import android.graphics.Canvas;
import android.graphics.Bitmap.CompressFormat;
import android.graphics.drawable.Drawable;
import android.widget.Toast;
import android.util.Base64;
import android.util.Log;
import android.app.ProgressDialog;

import com.android.volley.AuthFailureError;
import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.*;

import com.smirk.commom.CommonData;
import com.smirk.commom.VolleySingleton;
import com.smirk.commom.FriendNotification;
import com.smirk.commom.Friend;
import com.smirk.commom.Utils;
import com.smirk.commom.Constants;
import com.smirk.commom.SmirkImage;
import com.smirk.commom.SmirkResource;
import com.smirk.screens.LoginScreen;
import com.smirk.SmirkMainActivity;

import com.smirk.R;

public class SmirkApiCalls{
	private static final String TAG = "SmirkApiCalls";
	
	private static final int SOCKET_TIMEOUT_MS = 5000;
	private static final int DEFAULT_MAX_RETRIES = 3;
	private static final int DEFAULT_BACKOFF_MULT = 3;
	
	private static Bitmap friendProfilePic;
	private static List<FriendNotification> notificationsList;
	
	//Below function called only when we're already signed in with initialized info in CommonData, so that we can refresh the shared pics

	public static void refreshSignIn(final Context context){
		Log.d(TAG, "Before calling the refresh sign-in route!");
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		
		String url = Constants.LOGIN_URL;
		
		JSONObject refreshLogin = new JSONObject();
		
		try{
			refreshLogin.put("email", CommonData.email);
			refreshLogin.put("password", CommonData.password);
		} catch(JSONException e){
			e.printStackTrace();
		}
		
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, refreshLogin, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {					
						Log.d(TAG, "Response for refresh login: "+ response.toString());
						try {
							if (Integer.parseInt(response.getString("code")) == 0){	
								Log.d(TAG, "Refresh login successful");
								JSONArray sharedPics = response.getJSONArray("sharedPicsURLs");
								if (CommonData.sharedPicsCount != sharedPics.length()){
									CommonData.userSharedPictures.clear();
									CommonData.sharedPicsCount = sharedPics.length();
									getUserSharedPics(sharedPics);
								}
							}
							else{
								Log.d(TAG, "Refresh login failed");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
						@Override
						public void onErrorResponse(VolleyError error) {
							Log.d(TAG, "Volley Error; probably network connectivity-related." + error.getStackTrace());
							Toast.makeText(context, "Poor internet connectivity -- try again later.", Toast.LENGTH_LONG).show();
						}
			});
		queue.add(request);
	}

	public static void signLogInJsonObjectRequestQueue(final Context context, String url, final String sessionId, final JSONObject jObject,
	final boolean facebookLogin) {
		Log.d(TAG, "before getting the queue from volley singleton");
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		Log.d(TAG, "before signInJSON request" + " url = " + url);
		
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, jObject, new Response.Listener<JSONObject>() {
					
					@Override
					public void onResponse(JSONObject response) {
						boolean broadcastSent = false;
						Log.d(TAG, "got the response" + response);

						if (response.has("code")) {
							int code = 0;
							try {
								code = response.getInt("code");
								Log.d(TAG,"Response Code is : "+ response.getString("code"));
								Log.d(TAG,"Response Message is : " + response.getString("message"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}

							if (code == 0) { // signin successful
								try {									
									String password = jObject.getString("password");
									
									CommonData.setSessionId(response.getString("sessionId"));
									
									//User is a JSON object embedded within response
									JSONObject user = response.getJSONObject("user");
									CommonData.setUserEmail(user.getString("email"));
									CommonData.setUserName(user.getString("screenName"));
									CommonData.setPoints(user.getString("points"));
									CommonData.setSkinTone(user.getString("skinTone"));
									
									Intent homescreen = new Intent();
									homescreen.setAction("HOMESCREEN");
									context.sendBroadcast(homescreen);
									
									if (!facebookLogin){
										String profilePictureURL = user.getString("profilePictureUrl");
										profilePictureURL = profilePictureURL.replaceAll("\\\\", "");
										CommonData.setProfileImageDownloadUrl(profilePictureURL);
										downloadProfileImage(CommonData.profileImageDownloadUrl, true, 0, null, 0, context);
									} else{
										String profilePictureURL = user.getString("profilePicture");
										profilePictureURL = profilePictureURL.replaceAll("\\\\", "");
										CommonData.setProfileImageDownloadUrl(profilePictureURL);
										Log.d(TAG, "User was FB user with profile pic URL: "+profilePictureURL);
										downloadProfileImage(profilePictureURL, true, 0, null, 0, context);
									}
									
									Log.d(TAG, " Session Id = " + CommonData.sessionId);
									Log.d(TAG, " Username = " + CommonData.userName);
									Log.d(TAG, " Points = " + CommonData.points);
									Log.d(TAG, " Profile Picture URL = "+CommonData.profileImageDownloadUrl);
									Log.d(TAG, " Skintone = " +CommonData.skinTone);
									Log.d(TAG, " Email = "+CommonData.email);
									
									CommonData.setUserPassword(password);
									Log.d(TAG, "Password in CommonData was set to: "+CommonData.password);
									
									//getUserSmirkedPics();
									getSmirkResources(context, CommonData.sessionId);
									
									if (facebookLogin && !broadcastSent){
										Intent intent = new Intent();
										intent.setAction("LOGIN");
										intent.putExtra("AUTH_OKAY", "OK");
										Log.d(TAG, "Sending broadcast indicating successful Facebook login");
										context.sendBroadcast(intent);
										broadcastSent = true;
									}
									else if (!facebookLogin && !broadcastSent){
										Intent intent = new Intent();
										intent.setAction("LOGIN");
										intent.putExtra("AUTH_OKAY", "OK");
										Log.d(TAG, "Sending broadcast indicating successful login");
										context.sendBroadcast(intent);
										broadcastSent = true;
									}

								} catch (JSONException e) {
									// TODO Auto-generated catch block
									e.printStackTrace();
								}
								
							}
							else if (code == 1000){ //invalid username or password
								
								if (facebookLogin && !broadcastSent){
									Intent intent = new Intent();
									intent.setAction("LOGIN");
									intent.putExtra("AUTH_OKAY", "FIRSTTIMEFB");
									Log.d(TAG, "Sending broadcast indicating unsuccessful Facebook login");
									context.sendBroadcast(intent);
									broadcastSent = true;
								}
								else{
									Toast.makeText(context, "Username and password combination not found", Toast.LENGTH_LONG).show();
								}
							}
							else if (code == 1002){ //email was taken
								Toast.makeText(context, "Email already registered with Smirk.", Toast.LENGTH_LONG).show();
							}
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.d(TAG, "Volley Error; probably network connectivity-related." + error.getStackTrace());
						Toast.makeText(context, "Poor internet connectivity -- try again later.", Toast.LENGTH_LONG).show();
					}
				});
		queue.add(request);
	}
	
	public static void getUserSharedPics(JSONArray sharedPicsArray){
		Log.d(TAG, "Iterating through shared pictures URLs to download, add to list");
		String url;
		try{
			for (int i = 0; i<sharedPicsArray.length(); i++){
				url = sharedPicsArray.get(i).toString();
				Log.d(TAG, "URL: "+url);
				if (!url.contains("http://")){ url = "http://"+url; }
				if (!url.equals("null")){
					//check to make sure url is not already a member of one of the 
					//fields we have in CommonData.userSharedPictures
					if (sharedImageUnique(url)){
						downloadSmirkImage(url, true);
					}
				}
			}
		} catch (JSONException e){
			e.printStackTrace();
		}
	}
	
	public static boolean sharedImageUnique(String url){
		if (CommonData.userSmirkedPictures.size() != 0){
			for (SmirkImage image : CommonData.userSmirkedPictures){
				if (image.getUrl().equals(url)){ 
					Log.d(TAG, "Found a duplicate URL");
					return false; }
			}
		}
		return true;
	}
	
	public static void setSecretAnswers(final Context context, final JSONObject answers){
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
				Constants.SET_SECRET_ANSWERS, answers, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {					
						Log.d(TAG, "Response obj string is: "+ response.toString());
						try {
							if (Integer.parseInt(response.getString("code")) == 0){
								Log.d(TAG, "Secret question set successful");
								Toast.makeText(context, "Secret questions set successfully", Toast.LENGTH_LONG).show();
							}	
							else{
								Toast.makeText(context, "Error setting secret questions", Toast.LENGTH_LONG).show();
								Log.d(TAG, "Answers were supplied as: "+answers.toString());
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error setting secret questions: " + error.getStackTrace());
						Toast.makeText(context, "Server error encountered setting secret questions", Toast.LENGTH_LONG).show();
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("Content-Type", "application/json");
				m.put("sessionid", CommonData.sessionId);
				Log.d(TAG, "Session ID: "+CommonData.sessionId);
				return m;
			}
		};
		queue.add(request);
	}
	
	public static void getSmirkResources(final Context context, final String sessionId){
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		String url = Constants.DOWNLOAD_RESOURCES;
		Log.d(TAG, "Before downloading all the Smirk resources");
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						//Log.d(TAG,"Response for Smirk download: "+response.toString());					
						try {
							String message = response.getString("message");
							if (message.equalsIgnoreCase("Operation Success")){
								JSONArray resources = response.getJSONArray("smirkExpressions");
								
								if (CommonData.resourceCount != resources.length()){
									CommonData.resourceCount = resources.length();
								
									for (int i = 0; i<resources.length(); i++){
										String imageListString = resources.get(i).toString();
										JSONObject imageListObject = new JSONObject(imageListString);
										JSONArray smirkImageList = imageListObject.getJSONArray("smirkImageList");
							
										for (int j = 0; j<smirkImageList.length(); j++){
											String resourceString = smirkImageList.get(j).toString();
					
											JSONObject resourceObject = new JSONObject(resourceString);
											String thumbUrl = resourceObject.getString("thumbnail");
											thumbUrl = thumbUrl.replaceAll("\\\\", "");
											String gifUrl = resourceObject.getString("original");
											gifUrl = gifUrl.replaceAll("\\\\", "");
										
											int id = Integer.parseInt(resourceObject.getString("id"));
										
											int expressionType;
											if (thumbUrl.contains("eye")){
												expressionType = Utils.EYE_SMIRK;
												downloadSmirkThumbAndAdd(id, expressionType, thumbUrl, gifUrl);
											} else if (thumbUrl.contains("mouth")){
												expressionType = Utils.MOUTH_SMIRK;
												downloadSmirkThumbAndAdd(id, expressionType, thumbUrl, gifUrl);
											} else if (thumbUrl.contains("word")){
												expressionType = Utils.WORD_SMIRK;
												downloadSmirkThumbAndAdd(id, expressionType, thumbUrl, gifUrl);
											}
										}
									}
								}
							}
							else{
								Log.d(TAG, "Couldn't download the Smirk resources!");
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error downloading Smirks with Volley Error: " + error.getStackTrace());
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("sessionid", sessionId);
				return m;
			}

		};
		queue.add(request);
	}
	
	public static void getJsonObjectRequestQueue(final Context context,
			String url, final String sessionId, JSONObject jObject) {

		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		Log.d(TAG, "before getJSON request" + " url = " + url+ " sessionid = " + sessionId);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						try {
							//Will return 1 with code if no matching users found
							int code = Integer.parseInt(response.getString("code"));
							
							if (code == 0){ //success
								Intent intent = new Intent();
								intent.setAction("UsersGet");
								intent.putExtra("RESULT", response.toString());
								Log.d(TAG, "sending the broadcast");
								context.sendBroadcast(intent);
							}
							else if (code == 1){ //failure
								Intent intent = new Intent();
								intent.setAction("UsersGet");
								intent.putExtra("RESULT", "NOUSERS");
								Log.d(TAG, "sending the broadcast");
								context.sendBroadcast(intent);
							}
					

							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "got error = " + error.getStackTrace());

					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("sessionid", sessionId);
				return m;
			}

		};
		queue.add(request);
		
	} // getJsonObjectRequestQueue
	
	
	public static void getFriendsJsonObjectRequestQueue(final Context context,
			String url, final String sessionId, JSONObject jObject) {
		final List<Friend> existingFriends = new ArrayList<Friend>();
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		Log.d(TAG, "before getFriendsJSON request" + " url = " + url+ " sessionid = " + sessionId);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						Log.d(TAG, "getFriends - got the response - resultObject: ");
						Log.d(TAG, "Response obj string is: "+ response.toString());
						try {
							if (Integer.parseInt(response.getString("code")) == 0){
								JSONArray userArray = response.getJSONArray("userList");
								
								int friendCount = userArray.length();

								if (Friend.countHolder != friendCount){

									Friend.countHolder = friendCount;

									for (int i = 0; i < userArray.length(); i++) {
										JSONObject user = userArray.getJSONObject(i);
										String userName = user.getString("screenName");
										String profilePictureURL = user.getString("profilePicture");
										profilePictureURL = profilePictureURL.replaceAll("\\\\", "");
										int id = Integer.parseInt(user.getString("id"));
										Friend friend = new Friend(userName, profilePictureURL, id, null);
										Log.d(TAG, "Added friend: "+userName+"; "+id);
										existingFriends.add(friend);
										}
									
										Friend.setList(existingFriends);
									
										Intent intent = new Intent();
										intent.setAction("FRIEND_SCREEN_BROADCAST");
										intent.putExtra("FRIEND_BROADCAST", "NEW_EXISTING_FRIENDS");
										context.sendBroadcast(intent);
									
									} else{
										Intent intent = new Intent();
										intent.setAction("FRIEND_SCREEN_BROADCAST");
										intent.putExtra("FRIEND_BROADCAST", "NO_NEW_EXISTING_FRIENDS");
										context.sendBroadcast(intent);
									}
							}
							
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error pulling existing friends: " + error.getStackTrace());
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("sessionid", sessionId);
				return m;
			}
		};
		queue.add(request);
		
	} // getFriendsJsonObjectRequestQueue
	
	public static void imageShare(final Context context, final String imageServerPath) {
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		String url = Constants.SHARE_SMIRKS;
		Log.d(TAG, "Before sharing the image at: "+imageServerPath);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						try {
							int code = Integer.parseInt(response.getString("code"));
							if (code == 0){
								Log.d(TAG, "Successfully shared Smirk image");
							}
							else{
								Log.d(TAG, "Error occured with response code: "+code);
								Log.d(TAG, "Response: "+response.toString());
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error sharing Smirked picture: " + error.getStackTrace());
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("Content-Type", "application/json");
				m.put("sessionid", CommonData.sessionId);
				m.put("imageString", imageServerPath);
				return m;
			}
		};
		queue.add(request);
		
	}
	
	public static void inviteFriendJsonObjectRequestQueue(final Context context,
			String url, final String sessionId, JSONObject jObject) {

		RequestQueue queue = VolleySingleton.getInstance(context)
				.getRequestQueue();
		Log.d(TAG, "before inviteFriend request" + " url = " + url+ " sessionid = " + sessionId);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						Log.d(TAG, "inviteFriend - got the response - resultObject: ");
						Log.d(TAG,
								"Response obj string is: "
										+ response.toString());
						
						try {
							
				// code = 0 means invitation sent
				if (response.has("code")) {
						Intent intent = new Intent();
						intent.setAction("InviteFriend");
						intent.putExtra("RESULT", response.toString());
						//intent.putExtra("RESULT", response.getString("code"));
						Log.d(TAG, "sending the broadcast");
						context.sendBroadcast(intent);
				}   // end if	
				
							Log.d(TAG,
									"Response Code is : "
											+ response.getString("code") + " int val : " + response.getInt("code"));
							Log.d(TAG,
									"Response Message is : "
											+ response.getString("message"));
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
						
						
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "got error = " + error.getStackTrace());

					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("sessionid", sessionId);
				m.put("Content-Type", "application/json");
				return m;
			}

		};
		queue.add(request);
		
	} // inviteFriendJsonObjectRequestQueue
	
	
	public static void notificationsGetJsonObjectRequestQueue(final Context context, String url, final String sessionId, JSONObject jObject) {
		
		notificationsList = new ArrayList<FriendNotification>();
		
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		Log.d(TAG, "before notificationsGetJSON request" + " url = " + url + " sessionid = " + sessionId);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						Log.d(TAG, "notifications - got the response - resultObject: ");
						Log.d(TAG, "Response obj string is: " + response.toString());
						try {
							Log.d(TAG, "Response Code for FriendRequests is : " + response.getString("code"));
							Log.d(TAG, "Response Code for FriendRequests is : " + response.getString("message"));
							
							int code = Integer.parseInt(response.getString("code"));
											
							if (code == 0){
							//If response successfully received	
								JSONArray notificationsJSON = response.getJSONArray("notificationList");
								int numberNotifications = notificationsJSON.length();
								
								if (numberNotifications == 0){
									Log.d(TAG, "No notifications found in JSON response");
								}
								
								//iterate through array of notifications and add to 
								if (FriendNotification.countHolder != numberNotifications){
									FriendNotification.countHolder = numberNotifications;
									for (int i = 0; i < numberNotifications; i++) {
									    JSONObject notification = notificationsJSON.getJSONObject(i);
								
									    int notificationId = notification.getInt("id");
										Log.d(TAG, "Notification ID: "+notificationId);
								
										JSONObject sender = notification.getJSONObject("sender");
								
										String senderUserName = sender.getString("screenName");
										String senderProfilePicURL = sender.getString("profilePicture");
										senderProfilePicURL = "http://23.253.166.61"+senderProfilePicURL;
										senderProfilePicURL = senderProfilePicURL.replaceAll("\\\\", "");
										int id = Integer.parseInt(sender.getString("id"));
										Log.d(TAG, "Sender username and picture URL: "+senderUserName+" ; "+senderProfilePicURL+"; "+id);

										downloadProfileImage(senderProfilePicURL, false, notificationId, senderUserName, id, context);	
									}
														
									Intent intent = new Intent();
									intent.setAction("FRIEND_SCREEN_BROADCAST");
									intent.putExtra("FRIEND_BROADCAST", "NEW_PENDING_FRIENDS");
									context.sendBroadcast(intent);
									
								} else{
									Intent intent = new Intent();
									intent.setAction("FRIEND_SCREEN_BROADCAST");
									intent.putExtra("FRIEND_BROADCAST", "NO_NEW");
									context.sendBroadcast(intent);
								}
							}				
											
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "got error = " + error.getStackTrace());

					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("sessionid", sessionId);
				return m;
			}
		};
		queue.add(request);
	} // notificationsGetJsonObjectRequestQueue
	
	public static void inviteResponse(final Context context, final String url, final String userName) {
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		Log.d(TAG, "before inviteResponseJSON request" + " url = " + url+ " sessionid = " + CommonData.sessionId);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						Log.d(TAG,"Invite Response obj string is: "+ response.toString());	
						try {
							if (response.getInt("code") == 0){
								if (url.contains("accept_friend_request")){
									//If we were approving a friend request, we'll remove the user from pending list
									//and add them to the existing list (this happens on front end already back in 
									//FriendRequestResponse fragment
									FriendNotification.removeMember(userName);
									Toast.makeText(context, userName+" successfully added to Friends list.", Toast.LENGTH_LONG).show();
								}
								else if (url.contains("decline_friend_request")){
									//Remove from notification list
									FriendNotification.removeMember(userName);
									Toast.makeText(context, userName+"'s invitation declined.", Toast.LENGTH_LONG).show();
								}
							}
							else{
								Log.d(TAG, "Invite response failed with backend message: "+response.toString());
							}	
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error responding to friend request " + error.getStackTrace());
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("sessionid", CommonData.sessionId);
				m.put("Content-Type", "application/json");
				return m;
			}
		};
		queue.add(request);
		
	} 
	
	public static void removeFriendJsonObjectRequestQueue(final Context context, String url, final String sessionId, JSONObject jObject) {
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		Log.d(TAG, "before removeFriendJSON request" + " url = " + url+ " sessionid = " + sessionId);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
				url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						Log.d(TAG, "removeFriend - got the response - resultObject: ");
						Log.d(TAG, "Response obj string is: " + response.toString());
						try {
							Log.d(TAG,"Response Code is : "+ response.getString("code") + " int val : " + response.getInt("code"));
							Log.d(TAG,"Response Message is : "+ response.getString("message"));
							if (response.getString("code").equals("0")){
								Toast.makeText(context, "Friend was deleted!", Toast.LENGTH_LONG).show();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "got error = " + error.getStackTrace());

					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("sessionid", sessionId);
				m.put("Content-Type", "application/json");
				return m;
			}

		};
		queue.add(request);
		
	} // removeFriendJsonObjectRequestQueue
	
	public static void getUserSmirkedPics(){
		Log.d(TAG, "Call before getting user's smirked pics");
		final HttpClient httpclient = new DefaultHttpClient();
		final HttpGet httpGet = new HttpGet(Constants.GET_SMIRKED_PICS_URL);
		
		httpGet.setHeader("Content-type", "application/json");
		httpGet.setHeader("sessionid", CommonData.sessionId);
		
		new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(final Void ... params) {
				try{
					HttpResponse response = httpclient.execute(httpGet);
					JSONObject jsonResponse = parseJSONfromHttpResponse(response);
					Log.d(TAG, "jsonResponse for smirked image URLs download: "+jsonResponse.toString());
					if (jsonResponse.has("code")){
						int code = Integer.parseInt(jsonResponse.get("code").toString());;
						//Image upload was successful
						if (code == 0){ 
							Log.d(TAG, "Images retrieved successfully");
							Log.d(TAG, "Response: "+jsonResponse.toString());
							JSONArray imageURLs = jsonResponse.getJSONArray("smirkedImages");
							
							if (CommonData.smirkedPicsCount != imageURLs.length()){
								CommonData.smirkedPicsCount = imageURLs.length();
								for (int i = 0; i<imageURLs.length(); i++){
									String imageURL = imageURLs.get(i).toString();
									imageURL = imageURL.replaceAll("\\\\", "");
									imageURL = "http://"+imageURL;
									Log.d(TAG, "imageURL: "+imageURL);
									downloadSmirkImage(imageURL, false);
								}
							}
						}
						else if (code != 0){ Log.d(TAG, "Image retrieve failed!");}
					}
					else{
						Log.d(TAG, "Attempt to download images was unsuccessful");
					}
				} catch (Exception e){
					e.printStackTrace();
				}
				return null;
			}
		}.execute();
	}
	
	public static File convertBitmapToFile(Bitmap image, Context context){
		//create a file to write bitmap data
		File f = null;
		try{
			f = new File(context.getCacheDir(), "SmirkImage");
			f.createNewFile();

			//Convert bitmap to byte array
			Bitmap bitmap = image;
			ByteArrayOutputStream bos = new ByteArrayOutputStream();
			bitmap.compress(CompressFormat.PNG, 50, bos);
			byte[] bitmapdata = bos.toByteArray();

			//write the bytes in file
			FileOutputStream fos = new FileOutputStream(f);
			fos.write(bitmapdata);
		} catch (IOException e){
			e.printStackTrace();
		}
		return f;
	}
	
	public static void smirkImageUploadHTTPClient(final Bitmap image, final int points, final Context context){
		Log.d(TAG, "Call before preparing and executing HTTP Post, bitmap image");
		final HttpClient httpclient = new DefaultHttpClient();
		final HttpPost httpPost = new HttpPost(Constants.SMIRKED_PICS_URL);
		String boundary = "-------------" + System.currentTimeMillis();
		
		httpPost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);
		httpPost.setHeader("sessionid", CommonData.sessionId);
		httpPost.setHeader("points", String.valueOf(points));
		
		File bitmapFile = convertBitmapToFile(image, context);
		
		if (bitmapFile == null){
			Log.d(TAG, "Bitmap converted incorrectly to File");
		}
		
		FileBody imageBody = new FileBody(bitmapFile, ContentType.MULTIPART_FORM_DATA);
		HttpEntity entity = MultipartEntityBuilder.create()
                .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                .setBoundary(boundary)
				.addPart("fileUpload", imageBody)
                .build();
        httpPost.setEntity(entity);

		new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(final Void ... params) {
				try{
					HttpResponse response = httpclient.execute(httpPost);
					JSONObject jsonResponse = parseJSONfromHttpResponse(response);
					Log.d(TAG, "jsonResponse for smirked image upload: "+jsonResponse.toString());
					if (jsonResponse.has("code")){
						int code = Integer.parseInt(jsonResponse.get("code").toString());
						//Image upload was successful
						if (code == 0){ 
							Log.d(TAG, "Image upload success!");
							String imagePath = jsonResponse.getString("extra");
							Intent intent = new Intent();
							intent.setAction("UPLOAD_PICTURE");
							intent.putExtra("url", imagePath);
							context.sendBroadcast(intent);
						}
						else if (code != 0){ Log.d(TAG, "Image upload failed!");}
					}
					else{
						Log.d(TAG, "Attempt to upload image was unsuccessful");
					}
				} catch (Exception e){
					e.printStackTrace();
				}
				return null;
			}
		}.execute();
		
	}
			
	public static void profileCreateHTTPClient(final Bitmap profilePic, final JSONObject newUser, final Context context){
		 final HttpClient httpclient = new DefaultHttpClient();
         final HttpPost httpPost = new HttpPost("http://23.253.166.61:8080/smirkservices/api/user/profile/image");

         String boundary = "-------------" + System.currentTimeMillis();

         httpPost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);

		 File bitmapFile = convertBitmapToFile(profilePic, context);

		 FileBody imageBody = new FileBody(bitmapFile, ContentType.MULTIPART_FORM_DATA);

         HttpEntity entity = MultipartEntityBuilder.create()
                 .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                 .setBoundary(boundary)
				 .addPart("fileUpload", imageBody)
                 .build();

         httpPost.setEntity(entity);

		new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(final Void ... params) {
				try{
					HttpResponse response = httpclient.execute(httpPost);
					JSONObject jsonResponse = parseJSONfromHttpResponse(response);
					Log.d(TAG, "jsonResponse for profile image upload: "+jsonResponse.toString());
					if (jsonResponse.has("code")){
						int code = Integer.parseInt(jsonResponse.get("code").toString());;
						//Image upload was successful
						if (code == 0){
							String location = jsonResponse.get("extra").toString();
							location = location.replaceAll("\\\\", ""); //replacing all backslashes with ""
							CommonData.setProfileImageServerLocation(location);
							Log.d(TAG, "profileImage location: "+location);
						}
					}
					else{
						Log.d(TAG, "Attempt to upload profile image was unsuccessful");
					}
				} catch (Exception e){
					e.printStackTrace();
				}
				return null;
			}
			@Override
			protected void onPostExecute(final Void result) {
				 try{
		         	newUser.put("profilePicture", CommonData.profileImageServerLocation);
				 } catch (JSONException e){
					e.printStackTrace();
				 }
				signLogInJsonObjectRequestQueue(context, Constants.SIGNUP_URL, null, newUser, false);
		     }
			
		}.execute();	
	}
	
	public static void deleteSmirkImage(final Context context, final String imageUrl){
		Log.d(TAG, "Before calling to delete Smirk image: "+imageUrl);
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		
		JSONObject deleteObject = new JSONObject();
		try{
			deleteObject.put("imageString", imageUrl);
			Log.d(TAG, "Put image url: "+imageUrl);
		} catch (JSONException e){
			e.printStackTrace();
		}
		
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
				Constants.DELETE_SMIRK, deleteObject, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, "Received response for delete Smirk: " + response);
						if (response.has("code")) {
							int code = 0;
							try {
								code = response.getInt("code");
								Log.d(TAG,"Response Code is : "+ response.getString("code"));
								Log.d(TAG,"Response Message is : "+ response.getString("message"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (code == 0) { //Updated successful
								Toast.makeText(context, "Image successfully deleted", Toast.LENGTH_LONG).show();
							}
							else if (code == 1){
								Toast.makeText(context, "Failure in deleting image", Toast.LENGTH_LONG).show();
							}
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.d(TAG, "Error deleting Smirk: " + error.getStackTrace());
					}
				}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> m = new HashMap<String, String>();
					m.put("Content-Type", "application/json");
					m.put("sessionid", CommonData.sessionId);
					return m;
				}		
			};
			queue.add(request);
	}
	
	public static void updateProfileVolley(final JSONObject updatedUser, final Context context){
		Log.d(TAG, "Before calling for the queue to make profile updates");
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST,
				Constants.UPDATE_PROFILE_URL, updatedUser, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {
						Log.d(TAG, "Received response for update profile: " + response);
						if (response.has("code")) {
							int code = 0;
							try {
								code = response.getInt("code");
								Log.d(TAG,"Response Code is : "+ response.getString("code"));
								Log.d(TAG,"Response Message is : "+ response.getString("message"));
							} catch (JSONException e) {
								// TODO Auto-generated catch block
								e.printStackTrace();
							}
							if (code == 0) { //Updated successful
								Toast.makeText(context, "Profile update successful!", Toast.LENGTH_LONG).show();
							}
							else if (code == 1000){
								Toast.makeText(context, "Profile update could not be completed.", Toast.LENGTH_LONG).show();
							}
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						Log.d(TAG, "Error updating profile: " + error.getStackTrace());
					}
				}){
				@Override
				public Map<String, String> getHeaders() throws AuthFailureError {
					Map<String, String> m = new HashMap<String, String>();
					m.put("Content-Type", "application/json");
					m.put("sessionid", CommonData.sessionId);
					return m;
				}		
			};
			queue.add(request);
	}
	
	public static void updateProfileHTTP(final Bitmap image, final JSONObject updatedUser, final Context context){
		 final HttpClient httpclient = new DefaultHttpClient();
		
		 //We're calling this post here to get the new profile image location, and then calling updateProfileVolley 
         final HttpPost httpPost = new HttpPost("http://23.253.166.61:8080/smirkservices/api/user/profile/image");

         String boundary = "-------------" + System.currentTimeMillis();

         httpPost.setHeader("Content-type", "multipart/form-data; boundary="+boundary);

		 File bitmapFile = convertBitmapToFile(image, context);

		 FileBody imageBody = new FileBody(bitmapFile, ContentType.MULTIPART_FORM_DATA);

         HttpEntity entity = MultipartEntityBuilder.create()
                 .setMode(HttpMultipartMode.BROWSER_COMPATIBLE)
                 .setBoundary(boundary)
				 .addPart("fileUpload", imageBody)
                 .build();

         httpPost.setEntity(entity);

		new AsyncTask<Void, Void, Void>(){
            @Override
            protected Void doInBackground(final Void ... params) {
				try{
					HttpResponse response = httpclient.execute(httpPost);
					JSONObject jsonResponse = parseJSONfromHttpResponse(response);
					Log.d(TAG, "jsonResponse for profile image upload: "+jsonResponse.toString());
					if (jsonResponse.has("code")){
						int code = Integer.parseInt(jsonResponse.get("code").toString());;
						//Image upload was successful
						if (code == 0){
							String location = jsonResponse.get("extra").toString();
							location = location.replaceAll("\\\\", ""); //replacing all backslashes with ""
							CommonData.setProfileImageServerLocation(location);
							Log.d(TAG, "profileImage location: "+location);
						}
					}
					else{
						Log.d(TAG, "Attempt to upload profile image was unsuccessful");
					}
				} catch (Exception e){
					e.printStackTrace();
				}
				return null;
			}
			@Override
			protected void onPostExecute(final Void result) {
				 try{
		         	updatedUser.put("profilePicture", CommonData.profileImageServerLocation);
				 } catch (JSONException e){
					e.printStackTrace();
				 }
				updateProfileVolley(updatedUser, context);
		     }
			
		}.execute();	
	}
	
	public static JSONObject parseJSONfromHttpResponse(HttpResponse httpResponse){
		JSONObject finalResult = null;
		try{
			BufferedReader reader = new BufferedReader(new InputStreamReader(httpResponse.getEntity().getContent(), "UTF-8"));
			StringBuilder builder = new StringBuilder();
			for (String line = null; (line = reader.readLine()) != null;) {
			    builder.append(line);
			}	
			finalResult = new JSONObject(builder.toString());
		} catch (IOException e){
			e.printStackTrace();
		} catch (JSONException e){
			e.printStackTrace();
		}
		return finalResult;
	}
	
	public static void downloadSmirkThumbAndAdd(final int id, final int expressionType, final String thumbUrl,
	final String gifUrl){
		
		final float width = 150f;
		final float height = 100f; //dip size for thumbnail gridview
		
		new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(final Void ... params) {
				Bitmap image = null;
		        try {
					String urlString = thumbUrl;
					if (!urlString.contains("http://")){
						urlString = "http://"+thumbUrl;
					}
					Log.d(TAG, "Before downloading the thumbnail at: "+thumbUrl);
					URL url = new URL(urlString);
					URLConnection urlConnection = url.openConnection();
					InputStream in = new BufferedInputStream(urlConnection.getInputStream());

					image = BitmapFactory.decodeStream(in);
					
				} catch (Exception e) {e.printStackTrace();}
				return image;
			}
			@Override
			protected void onPostExecute(Bitmap result) {
		        if (result == null){Log.d(TAG, "Bitmap result was null");}
				else{
					//resize image to dimensions for thumbnail, make smirk resource obj,
					//and add to list
					result = Utils.scaleBitmap(result, width, height);
					SmirkResource newResource = new SmirkResource(id, expressionType, thumbUrl,
					gifUrl, result, null);
					SmirkResource.addSmirkToList(newResource);
				}
		    }
		}.execute();
	}
	
	public static void downloadSmirkGIF(final int expressionType, final int id, final String gifUrl, final Context context, final boolean launchPrev){
		new AsyncTask<Void, Void, byte[]>(){
			ProgressDialog pdia;
			
			@Override
		    protected void onPreExecute() {
				pdia = new ProgressDialog(context);
				pdia.setMessage("Loading Animation -- please wait!");
		        pdia.show();
		    }
			
            @Override
            protected byte[] doInBackground(final Void ... params) {
				byte[] gifAsByteArr = null;
		        try {
					String urlString = gifUrl;
					if (!urlString.contains("http://")){
						urlString = "http://"+gifUrl;
					}
					Log.d(TAG, "Before downloading the gif at: "+gifUrl);
					URL url = new URL(urlString);
					URLConnection urlConnection = url.openConnection();
					InputStream in = new BufferedInputStream(urlConnection.getInputStream());
					gifAsByteArr = IOUtils.toByteArray(in);
					
				} catch (Exception e) {e.printStackTrace();}
				return gifAsByteArr;
			}
			
			
			@Override
			protected void onPostExecute(byte[] gifAsByteArr) {
				pdia.dismiss();
				
		        if (gifAsByteArr == null){
					Log.d(TAG, "Byte array encoding of GIF was null!");
					//Send broadcast to SmirkResource with "FAILED" message on Bundle
					Intent intent = new Intent();
					intent.setAction("DOWNLOAD_GIF");
					intent.putExtra("GIF_DOWNLOADED", "FAILED");
					Log.d(TAG, "Broadcast indicating failed download of Smirk GIF.");
					context.sendBroadcast(intent);
				}
				else{
					//Send broadcast to SmirkResource with Bundle containing expressionType and id (for locating item on appropriate list),
					//and the GIF as a byte[]
					Intent intent = new Intent();
					intent.setAction("DOWNLOAD_GIF");
					intent.putExtra("GIF_DOWNLOADED", "OK");
					intent.putExtra("EXP_TYPE", expressionType);
					intent.putExtra("ID", id);
					//intent.putExtra("BYTE_ARRAY_GIF", gifAsByteArr);
					SmirkResource.gifByteArrHolder = gifAsByteArr;
					intent.putExtra("LAUNCH_PREV", launchPrev);
					Log.d(TAG, "Broadcast indicating ok download of Smirk GIF.");
					context.sendBroadcast(intent);
				}
		    }
		}.execute();
	}
	
	
	public static void downloadSmirkImage(final String imageURL, final boolean sharedOrPrivate){
		//sharedOrPrivate: true means it's a shared image, so the image downloaded will be added
		//to the user's shared picture list that displays on the home screen. false means it's
		//an image which may or may not be private -- those get added to the user's own smirk pic list
		new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(final Void ... params) {
				Bitmap image = null;
		        try {
					String urlString = imageURL;
					Log.d(TAG, "Before downloading the image at: "+imageURL);
					//InputStream in = new java.net.URL(imageURL).openStream();
					if (!urlString.contains("http://")){
						urlString = "http://"+imageURL;
					}
					URL url = new URL(urlString);
					URLConnection urlConnection = url.openConnection();
					InputStream in = new BufferedInputStream(urlConnection.getInputStream());

					image = BitmapFactory.decodeStream(in);
					//image = BitmapFactory.decodeByteArray(decodedString, 0, decodedString.length);
					
				} catch (Exception e) {e.printStackTrace();}
				return image;
			}
			@Override
			protected void onPostExecute(Bitmap result) {
		        if (result == null){Log.d(TAG, "Bitmap result was null");}
				else{
					SmirkImage smirkImage = new SmirkImage(result, 0, imageURL);
					if (!sharedOrPrivate){
						CommonData.addUserSmirkPicToList(smirkImage);
					} else{
						CommonData.addSharedPicToList(smirkImage);
					}
				}
		    }
		}.execute();
	}

	public static void downloadProfileImage(final String imageURL, final boolean isUserOrFriend, final int notificationId, 
	final String senderUserName, final int id, final Context context){
		//If iUserOrFriend = true, it's the user's own profile picture. If false, it's a friend's picture.
		new AsyncTask<Void, Void, Bitmap>(){
            @Override
            protected Bitmap doInBackground(final Void ... params) {
		        Bitmap profilePicture = null;
		        try {
						String pic = imageURL;
						Log.d(TAG, "Before downloading the image at: "+pic);
						if (pic.contains("https://graph.facebook.com/") && pic.contains("23.253.166.61")){
							pic = pic.replace("23.253.166.61", "");
							Log.d(TAG, "Changed pic url to: "+pic);
							if (pic.contains("http://") && pic.contains("https://")){
								pic = pic.replace("http://", "");
								Log.d(TAG, "Changed pic url to: "+pic);
							}
						}
						URL url = new URL(pic);
						URLConnection urlConnection = url.openConnection();
						InputStream in = new BufferedInputStream(urlConnection.getInputStream());

						profilePicture = BitmapFactory.decodeStream(in);

		        } catch (Exception e) {
		            e.printStackTrace();
		        }
		        return profilePicture;
			}
			@Override
			protected void onPostExecute(Bitmap result) {
				if (isUserOrFriend){ 
					CommonData.setProfilePicture(result); 
					Intent homescreen = new Intent();
					homescreen.setAction("HOMESCREEN");
					context.sendBroadcast(homescreen);
				}
				else{ setFriendProfileImageResult(result, notificationId, senderUserName, imageURL, id, context); }
		    }
		}.execute();	
	}

	private static void setFriendProfileImageResult(Bitmap result, int notificationId, String senderUserName, String senderProfilePicURL, int id, Context context){
		Log.d(TAG, "Entered setFriendProfileImageResult");
		friendProfilePic = result;
		Drawable friendPicDrawable = null;
		
		//crop and make into circle
		if (friendProfilePic != null){
			if (senderProfilePicURL.contains("graph.facebook.com")){
				//FB friends' pics will come in as squares, unlike regular Smirk users
				friendProfilePic = Utils.getRoundedCornerBitmap(friendProfilePic, friendProfilePic.getWidth());
				friendProfilePic = Utils.scaleBitmap(friendProfilePic, 100f, 100f);
				friendPicDrawable = new BitmapDrawable(context.getResources(), friendProfilePic);
			}
			else{
				friendProfilePic = Utils.scaleBitmap(friendProfilePic, 200f, 200f);
				friendPicDrawable = new BitmapDrawable(context.getResources(), friendProfilePic);
			}
		} else{
			//set Drawable friendPicDrawable to the default
			Log.d(TAG, "Friend image was null");
			friendPicDrawable = context.getResources().getDrawable(R.drawable.icon_profile_pic_mini);
		}
		FriendNotification pending = new FriendNotification(notificationId, senderUserName, senderProfilePicURL, 
		friendPicDrawable, id);
		notificationsList.add(pending);
		
		FriendNotification.setList(notificationsList);
	}
	
	public static void checkDuplicateEmail(final Context context, final String email) {
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		String url = Constants.GET_SECRET_ANSWERS;
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						try {
							Log.d(TAG,"Response: "+response.toString());
							int code = Integer.parseInt(response.getString("code"));
							if (code == 0){
								Intent intent = new Intent();
								intent.setAction("DUPLICATE_EMAIL_CHECK");
								intent.putExtra("EMAIL_DUPLICATE", "YES");
								Log.d(TAG, "Sending brodcast indicating duplicate email!");
								context.sendBroadcast(intent);
							} else if (code == 1001){
								Intent intent = new Intent();
								intent.setAction("DUPLICATE_EMAIL_CHECK");
								intent.putExtra("EMAIL_DUPLICATE", "NO");
								Log.d(TAG, "Sending brodcast indicating not duplicate email!");
								context.sendBroadcast(intent);
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error retrieving the user's password reset answers: " + error.getStackTrace());
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("email", email);
				return m;
			}
		};
		queue.add(request);		
	}
	
	public static void getUserPwdRetrivalAnswers(final Context context, final String email) {
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		String url = Constants.GET_SECRET_ANSWERS;
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.GET, url, null, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						try {
							Log.d(TAG,"Response: "+response.toString());
							int code = Integer.parseInt(response.getString("code"));
							if (code == 0){
								//response successful
								String questionOne = "";
								String questionTwo = "";
								
								String answerOne = "";
								String answerTwo = "";
								
								JSONArray answersArray = response.getJSONArray("qaList");
								
								for (int i = 0; i<answersArray.length(); i++){
									if (i == 0){
										answerOne = answersArray.getJSONObject(i).getString("answer");
										Log.d(TAG, "Got first answer: "+answerOne);
									}
									if (i == 1){
										answerTwo = answersArray.getJSONObject(i).getString("answer");
										Log.d(TAG, "Got second answer: "+answerTwo);
									}
								}
								
								
								Intent intent = new Intent();
								intent.setAction("SECRET_QA");
								intent.putExtra("SECRET_QA_RESPONSE", "OK");
								intent.putExtra("questionOne", questionOne);
								intent.putExtra("questionTwo", questionTwo);
								intent.putExtra("answerOne", answerOne);
								intent.putExtra("answerTwo", answerTwo);
								Log.d(TAG, "Sending brodcast indicating successful retrival of user pwd reset answers");
								context.sendBroadcast(intent);
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {

					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error retrieving the user's password reset answers: " + error.getStackTrace());
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("email", email);
				return m;
			}

		};
		queue.add(request);
		
	}
	
	public static void setNewPassword(final Context context, final String email, final JSONObject passwordObject) {
		RequestQueue queue = VolleySingleton.getInstance(context).getRequestQueue();
		String url = Constants.SET_NEW_PASSWORD;
		String newPassword = "";
		try{
			newPassword = passwordObject.getString("newpassword");
			Log.d(TAG, "JSON object for set pwd request had pwd: "+newPassword);
		} catch (JSONException e){
			e.printStackTrace();
		}
		Log.d(TAG, "Before calling the setNewPassword method with email, password: "+email+", "+newPassword);
		JsonObjectRequest request = new JsonObjectRequest(Request.Method.POST, url, passwordObject, new Response.Listener<JSONObject>() {
					@Override
					public void onResponse(JSONObject response) {						
						try {
							Log.d(TAG,"Response: "+response.toString());
							int code = Integer.parseInt(response.getString("code"));
							if (code == 0){
								CommonData.setUserPassword(passwordObject.getString("newpassword"));
								Log.d(TAG, "New password is: "+CommonData.password);
								Toast.makeText(context, "New password set", Toast.LENGTH_LONG).show();
							} else{
								Toast.makeText(context, "Password update could not be completed.", Toast.LENGTH_LONG).show();
							}
						} catch (JSONException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
						}
					}
				}, new Response.ErrorListener() {
					@Override
					public void onErrorResponse(VolleyError error) {
						// TODO Auto-generated method stub
						Log.d(TAG, "Error setting user's new password: " + error.getStackTrace());
					}
				}) {
			@Override
			public Map<String, String> getHeaders() throws AuthFailureError {
				Map<String, String> m = new HashMap<String, String>();
				m.put("Content-Type", "application/json");
				m.put("email", email);
				return m;
			}};
		queue.add(request);
	}
	
}
