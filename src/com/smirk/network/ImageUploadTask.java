package com.smirk.network;

import java.io.DataOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.net.HttpURLConnection;
import java.net.URI;
import java.net.URISyntaxException;
import java.net.URL;
import java.util.Scanner;
import java.util.List;
import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;

import org.apache.http.entity.ContentType;
import org.apache.http.entity.mime.MultipartEntityBuilder;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

//import com.trending.android.settings.Constants;
import com.smirk.commom.CommonData;
import com.smirk.commom.VolleySingleton;
import com.smirk.commom.Utils;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;
import android.os.AsyncTask;
import android.util.Base64;
import android.util.Log;

public class ImageUploadTask extends AsyncTask<Void, Void, String> {
	public static String TAG = "ImageUploadTask";
	private String urlServer = null;
	private String imageBase64;
	private Activity mActivity;
	private String mImgUrlString;
	private String serverResponseMessage = null;
	private int serverResponseCode = 0;
	private Map<String, String> headers;
	
	private String sessionId;
	private int points;

	public ImageUploadTask(Activity activity, String serverURL, String imageBase64, Map<String, String> headers) {
		this.mActivity = activity;
		this.urlServer = serverURL;
		this.imageBase64 = imageBase64;
		this.headers = headers;
	}
	
	public void getHeaders(){
		//This is so that we can use this class for any type of image within Smirk -- just need to see what its headers Map looks like
		//If no headers, it's a profile picture
		//If two headers, it's a Smirk picture upload
		if(headers.size() == 0){
			Log.d(TAG, "Received a profile picture to upload");
		}
		else if (headers.size() > 0){
			Log.d(TAG, "Recieved a Smirk picture to upload");
			
			for (Entry<String, String> entry : headers.entrySet())
			{
			    Log.d(TAG, (entry.getKey() + "/" + entry.getValue()));
				if (entry.getKey().equals("sessionid")){
					sessionId = entry.getValue();
				}
				if (entry.getKey().equals("points")){
					points = Integer.parseInt(entry.getValue());
				}
			}	
		}	
	}

	@Override
	protected String doInBackground(Void... params) {
		HttpURLConnection connection = null;
		DataOutputStream outputStream = null;
		String lineEnd = "\r\n";
		String twoHyphens = "--";
		String boundary = "*****";
		String myFile1 = "fileUpload";

		JSONArray jArray = null;
		String hostUrl = null;

		try {
			URL url = new URL(urlServer);
			connection = (HttpURLConnection) url.openConnection();

			// Allow Inputs &amp; Outputs
			connection.setDoInput(true);
			connection.setDoOutput(true);
			connection.setUseCaches(false);

			// Set HTTP method to POST.
			connection.setRequestMethod("POST");

			connection.setRequestProperty("Connection", "Keep-Alive");
			connection.setRequestProperty("Content-Type", "multipart/form-data;boundary=" + boundary);

			outputStream = new DataOutputStream(connection.getOutputStream());
			outputStream.writeBytes(twoHyphens + boundary + lineEnd);
			outputStream.writeBytes("Content-Disposition: form-data; name=\"uploadedfile\";filename=\""+ myFile1 + "\"" + lineEnd);
			outputStream.writeBytes(lineEnd);
			outputStream.write(imageBase64.getBytes());

			outputStream.writeBytes(lineEnd);
			outputStream.writeBytes(twoHyphens + boundary + twoHyphens + lineEnd);

			// Responses from the server (code and message)
			Scanner scanner = new Scanner(connection.getInputStream());
			String response = scanner.useDelimiter("\\Z").next();
			JSONObject jObject = new JSONObject(response);
			Log.d(TAG, " The response is : ");
			// Log.d(TAG, response);
			Log.d(TAG, jObject.toString());
			scanner.close();
			hostUrl = jObject.getString("hostURL");
	
			try {
				if (jObject.has("code")) {
					int code = jObject.getInt("code");
					if (code == 0 && jObject.has("extra")) { // login
																// successful
						// get url
						mImgUrlString = jObject.getString("extra");

					} else { // if code is not 0 means error
						String msg = jObject.getString("message");
						Log.d(TAG, "Error ! " + msg);
					}
				}

				} catch (JSONException e) {
					e.printStackTrace();
					Log.d(TAG, "Exception Error ! " + e);
				}
				
			serverResponseCode = connection.getResponseCode();
			serverResponseMessage = connection.getResponseMessage();
			Log.d(TAG, "Response Code = " + serverResponseCode
					+ " Response Message = " + serverResponseMessage);
			outputStream.flush();
			outputStream.close();

		} catch (Exception e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return mImgUrlString;
	} 

	@Override
	protected void onPostExecute(String result) {
		if (serverResponseCode == 200) {
			Log.d(TAG, "Image uploaded");
			Log.d(TAG, "Response: "+result);
		}
		else{
			
		}
		
	} 
}

	/*
 Thread thread = new Thread(){
    @Override
    public void run() {
		 Log.d(TAG, "Entered the httpclient execute thread");
         try {
             HttpResponse response = httpclient.execute(httpPost);
			 Log.d(TAG, "Profile image upload status: "+response.getStatusLine().toString());
			 //Can turn the HttpResponse into JSON

		} catch (Exception e){
			e.printStackTrace();
		}
    }
};
thread.start();
*/
