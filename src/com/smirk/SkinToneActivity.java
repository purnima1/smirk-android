package com.smirk;

import java.io.InputStream;
import java.net.URL;
import java.net.HttpURLConnection;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.lang.StringBuilder;
import java.io.InputStreamReader;

import org.apache.http.protocol.HTTP;
import org.apache.http.client.HttpClient;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.params.HttpConnectionParams;
import org.apache.http.HttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.StringEntity;
import org.apache.http.message.BasicHeader;
import com.android.volley.Request.Method;

import org.json.JSONObject;
import org.json.JSONException;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Intent;
import android.provider.MediaStore;
import android.os.Bundle;
import android.os.Looper;
import android.text.TextUtils;
import android.util.Log;
import android.view.MenuItem;
import android.view.View;
import android.view.View.OnClickListener;
import android.widget.ImageButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.graphics.Bitmap;
import android.net.Uri;
import android.util.Base64;

import com.android.volley.*;
import com.android.volley.toolbox.JsonObjectRequest;

import com.smirk.commom.*;
import com.smirk.commom.database.*;
import com.smirk.network.SmirkApiCalls;

public class SkinToneActivity extends Activity implements OnClickListener {

	public final static String apiURL = "http://71.96.94.57/verizonbackend/v1/api/user/signup";
	public final static String TAG = "SkinToneActivity";
	private final static int SIGNUP_SUCCESSFUL = 2;

	private int[] mImgBtnId = { R.id.colorwheel_goldenfair_imgbtn_id,
			R.id.colorwheel_fair_imgbtn_id, R.id.colorwheel_light_imgbtn_id,
			R.id.colorwheel_medium_imgbtn_id,
			R.id.colorwheel_mediumcool_imgbtn_id,
			R.id.colorwheel_bronzemedium_imgbtn_id,
			R.id.colorwheel_bronzedark_imgbtn_id,
			R.id.colorwheel_lighttan_imgbtn_id, R.id.colorwheel_tan_imgbtn_id,
			R.id.colorwheel_dark_imgbtn_id, R.id.colorwheel_deep_imgbtn_id,
			R.id.submit_imgbtn_id };
	private ImageButton mImgBtn;
	// default RGB values
	private int[] mRGB = { 243, 241, 191 };
	private ImageView mImageView;
	private int mTvId = R.id.colorwheel_goldenfair_tv_id;

	private String userName, email, password, fullName, gender, dateOfBirth,
			skinTone, profilePictureURIString;

	private int emailUpdates, cameraAccess, stayLoggedIn;

	private Bitmap profilePicture;
	private byte[] profilePictureByteArray;
	private String base64profilePicture;

	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		setContentView(R.layout.activity_skin_tone);

		userName = getIntent().getStringExtra(Utils.USER_NAME);
		password = getIntent().getStringExtra(Utils.PASSWORD);
		email = getIntent().getStringExtra(Utils.EMAIL);
		profilePictureURIString = getIntent().getStringExtra(Utils.PROFILE_PICTURE);
		fullName = getIntent().getStringExtra(Utils.FULL_NAME);
		gender = getIntent().getStringExtra(Utils.GENDER);
		dateOfBirth = getIntent().getStringExtra(Utils.DOB);
		emailUpdates = getIntent().getIntExtra(Utils.MAILING_LIST, 1); // defaults are 1
		cameraAccess = getIntent().getIntExtra(Utils.CAMERA_PERMISSION, 1);
		
		try{
			profilePicture = MediaStore.Images.Media.getBitmap(this.getContentResolver(), Uri.parse(profilePictureURIString));
			profilePicture = Bitmap.createScaledBitmap(profilePicture, 200, 200, false);
			profilePicture = Utils.getRoundedCornerBitmap(profilePicture, profilePicture.getWidth());
			profilePicture = Utils.addCircleFrame(profilePicture);
			for (int i=0; i<CommonData.profilePicRotations; i++){
				profilePicture = Utils.RotateBitmap(profilePicture, 90);
			}
			
		} catch (FileNotFoundException e){
			e.printStackTrace();
		} catch (IOException e){
			e.printStackTrace();
		}
		
		profilePictureByteArray = Utils.getBitmapAsByteArray(profilePicture);
		base64profilePicture = Base64.encodeToString(profilePictureByteArray, Base64.URL_SAFE); 
		Log.d(TAG, "Base64ProfilePicture.length(): "+base64profilePicture.length());

		for (int i = 0; i < mImgBtnId.length; i++) {
			mImgBtn = (ImageButton) findViewById(mImgBtnId[i]);
			mImgBtn.setOnClickListener(this);
		}

		mImageView = (ImageView) findViewById(R.id.color_selected_iv_id);
	}

	@Override
	public void onClick(View v) {
		int drawableId = 0;
		switch (v.getId()) {
		case R.id.colorwheel_goldenfair_imgbtn_id:
			mTvId = R.id.colorwheel_goldenfair_tv_id;
			drawableId = R.drawable.colorsample_goldenfair;
			Log.i("***********************", "************** tvid = " + mTvId);
			// parseRGB(R.id.colorwheel_goldenfair_tv_id, mRGB);
			// mImageView.setImageDrawable(this.getResources().getDrawable(R.drawable.colorsample_goldenfair));
			break;
		case R.id.colorwheel_fair_imgbtn_id:
			mTvId = R.id.colorwheel_fair_tv_id;
			drawableId = R.drawable.colorsample_fair;
			break;
		case R.id.colorwheel_light_imgbtn_id:
			mTvId = R.id.colorwheel_light_tv_id;
			drawableId = R.drawable.colorsample_light;
			break;
		case R.id.colorwheel_medium_imgbtn_id:
			mTvId = R.id.colorwheel_medium_tv_id;
			drawableId = R.drawable.colorsample_medium;
			break;
		case R.id.colorwheel_mediumcool_imgbtn_id:
			mTvId = R.id.colorwheel_mediumcool_tv_id;
			drawableId = R.drawable.colorsample_mediumcool;
			break;
		case R.id.colorwheel_bronzemedium_imgbtn_id:
			mTvId = R.id.colorwheel_bronzemedium_tv_id;
			drawableId = R.drawable.colorsample_bronzemedium;
			break;
		case R.id.colorwheel_bronzedark_imgbtn_id:
			mTvId = R.id.colorwheel_bronzedark_tv_id;
			drawableId = R.drawable.colorsample_bronzedark;
			break;
		case R.id.colorwheel_lighttan_imgbtn_id:
			mTvId = R.id.colorwheel_lighttan_tv_id;
			drawableId = R.drawable.colorsample_lighttan;
			break;
		case R.id.colorwheel_tan_imgbtn_id:
			mTvId = R.id.colorwheel_tan_tv_id;
			drawableId = R.drawable.colorsample_tan;
			break;
		case R.id.colorwheel_dark_imgbtn_id:
			mTvId = R.id.colorwheel_dark_tv_id;
			drawableId = R.drawable.colorsample_dark;
			break;
		case R.id.colorwheel_deep_imgbtn_id:
			mTvId = R.id.colorwheel_deep_tv_id;
			drawableId = R.drawable.colorsample_deep;
			break;
		case R.id.submit_imgbtn_id:
			TextView tv = (TextView) findViewById(mTvId);
			skinTone = tv.getText().toString();
			//CommonData.setSkinToneRGB(skinTone);
			
			uploadNewProfile();

			Intent intent = new Intent(this, SmirkMainActivity.class);
			this.startActivity(intent);
			this.finish();
			break;

		}
		if (v.getId() != R.id.submit_imgbtn_id) {
			mImageView.setImageDrawable(this.getResources().getDrawable(
					drawableId));
			// parseRGBResetColor(tvId, drawableId, mRGB);
		}
	}

	private void parseRGBResetColor(int tvId, int drawableId, int[] rgb) {
		parseRGB(tvId, rgb);
		mImageView.setImageDrawable(this.getResources().getDrawable(drawableId));
	}

	private void parseRGB(int tvId, int[] rgb) {
		TextView tv = (TextView) findViewById(tvId);
		String[] sAry = tv.getText().toString().split(":");
		int temp[] = new int[sAry.length];
		for (int i = 0; i < temp.length; i++) {
			temp[i] = -1;
		}
		for (int i = 0; i < sAry.length; i++) {
			if (TextUtils.isDigitsOnly(sAry[i])) {
				temp[i] = Integer.parseInt(sAry[i]);
			} else {
				if (sAry[i].contains("\n")) {
					String[] t = sAry[i].split("\n");
					for (int k = 0; k < t.length; k++) {
						if (TextUtils.isDigitsOnly(t[k])) {
							temp[i] = Integer.parseInt(t[k]);
						}
					}
				}
			}
		}
		for (int i = 0, index = 0; i < temp.length; i++) {
			if (temp[i] != -1) {
				rgb[index++] = temp[i];
			}
		}
	}

	@Override
	public boolean onOptionsItemSelected(MenuItem item) {
		// Handle action bar item clicks here. The action bar will
		// automatically handle clicks on the Home/Up button, so long
		// as you specify a parent activity in AndroidManifest.xml.
		int id = item.getItemId();
		if (id == R.id.action_settings) {
			return true;
		}
		return super.onOptionsItemSelected(item);
	}
	
	private void uploadNewProfile(){
		JSONObject newUser = makeNewUserJSON();
		SmirkApiCalls.profileCreateHTTPClient(profilePicture, newUser, getApplicationContext());
	}

	private JSONObject makeNewUserJSON() {
		// Tag used to cancel the request
		String tag_json_obj = "json_obj_req";

		String url = "http://23.253.166.61:8080/smirkservices/api/user/signup";

		final ProgressDialog pDialog = new ProgressDialog(this);
		pDialog.setMessage("Loading...");
		pDialog.show();

		JSONObject newUser = new JSONObject();

		try {
			newUser.put("screenName", userName);
			newUser.put("email", email);
			newUser.put("password", password);
			newUser.put("name", fullName);
			newUser.put("birthDate", dateOfBirth);
			newUser.put("gender", gender);
			newUser.put("skinTone", skinTone);
			newUser.put("offerEmail", emailUpdates);
			newUser.put("useCamera", cameraAccess);
		} catch (JSONException e) {
			throw new RuntimeException(e);
		}
		Log.d(TAG, "before calling the signin api" + newUser);
		//SmirkApiCalls.signLogInJsonObjectRequestQueue(this, url, null, newUser);
		return newUser;
	}
}

	